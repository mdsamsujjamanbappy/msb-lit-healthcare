<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExtraController extends Controller
{
    public function zone(){
        return view('extra.zone');
    }
    public function user(){
        return view('extra.user');
    }
    public function station(){
        return view('extra.station');
    }
    public function inventory_report(){
        return view('extra.inventory_report');
    }

    public function head(){
        return view('extra.head');
    }
    public function subhead(){
        return view('extra.subhead');
    }
    public function cash_receive(){
        return view('extra.cash_receive');
    }
    public function cash_distribute(){
        return view('extra.cash_distribute');
    }
    public function cash_expense(){
        return view('extra.cash_expense');
    }

    public function category(){
        return view('extra.category');
    }
    public function product(){
        return view('extra.product');
    }
    public function unit(){
        return view('extra.unit');
    }
    public function supplier(){
        return view('extra.supplier');
    }
    public function purchase_receive(){
        return view('extra.purchase_receive');
    }
    public function distribution(){
        return view('extra.distribution');
    }


}
