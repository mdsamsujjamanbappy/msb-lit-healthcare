<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccommodationListController extends Controller
{
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }

	public function list()
    {   
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        $data_list = DB::table('tb_accommodation_list')
        ->leftjoin('tb_accommodation_category_list','tb_accommodation_category_list.id','=','tb_accommodation_list.accommodation_category_id')
        ->leftjoin('tb_company_information','tb_accommodation_list.company_id','=','tb_company_information.id')
        ->select('tb_accommodation_list.*', 'tb_accommodation_category_list.category_name', 'tb_company_information.company_name')
        ->where('tb_accommodation_list.company_id', $user_company)
        ->get();
		
		$category_list = DB::table('tb_accommodation_category_list')
        ->select('tb_accommodation_category_list.id', 'tb_accommodation_category_list.category_name')
        ->where([['tb_accommodation_category_list.company_id', $user_company], ['tb_accommodation_category_list.status', 1]])
        ->get();

        return view('accommodation.accommodation_list.list',compact('data_list', 'category_list'));
    }

  	public function store(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

  		DB::table('tb_accommodation_list')->insert([
            'accommodation_name_number'	=> $request->accommodation_name_number,
            'accommodation_category_id'	=> $request->accommodation_category_id,
            'rent_amount'				=> $request->rent_amount,
            'remarks' 					=> $request->remarks,
            'status' 					=> $request->status,
            'company_id' 				=> auth::user()->company_id,
            'created_by' 				=> Auth::user()->id,
            'created_at'  				=> Carbon::now()->toDateTimeString(),
            'updated_at'  				=> Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Accommodation information has been successfully added.');
        return redirect()->back();
  	}

	public function edit($id)
    {   
        $this->company_selector();
        $user_company = Auth::user()->company_id;
        $id = base64_decode($id);
        $details = DB::table('tb_accommodation_list')
        ->where([['tb_accommodation_list.id', $id], ['tb_accommodation_list.company_id', $user_company]])
        ->first();
        return response()->json($details);
    }

  	public function update(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

  		DB::table('tb_accommodation_list')->where('tb_accommodation_list.id', $request->id)->update([
            'accommodation_name_number'	=> $request->accommodation_name_number,
            'accommodation_category_id'	=> $request->accommodation_category_id,
            'rent_amount'				=> $request->rent_amount,
            'remarks' 					=> $request->remarks,
            'status' 					=> $request->status,
            'company_id' 				=> auth::user()->company_id,
            'created_by' 				=> Auth::user()->id,
            'updated_at'  				=> Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Accommodation information has been successfully updated.');
        return redirect()->back();
  	}

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        DB::table('tb_accommodation_list')->where('id', '=', $id)->delete();
        Session::flash('successMessage', 'Accommodation has been successfully destroyed.');
        return redirect()->back();
    }
}
