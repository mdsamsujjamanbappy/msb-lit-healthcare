<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WeekendHolidayController extends Controller
{
    public function weekend_holiday_list()
    {   
        $weekend_holiday = DB::table('tb_weekend_holiday')
        ->leftjoin('tb_company_information','tb_weekend_holiday.company_id','=','tb_company_information.id')
        ->select('tb_weekend_holiday.*', 'tb_company_information.company_name')
        ->get();
        return view('weekend_holiday.list', compact('weekend_holiday'));
    }

}
