<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DoctorShiftListController extends Controller
{
    //doctor shift view page
    public function index(){
        $shiftdata=DB::table('tb_doctor_shift_list')
        ->select('tb_doctor_shift_list.doctor_id')
        ->groupby('tb_doctor_shift_list.doctor_id')
        ->get();

        foreach($shiftdata as $sd){
	       $doctor_details = DB::table('tb_doctor_list')
	        ->leftjoin('tb_department_list','tb_doctor_list.doctor_department_id','=','tb_department_list.id')
	        ->leftjoin('tb_designation_list','tb_doctor_list.doctor_designation_id','=','tb_designation_list.id')
	        ->leftjoin('tb_company_information','tb_doctor_list.company_id','=','tb_company_information.id')
	        ->select('tb_doctor_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_company_information.company_name')
	        ->where('tb_doctor_list.id', $sd->doctor_id)
	        ->first();
	        $sd->doctor_details = $doctor_details;
        }
        return view('doctor_shift.index', compact('shiftdata'));
    }

    //check doctor shift already exists data
    public function CheckDoctorShift($id){
       $data=DB::table('doctor_shift')
       ->select('startTime','endTime')
       ->where('doctorId',$id)
       ->get();
       return response()->json($data);
    }


    //shift create method
    public function create(){
        $doctor_list=DB::table('tb_doctor_list')->select('id','doctor_name','doctor_phone')->get();
        return view('doctor_shift.create', compact('doctor_list'));
    }

    //shift store admin or super admin method
    public function ShiftStore(Request $request){
        $doctorid=array($request->doctor_id);
        $shiftCheck=DB::table('tb_doctor_shift_list')->where('doctor_id',$doctorid)->count();
        if($shiftCheck>1){
            $delete=DB::table('tb_doctor_shift_list')->where('doctor_id',$doctorid)->delete();
            for($i=0; $i<count($request->day_select); $i++){
                if(!empty($request->shift_start[$i]) || !empty($request->shift_end[$i])){
                	$data=DB::table('tb_doctor_shift_list')->insert([
	                    'doctor_id' 	=>	$request->doctor_id,
	                    'shift_day' 	=>	$request->day_select[$i],
	                    'start_time' 	=>	$request->shift_start[$i],
	                    'end_time' 		=>	$request->shift_end[$i],
	                    'created_by' 	=>	auth::user()->id,
	                    'created_at'  	=>	Carbon::now()->toDateTimeString(),
	                    'updated_at'  	=>	Carbon::now()->toDateTimeString(),
	                ]);
                }
            }
        }
        else{
            for($i=0; $i<count($request->day_select); $i++){
                if(!empty($request->shift_start[$i]) || !empty($request->shift_end[$i])){
	                $data=DB::table('tb_doctor_shift_list')->insert([
	                    'doctor_id' 	=>	$request->doctor_id,
	                    'shift_day' 	=>	$request->day_select[$i],
	                    'start_time' 	=>	$request->shift_start[$i],
	                    'end_time' 		=>	$request->shift_end[$i],
	                    'created_by' 	=>	auth::user()->id,
	                    'created_at'  	=>	Carbon::now()->toDateTimeString(),
	                    'updated_at'  	=>	Carbon::now()->toDateTimeString(),
	                ]);
                }
            }
        }
        Session::flash('successMessage', 'Doctor shift has been successfully added!');
        return redirect()->back();
    }

    //doctor shift show
    public function ShiftShow($id){
        $id=base64_decode($id);
        $shiftDatasingle=DB::table('tb_doctor_shift_list')
        ->leftjoin('tb_doctor_list','tb_doctor_shift_list.doctor_id','=','tb_doctor_list.id')
        ->leftjoin('users','tb_doctor_shift_list.created_by','=','users.id')
        ->leftjoin('tb_department_list','tb_doctor_list.doctor_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_doctor_list.doctor_designation_id','=','tb_designation_list.id')
        ->select('tb_doctor_shift_list.*','tb_doctor_list.*','users.name','tb_department_list.department_name','tb_designation_list.designation_name')
        ->where('tb_doctor_shift_list.doctor_id', $id)
        ->first();

        $shiftData=DB::table('tb_doctor_shift_list')
        ->where('tb_doctor_shift_list.doctor_id', $id)
        ->WhereNotNull('tb_doctor_shift_list.start_time')
        ->get();

       return view('doctor_shift.details', compact('shiftData','shiftDatasingle'));
    }

    //doctor shift delete
    public function ShiftDelete($id){
        $id=base64_decode($id);
        $data=DB::table('tb_doctor_shift_list')->where('doctor_id',$id)->delete();
        if($data){
            Session::flash('successMessage', 'Doctor Shift has been successfully deleted !');
            return redirect()->back();
        }
    }

    //specific doctor wise shift show after doctor login
    public function doctorsShift(){
        $doctorid=auth()->user()->doctor_id;
        $data=DB::table('tb_doctor_shift_list')
            ->where('tb_doctor_shift_list.doctor_id', $doctorid)
            ->get();
        return view('profile.doctor_shift.shift_show',compact('data'));
    }

    //shift update doctor wise
    public function ShiftUpdateDoctor(Request $request){
        $doctorid=array($request->doctor_id);
        $shiftCheck=DB::table('doctor_shift')->where('doctorId',$doctorid)->count();
        if($shiftCheck>1){
            $delete=DB::table('doctor_shift')->where('doctorId',$doctorid)->delete();
            for($i=0;$i<count($request->day_select);$i++){
                $data=DB::table('doctor_shift')->insert([
                    'doctorId' =>$request->doctor_id,
                    'shiftDay' =>$request->day_select[$i],
                    'startTime' =>$request->shift_start[$i],
                    'endTime' =>$request->shift_end[$i],
                    'createdBy' =>auth::user()->is_permission,
                    'created_at'  =>Carbon::now()->toDateTimeString(),
                    'updated_at'  =>Carbon::now()->toDateTimeString(),
                ]);
            }
        }
        else{
            for($i=0;$i<count($request->day_select);$i++){
                $data=DB::table('doctor_shift')->insert([
                    'doctorId' =>$request->doctor_id,
                    'shiftDay' =>$request->day_select[$i],
                    'startTime' =>$request->shift_start[$i],
                    'endTime' =>$request->shift_end[$i],
                    'createdBy' =>auth::user()->is_permission,
                    'created_at'  =>Carbon::now()->toDateTimeString(),
                    'updated_at'  =>Carbon::now()->toDateTimeString(),
                ]);
            }
        }
        Session::flash('shift_add', 'Shift Update Successful!');
        return redirect()->back();
    }

}