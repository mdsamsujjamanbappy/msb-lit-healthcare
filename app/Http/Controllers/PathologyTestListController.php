<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PathologyTestListController extends Controller
{
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }

	public function list()
    {   
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        $data_list = DB::table('tb_pathology_test_list')
        ->leftjoin('tb_pathology_test_category_list','tb_pathology_test_category_list.id','=','tb_pathology_test_list.test_category_id')
        ->leftjoin('tb_company_information','tb_pathology_test_list.company_id','=','tb_company_information.id')
        ->select('tb_pathology_test_list.*', 'tb_pathology_test_category_list.category_name', 'tb_company_information.company_name')
        ->where('tb_pathology_test_list.company_id', $user_company)
        ->get();
		
		$test_category_list = DB::table('tb_pathology_test_category_list')
        ->select('tb_pathology_test_category_list.id', 'tb_pathology_test_category_list.category_name')
        ->where([['tb_pathology_test_category_list.company_id', $user_company], ['tb_pathology_test_category_list.status', 1]])
        ->get();

        return view('pathology.test_list.list',compact('data_list', 'test_category_list'));
    }

  	public function store(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

  		DB::table('tb_pathology_test_list')->insert([
            'test_name'			=> $request->test_name,
            'test_category_id'	=> $request->test_category_id,
            'price_amount'		=> $request->price_amount,
            'test_location'		=> $request->test_location,
            'remarks' 			=> $request->remarks,
            'status' 			=> $request->status,
            'company_id' 		=> auth::user()->company_id,
            'created_by' 		=> Auth::user()->id,
            'created_at'  		=> Carbon::now()->toDateTimeString(),
            'updated_at'  		=> Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Pathology test information has been successfully added.');
        return redirect()->back();
  	}

	public function edit($id)
    {   
        $this->company_selector();
        $user_company = Auth::user()->company_id;
        $id = base64_decode($id);
        $details = DB::table('tb_pathology_test_list')
        ->where([['tb_pathology_test_list.id', $id], ['tb_pathology_test_list.company_id', $user_company]])
        ->first();
        return response()->json($details);
    }

  	public function update(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

  		DB::table('tb_pathology_test_list')->where('tb_pathology_test_list.id', $request->id)->update([
            'test_name'			=> $request->test_name,
            'test_category_id'	=> $request->test_category_id,
            'price_amount'		=> $request->price_amount,
            'test_location'		=> $request->test_location,
            'remarks' 			=> $request->remarks,
            'status' 			=> $request->status,
            'company_id' 		=> auth::user()->company_id,
            'created_by' 		=> Auth::user()->id,
            'updated_at'  		=> Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Pathology test information has been successfully updated.');
        return redirect()->back();
  	}

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        DB::table('tb_pathology_test_list')->where('id', '=', $id)->delete();
        Session::flash('successMessage', 'Test has been successfully destroyed.');
        return redirect()->back();
    }
}
