<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InPatientOccupanciesController extends Controller
{
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }

	public function list()
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        $data_list = DB::table('tb_inpatient_occupancies_list')
        ->leftjoin('tb_patient_list','tb_patient_list.id','=','tb_inpatient_occupancies_list.patient_id')
        ->leftjoin('tb_doctor_list','tb_doctor_list.id','=','tb_inpatient_occupancies_list.doctor_id')
        ->leftjoin('tb_accommodation_list','tb_accommodation_list.id','=','tb_inpatient_occupancies_list.accommodation_id')
        ->leftjoin('tb_accommodation_category_list','tb_accommodation_category_list.id','=','tb_accommodation_list.accommodation_category_id')
        ->leftjoin('tb_company_information','tb_doctor_list.company_id','=','tb_company_information.id')
        ->select('tb_inpatient_occupancies_list.*', 'tb_patient_list.patient_id as patientId', 'tb_patient_list.patient_name', 'tb_doctor_list.doctor_name', 'tb_accommodation_list.accommodation_name_number', 'tb_accommodation_category_list.category_name', 'tb_company_information.company_name')
        ->where('tb_inpatient_occupancies_list.company_id', $user_company)
        ->whereIn('tb_inpatient_occupancies_list.status', [1,2])
        ->get();

        $patient_list = DB::table('tb_patient_list')
        ->select('tb_patient_list.*')
        ->where('tb_patient_list.company_id', $user_company)
        ->get();

        $doctor_list = DB::table('tb_doctor_list')
        ->leftjoin('tb_department_list','tb_doctor_list.doctor_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_doctor_list.doctor_designation_id','=','tb_designation_list.id')
        ->select('tb_doctor_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name')
        ->orderBy('tb_doctor_list.doctor_name', 'asc')
        ->where('tb_doctor_list.company_id', $user_company)
        ->get();
        
        $accommodation_list = DB::table('tb_accommodation_list')
        ->leftjoin('tb_accommodation_category_list','tb_accommodation_category_list.id','=','tb_accommodation_list.accommodation_category_id')
        ->leftjoin('tb_company_information','tb_accommodation_list.company_id','=','tb_company_information.id')
        ->select('tb_accommodation_list.*', 'tb_accommodation_category_list.category_name', 'tb_company_information.company_name')
        ->where('tb_accommodation_list.company_id', $user_company)
        ->get();

        return view('patient_appointment.inpatient_occupancies.list', compact('data_list', 'patient_list', 'doctor_list', 'accommodation_list'));
    }

    public function store(Request $request)
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;
         $date = date('Y-m-d');
         $id_prefix = date('ymd');
         $data = DB::table('tb_inpatient_occupancies_list')->select('admission_id')->orderBy('id', 'desc')->whereDate('created_at', $date)->first(); 
         if(!empty($data->admission_id)){
             $arr1 = substr($data->admission_id, 3);
             $arr1++;
         }else{
             $arr1 = $id_prefix."01";
         }
        $admission_id = "IPD".$arr1;

        DB::table('tb_inpatient_occupancies_list')->insert([
            'admission_id'     => $admission_id,
            'patient_id'       => $request->patient_id,
            'doctor_id'        => $request->doctor_id,
            'accommodation_id' => $request->accommodation_id,
            'admission_reason' => $request->admission_reason,
            'admission_date'   => $request->admission_date,
            'remarks'          => $request->remarks,
            'status'           => 1,
            'company_id'       => auth::user()->company_id,
            'created_by'       => Auth::user()->id,
            'created_at'       => Carbon::now()->toDateTimeString(),
            'updated_at'       => Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Selected patient admission has been successfully placed.');
        return redirect()->back();
    }

    public function details($id)
    {
        $id = base64_decode($id);
        $user_company = Auth::user()->company_id;
        $details = DB::table('tb_inpatient_occupancies_list')
        ->leftjoin('tb_patient_list','tb_patient_list.id','=','tb_inpatient_occupancies_list.patient_id')
        ->leftjoin('tb_doctor_list','tb_doctor_list.id','=','tb_inpatient_occupancies_list.doctor_id')
        ->leftjoin('tb_accommodation_list','tb_accommodation_list.id','=','tb_inpatient_occupancies_list.accommodation_id')
        ->leftjoin('tb_accommodation_category_list','tb_accommodation_category_list.id','=','tb_accommodation_list.accommodation_category_id')
        ->leftjoin('tb_company_information','tb_doctor_list.company_id','=','tb_company_information.id')
        ->select('tb_inpatient_occupancies_list.*', 'tb_patient_list.patient_id as patientId', 'tb_patient_list.id as pId', 'tb_patient_list.patient_name', 'tb_patient_list.patient_phone', 'tb_patient_list.emergency_contact_name', 'tb_patient_list.emergency_contact_phone', 'tb_patient_list.emergency_contact_relation', 'tb_patient_list.patient_address', 'tb_doctor_list.doctor_name', 'tb_doctor_list.doctor_phone', 'tb_accommodation_list.accommodation_name_number', 'tb_accommodation_category_list.category_name', 'tb_company_information.company_name')
        ->where([['tb_inpatient_occupancies_list.company_id', $user_company],['tb_inpatient_occupancies_list.id', $id]])
        ->first();

        $treatment_list = DB::table('tb_inpatient_treatment_list')
        ->leftjoin('users', 'users.id', '=', 'tb_inpatient_treatment_list.created_by')
        ->select('tb_inpatient_treatment_list.*', 'users.name as createdBy')
        ->where('tb_inpatient_treatment_list.inpatient_occupancies_id', $id)
        ->get();

        $bill_list = DB::table('tb_patient_bill_information')
        ->leftjoin('users', 'users.id', '=', 'tb_patient_bill_information.created_by')
        ->select('tb_patient_bill_information.*', 'users.name as createdBy')
        ->where([['tb_patient_bill_information.ref_id', $id], ['tb_patient_bill_information.bill_type', 1]])
        ->get();

        $payment_list = DB::table('tb_patient_payment_information')
        ->leftjoin('users', 'users.id', '=', 'tb_patient_payment_information.created_by')
        ->select('tb_patient_payment_information.*', 'users.name as createdBy')
        ->where([['tb_patient_payment_information.ref_id', $id], ['tb_patient_payment_information.payment_type', 1]])
        ->get();

        // dd($details);
        return view('patient_appointment.inpatient_occupancies.details', compact('details', 'treatment_list', 'bill_list', 'payment_list'));
    }

    public function medicine_name_list(Request $request)
    {
        $query = $request->get('query','');        
        $data = DB::table('tb_inpatient_medicine_list')->distinct('medicine_name')->where('medicine_name','LIKE','%'.$query.'%')->pluck('medicine_name');
        return response()->json($data);
    }
    
    public function medicine_taking_instruction_list(Request $request)
    {
        $query = $request->get('query','');        
        $data = DB::table('tb_inpatient_medicine_list')->distinct('medicine_taking_instruction')->where('medicine_taking_instruction','LIKE','%'.$query.'%')->pluck('medicine_taking_instruction');
        return response()->json($data);
    }
    
    public function treatment_store(Request $request)
    {
        $user_company = Auth::user()->company_id;
        $treatment_ref_no = DB::table('tb_inpatient_treatment_list')->insertGetId([
          'inpatient_occupancies_id'   =>  $request->inpatient_occupancies_id,
          'disease_name'        =>  $request->disease_name,
          'symptoms'            =>  $request->symptoms,
          'remarks'             =>  $request->remarks,
          'next_checkup'        =>  $request->next_checkup,
          'status'              =>  1,
          'company_id'          =>  $user_company,
          'created_by'          =>  Auth::user()->id,
          'created_at'          =>  Carbon::now()->toDateTimeString(),
          'updated_at'          =>  Carbon::now()->toDateTimeString()
        ]);

        if(isset($request->medicine_name) && is_array($request->medicine_name)){  
            foreach($request->medicine_name as $key => $text_field){

                DB::table('tb_inpatient_medicine_list')->insertGetId([
                    'inpatient_treatment_id' =>  $treatment_ref_no,
                    'medicine_name'     =>  $request->medicine_name[$key],
                    'medicine_doses'     =>  $request->medicine_doses[$key],
                    'medicine_times'     =>  $request->medicine_times[$key],
                    'medicine_taking_instruction' =>  $request->medicine_taking_instruction[$key],
                    'status'              =>  1,
                    'created_by'          =>  Auth::user()->id,
                    'created_at'          =>  Carbon::now()->toDateTimeString(),
                    'updated_at'          =>  Carbon::now()->toDateTimeString()
                ]);

            }
        }
        Session::flash('successMessage','Treatment information has been successfully added. ');
        return redirect()->back();
    }

    public function treatment_details($id)
    {
        $id = base64_decode($id);

        $treatment_details = DB::table('tb_inpatient_treatment_list')
        ->leftjoin('users', 'users.id', '=', 'tb_inpatient_treatment_list.created_by')
        ->select('tb_inpatient_treatment_list.*', 'users.name as createdBy')
        ->where('tb_inpatient_treatment_list.id', $id)
        ->first();

        $medicine_list = DB::table('tb_inpatient_medicine_list')
        ->where('tb_inpatient_medicine_list.inpatient_treatment_id', $id)
        ->get();

        return view('patient_appointment.inpatient_occupancies.treatment_details', compact('treatment_details', 'medicine_list'));
    }

}
