<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BloodBankController extends Controller
{
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }
    
  	public function list(){
        $data_list = DB::table('tb_blood_donor_list')
        ->leftjoin('tb_gender_list', 'tb_blood_donor_list.donor_gender_id', '=', 'tb_gender_list.id')
        ->leftjoin('tb_company_information', 'tb_blood_donor_list.company_id','=', 'tb_company_information.id')
        ->select('tb_blood_donor_list.*', 'tb_gender_list.gender_name', 'tb_company_information.company_name')
        ->orderBy('tb_blood_donor_list.id', 'asc')
        ->get();
        
      	return view('blood_donor.list', compact('data_list'));
  	}

  	public function create()
    {
      	$gender_list=DB::table('tb_gender_list')->where('tb_gender_list.status', '=', 1)->orderBy('tb_gender_list.id', 'asc')->get();
 
        return view('blood_donor.create', compact('gender_list'));
    }

    public function store(Request $request)
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        if(!empty($request->last_donation_date)){
            $date = $request->last_donation_date;
        }
        else{
            $date = null;
        }

        DB::table('tb_blood_donor_list')->insert([
            'donor_name'		=> $request->donor_name,
            'donor_phone' 		=> $request->donor_phone,
            'donor_gender_id' 	=> $request->donor_gender_id,
            'donor_age'			=> $request->donor_age,
            'donor_blood_group'	=> $request->donor_blood_group,
            'last_donation_date'=> $date,
            'donor_address'		=> $request->donor_address,
            'donor_remarks'		=> $request->donor_remarks,
            'company_id' 		=> auth::user()->company_id,
            'created_by' 		=> Auth::user()->id,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Blood donor information has been successfully added.');
        return redirect()->back();
    }

  	public function details($id){
  		$id = base64_decode($id);
        $donor_details = DB::table('tb_blood_donor_list')
        ->leftjoin('tb_gender_list', 'tb_blood_donor_list.donor_gender_id', '=', 'tb_gender_list.id')
        ->leftjoin('tb_company_information', 'tb_blood_donor_list.company_id','=', 'tb_company_information.id')
        ->leftjoin('users', 'tb_blood_donor_list.created_by', '=', 'users.id')
        ->select('tb_blood_donor_list.*', 'tb_gender_list.gender_name', 'tb_company_information.company_name', 'users.name as createdBy')
        ->where('tb_blood_donor_list.id', $id)
        ->first();
        
      	return view('blood_donor.details', compact('donor_details'));
  	}

  	public function edit($id){
  		$id = base64_decode($id);
        $donor_details = DB::table('tb_blood_donor_list')
        ->leftjoin('tb_gender_list', 'tb_blood_donor_list.donor_gender_id', '=', 'tb_gender_list.id')
        ->leftjoin('tb_company_information', 'tb_blood_donor_list.company_id','=', 'tb_company_information.id')
        ->leftjoin('users', 'tb_blood_donor_list.created_by', '=', 'users.id')
        ->select('tb_blood_donor_list.*', 'tb_gender_list.gender_name', 'tb_company_information.company_name', 'users.name as createdBy')
        ->where('tb_blood_donor_list.id', $id)
        ->first();

        $gender_list=DB::table('tb_gender_list')->where('tb_gender_list.status', '=', 1)->orderBy('tb_gender_list.id', 'asc')->get();

      	return view('blood_donor.edit', compact('donor_details', 'gender_list'));
  	}

    public function update(Request $request)
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        if(!empty($request->last_donation_date)){
            $date = $request->last_donation_date;
        }
        else{
            $date = null;
        }

        DB::table('tb_blood_donor_list')->where('tb_blood_donor_list.id', $request->id)->update([
            'donor_name'		=> $request->donor_name,
            'donor_phone' 		=> $request->donor_phone,
            'donor_gender_id' 	=> $request->donor_gender_id,
            'donor_age'			=> $request->donor_age,
            'donor_blood_group'	=> $request->donor_blood_group,
            'last_donation_date'=> $date,
            'donor_address'		=> $request->donor_address,
            'donor_remarks'		=> $request->donor_remarks,
            'company_id' 		=> auth::user()->company_id,
            'created_by' 		=> Auth::user()->id,
            'updated_at'		=> Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Blood donor information has been successfully updated.');
        return redirect()->back();
    }

  	public function destroy($id){
  		$id = base64_decode($id);
        $donor_details = DB::table('tb_blood_donor_list')
        ->where('tb_blood_donor_list.id', $id)
        ->delete();
		
		Session::flash('successMessage','Blood donor information has been successfully destroyed.');
        return redirect()->back();
  	}

  	public function blood_group_summary_list(){
  		$blood_count=DB::table('tb_blood_donor_list')
        ->select('tb_blood_donor_list.donor_blood_group')
        ->groupby('tb_blood_donor_list.donor_blood_group')
        ->orderBy('tb_blood_donor_list.donor_blood_group', 'ASC')
        ->get();

        foreach ($blood_count as $bc) {
        	$count=DB::table('tb_blood_donor_list')
        	->where('tb_blood_donor_list.donor_blood_group', $bc->donor_blood_group)
        	->count();
        	$bc->blood_count = $count;
        }

      	return view('blood_donor.blood_group_summary_list', compact('blood_count'));
  	}

}
