<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PatientListController extends Controller
{
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }

	public function list()
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        $data_list = DB::table('tb_patient_list')
        ->leftjoin('tb_gender_list','tb_patient_list.patient_gender_id','=','tb_gender_list.id')
        ->leftjoin('tb_company_information','tb_patient_list.company_id','=','tb_company_information.id')
        ->select('tb_patient_list.*', 'tb_gender_list.gender_name', 'tb_company_information.company_name')
        ->where('tb_patient_list.company_id', $user_company)
        ->get();

        return view('patient_list.list', compact('data_list'));
    }

	public function details($id)
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;
        $id = base64_decode($id);

        $petient_details = DB::table('tb_patient_list')
        ->leftjoin('tb_gender_list','tb_patient_list.patient_gender_id','=','tb_gender_list.id')
        ->leftjoin('tb_company_information','tb_patient_list.company_id','=','tb_company_information.id')
        ->select('tb_patient_list.*', 'tb_gender_list.gender_name', 'tb_company_information.company_name')
        ->where([['tb_patient_list.company_id', $user_company], ['tb_patient_list.id', $id]])
        ->first();
        // dd($petient_details);
        return view('patient_list.details', compact('petient_details'));
    }

  	public function create()
    {
      	$gender_list=DB::table('tb_gender_list')->where('tb_gender_list.status', '=', 1)->orderBy('tb_gender_list.id', 'asc')->get();
 
        return view('patient_list.create', compact('gender_list'));
    }

  	public function store(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

	     $year = date('Y');
	     $data = DB::table('tb_patient_list')->select('patient_id')->orderBy('id', 'desc')->whereYear('created_at', $year)->first(); 
	     if(!empty($data->patient_id)){
	         $arr1 = $data->patient_id;
	         $arr1++;
	     }else{
	         $arr1 = $year."01";
	     }
      	
      	$patient_id = $arr1;

      	$photoName = NULL;
      	if ($request->hasFile('patient_photo')) {
    		$photoName = $patient_id.time().'.'.$request->patient_photo->getClientOriginalExtension();
          	$request->patient_photo->move('patient_photo', $photoName);
         }

  		$str = DB::table('tb_patient_list')->insertGetId([
            'patient_id' 				 => $patient_id,
            'patient_name' 				 => $request->patient_name,
            'patient_father_name' 		 => $request->patient_father_name,
            'patient_mother_name' 		 => $request->patient_mother_name,
            'patient_gender_id' 		 => $request->patient_gender_id,
            'patient_age' 		    	 => $request->patient_age,
            'patient_address' 			 => $request->patient_address,
            'patient_blood_group' 		 => $request->patient_blood_group,
            'patient_phone' 			 => $request->patient_phone,
            'patient_email' 			 => $request->patient_email,
            'patient_profession'		 => $request->patient_profession,
            'emergency_contact_name'	 => $request->emergency_contact_name,
            'emergency_contact_phone' 	 => $request->emergency_contact_phone,
            'emergency_contact_relation' => $request->emergency_contact_relation,
            'patient_photo' 			 => $photoName,
            'patient_remarks' 			 => $request->patient_remarks,
            'company_id' 				 => auth::user()->company_id,
            'status' 					 => 1,
            'created_by' 				 => Auth::user()->id,
            'created_at'  				 => Carbon::now()->toDateTimeString(),
            'updated_at'  				 => Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Patient information has been successfully added.');
        return redirect()->route('patient.details', base64_encode($str));
  	}

	public function edit($id)
    {
        $this->company_selector();
        $user_company = Auth::user()->company_id;
        $id = base64_decode($id);

      	$gender_list=DB::table('tb_gender_list')->where('tb_gender_list.status', '=', 1)->orderBy('tb_gender_list.id', 'asc')->get();

        $petient_details = DB::table('tb_patient_list')
        ->leftjoin('tb_gender_list','tb_patient_list.patient_gender_id','=','tb_gender_list.id')
        ->leftjoin('tb_company_information','tb_patient_list.company_id','=','tb_company_information.id')
        ->select('tb_patient_list.*', 'tb_gender_list.gender_name', 'tb_company_information.company_name')
        ->where([['tb_patient_list.company_id', $user_company], ['tb_patient_list.id', $id]])
        ->first();
        // dd($petient_details);
        return view('patient_list.edit', compact('petient_details', 'gender_list'));
    }

  	public function update(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

      	if ($request->hasFile('patient_photo')) {
      		$photoName = NULL;
    		$photoName = $patient_id.time().'.'.$request->patient_photo->getClientOriginalExtension();
          	$request->patient_photo->move('patient_photo', $photoName);

  			DB::table('tb_patient_list')->where('id', $request->id)->update([
            	'patient_photo' => $photoName,
        	]);
         }

  		DB::table('tb_patient_list')->where('id', $request->id)->update([
            'patient_name' 				 => $request->patient_name,
            'patient_father_name' 		 => $request->patient_father_name,
            'patient_mother_name' 		 => $request->patient_mother_name,
            'patient_gender_id' 		 => $request->patient_gender_id,
            'patient_age' 		    	 => $request->patient_age,
            'patient_address' 			 => $request->patient_address,
            'patient_blood_group' 		 => $request->patient_blood_group,
            'patient_phone' 			 => $request->patient_phone,
            'patient_email' 			 => $request->patient_email,
            'patient_profession'		 => $request->patient_profession,
            'emergency_contact_name'	 => $request->emergency_contact_name,
            'emergency_contact_phone' 	 => $request->emergency_contact_phone,
            'emergency_contact_relation' => $request->emergency_contact_relation,
            'patient_remarks' 			 => $request->patient_remarks,
            'company_id' 				 => auth::user()->company_id,
            'status' 					 => 1,
            'created_by' 				 => Auth::user()->id,
            'created_at'  				 => Carbon::now()->toDateTimeString(),
            'updated_at'  				 => Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Patient information has been successfully updated.');
        return redirect()->route('patient.details', base64_encode($request->id));
  	}

}
