<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccommodationCategoryListController extends Controller
{
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }

	public function list()
    {   
        $this->company_selector();
        $user_company = Auth::user()->company_id;

        $data_list = DB::table('tb_accommodation_category_list')
        ->leftjoin('tb_company_information','tb_accommodation_category_list.company_id','=','tb_company_information.id')
        ->select('tb_accommodation_category_list.*', 'tb_company_information.company_name')
        ->where('tb_accommodation_category_list.company_id', $user_company)
        ->get();

        foreach($data_list as $dl){
			$count=DB::table('tb_accommodation_list')
        	->where('tb_accommodation_list.accommodation_category_id', $dl->id)
        	->count();
        	$dl->accommodation_count = $count;
        }

        return view('accommodation.accommodation_category.list', compact('data_list'));
    }

  	public function store(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

  		DB::table('tb_accommodation_category_list')->insert([
            'category_name'	=> $request->category_name,
            'remarks' 		=> $request->remarks,
            'status' 		=> $request->status,
            'company_id' 	=> auth::user()->company_id,
            'created_by' 	=> Auth::user()->id,
            'created_at'  	=> Carbon::now()->toDateTimeString(),
            'updated_at'  	=> Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage', 'Accommodation category information has been successfully added.');
        return redirect()->back();
  	}

	public function edit($id)
    {   
        $this->company_selector();
        $user_company = Auth::user()->company_id;
        $id = base64_decode($id);
        $details = DB::table('tb_accommodation_category_list')
        ->leftjoin('tb_company_information','tb_accommodation_category_list.company_id','=','tb_company_information.id')
        ->select('tb_accommodation_category_list.*', 'tb_company_information.company_name')
        ->where([['tb_accommodation_category_list.id', $id], ['tb_accommodation_category_list.company_id', $user_company]])
        ->first();
        return response()->json($details);
    }

  	public function update(Request $request)
  	{
        $this->company_selector();
        $user_company = Auth::user()->company_id;

  		DB::table('tb_accommodation_category_list')->where('tb_accommodation_category_list.id', $request->id)->update([
            'category_name'	=> $request->category_name,
            'remarks' 		=> $request->remarks,
            'status' 		=> $request->status,
            'company_id' 	=> auth::user()->company_id,
            'created_by' 	=> Auth::user()->id,
            'updated_at'  	=> Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Accommodation category information has been successfully updated.');
        return redirect()->back();
  	}

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        $count = DB::table('tb_accommodation_list')->where('accommodation_category_id', '=', $id)->count();

        if($count>0){
        	Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
        	DB::table('tb_accommodation_category_list')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Accommodation category has been successfully destroyed.');
        }
        return redirect()->back();
    }
}