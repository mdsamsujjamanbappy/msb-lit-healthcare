<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function designation_list()
    {   
        $designation_list = DB::table('tb_designation_list')->get();
        return view('designation.designation_list',compact('designation_list'));
    }

    public function store(Request $request)
    {
        $str = DB::table('tb_designation_list')->insert([
            'designation_name'	=>	$request->designation_name,
            'remarks'			=>	$request->remarks,
            'status'			=>	$request->status,
      		'created_by'        => 	Auth::user()->id,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Designation has been successfully added.');
        return redirect()->back();
    }

    public function edit($id)
    {   
    	$id=base64_decode($id);
    	$department_info = DB::table('tb_designation_list')->where('id', '=', $id)->first();
        return response()->json($department_info);
    }

    public function update(Request $request)
    {
        $str = DB::table('tb_designation_list')->where('id', '=', $request->id)->update([
            'designation_name'	=>	$request->designation_name,
            'remarks'			=>	$request->remarks,
            'status'			=>	$request->status,
      		'created_by'        => 	Auth::user()->id,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

	    Session::flash('successMessage','Designation has been successfully updated.');
	    return redirect()->back();
    }

    public function destroy($id)
    {   
    	$id=base64_decode($id);
        $emp_department_count = DB::table('tb_employee_list')->where('emp_designation_id', '=', $id)->count();

        if($emp_department_count>0){
        	Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
        	$company_info = DB::table('tb_designation_list')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Designation has been successfully destroyed.');
        }

        return redirect()->back();
    }
}
