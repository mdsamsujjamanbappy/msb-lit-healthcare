<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Session;
use Validator;
use Datatables;
use Carbon\Carbon;
use App\Mail\WelcomeMail;
use App\Jobs\SendEmailTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    public function Dashboard(){
        $employee_count = DB::table('tb_employee_list')
        ->count();

        return view('dashboard.dashboard', compact('employee_count'));
    }

    public function mail(){
        $details['email'] = 'system@dongyi-bd.com';

        dispatch(new SendEmailTest($details));

        dd('done');
    }

    public function update_logs(){
        return view('update_logs');
    }
}
