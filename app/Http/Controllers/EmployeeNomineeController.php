<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeNomineeController extends Controller
{

  public function employee_nominee_store(Request $request)
  {
  	if ($request->hasFile('nominee_photo')) {
      $nominee_photo = $request->ref_id."_".time().rand(0,9).'.'.$request->nominee_photo->getClientOriginalExtension();
      $request->nominee_photo->move('employee_nominee', $nominee_photo);
    }else{
    	 $nominee_photo = NULL;
    }

  	if ($request->hasFile('nominee_attachment')) {
      $nominee_attachment = $request->ref_id."_".time().rand(0,9).'.'.$request->nominee_attachment->getClientOriginalExtension();
      $request->nominee_attachment->move('employee_nominee', $nominee_attachment);
    }else{
    	 $nominee_attachment = NULL;
    }

    $employee_nominee = DB::table('tb_employee_nominee')->insert([
      'emp_id'        	   =>  $request->ref_id,
      'nominee_name'       =>  $request->nominee_name,
      'nominee_phone'      =>  $request->nominee_phone,
      'nominee_relation'   =>  $request->nominee_relation,
      'nominee_details'    =>  $request->nominee_details,
      'nominee_address'    =>  $request->nominee_address,
      'nominee_photo'      =>  $nominee_photo,
      'nominee_attachment' =>  $nominee_attachment,
      'created_by'         =>  Auth::user()->id,
      'created_at'         =>  Carbon::now()->toDateTimeString(),
      'updated_at'         =>  Carbon::now()->toDateTimeString()
    ]);

    Session::flash('successMessage','Employee nominee has been successfully added.');
    return redirect()->back();
  }

    public function employee_nominee_destroy($id)
    {   
    	$id=base64_decode($id);
    	$employee_nominee_info = DB::table('tb_employee_nominee')->where('id', '=', $id)->first();
    	$employee_nominee = DB::table('tb_employee_nominee')->where('id', '=', $id)->delete();
    	
    	if(!empty($employee_nominee)){
	    	if(!empty($employee_nominee_info->nominee_photo)){
	            if($employee_nominee_info->nominee_photo != "default.png"){
	              $file_path = "employee_nominee"."/".$employee_nominee_info->nominee_photo;
	              if(file_exists($file_path)){
	                unlink($file_path);
	              }
	            }
	        }

	        if(!empty($employee_nominee_info->nominee_attachment)){
	            if($employee_nominee_info->nominee_attachment != "default.png"){
	              $file_path1 = "employee_nominee"."/".$employee_nominee_info->nominee_attachment;
	              if(file_exists($file_path1)){
	                unlink($file_path1);
	              }
	            }
	        }
	    }

    	Session::flash('successMessage','Employee nominee has been successfully destroyed.');
        return redirect()->back();
    }
	
}
