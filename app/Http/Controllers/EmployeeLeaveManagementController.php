<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use DateTime;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class EmployeeLeaveManagementController extends Controller
{
	public function leave_type_list()
    {
        $leave_type_list = DB::table('tb_employee_leave_type_setting')
        ->leftjoin('tb_company_information','tb_employee_leave_type_setting.company_id','=','tb_company_information.id')
        ->select('tb_employee_leave_type_setting.*', 'tb_company_information.company_name')
        ->orderBy('leave_type_name', 'asc')->get();

        $user_company = Auth::user()->company_id;
        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.company_name', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.company_name', 'asc')->get();
        }

        return view('leave_management.leave_type.leave_type_list', compact('leave_type_list', 'company_list'));
    }

    public function leave_type_store(Request $request)
    {
        $str = DB::table('tb_employee_leave_type_setting')->insert([
            'company_id'		=>	$request->company_id,
            'leave_type_name'	=>	$request->leave_type_name,
            'total_leave_days'	=>	$request->total_leave_days,
            'remarks'			=>	$request->remarks,
            'status'			=>	$request->status,
      		'created_by'        => 	Auth::user()->id,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','New leave type has been successfully added.');
        return redirect()->back();
    }

    public function leave_type_edit($id)
    {   
    	$id=base64_decode($id);
    	$info = DB::table('tb_employee_leave_type_setting')->where('id', '=', $id)->first();
        return response()->json($info);
    }
    
    public function leave_type_update(Request $request)
    {
        $str = DB::table('tb_employee_leave_type_setting')->where('id', '=', $request->id)->update([
            'company_id'		=>	$request->company_id,
            'leave_type_name'	=>	$request->leave_type_name,
            'total_leave_days'	=>	$request->total_leave_days,
            'remarks'			=>	$request->remarks,
            'status'			=>	$request->status,
      		'created_by'        => 	Auth::user()->id,
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Leave type information has been successfully updated.');
        return redirect()->back();
    }

    public function leave_type_destroy($id)
    {   
    	$id=base64_decode($id);
        $count = DB::table('tb_employee_leave_application')->where('leave_type_id', '=', $id)->count();
        if($count>0){
        	Session::flash('failedMessage','Destroy request failed. There are already some data use this resource.');
        }else{
        	$info = DB::table('tb_employee_leave_type_setting')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Leave type has been successfully destroyed.');
        }

        return redirect()->back();
    }

	public function holidays_list()
    {
        $user_company = Auth::user()->company_id;

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
	        $holidays_list = DB::table('tb_holidays_observances_leave')
	        ->leftjoin('tb_company_information','tb_holidays_observances_leave.company_id','=','tb_company_information.id')
	        ->select('tb_holidays_observances_leave.*', 'tb_company_information.company_name')
	        ->orderBy('tb_holidays_observances_leave.company_id', 'asc')
	        ->orderBy('tb_holidays_observances_leave.start_date', 'asc')
	        ->get();
    	}else{
    		$holidays_list = DB::table('tb_holidays_observances_leave')
	        ->leftjoin('tb_company_information','tb_holidays_observances_leave.company_id','=','tb_company_information.id')
	        ->select('tb_holidays_observances_leave.*', 'tb_company_information.company_name')
	        ->where('tb_holidays_observances_leave.company_id', '=', $user_company)
	        ->orderBy('tb_holidays_observances_leave.company_id', 'asc')
	        ->orderBy('tb_holidays_observances_leave.start_date', 'asc')
	        ->get();
    	}

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.company_name', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.company_name', 'asc')->get();
        }

        return view('leave_management.holidays_observances.list', compact('holidays_list', 'company_list'));
    }

    public function holiday_store(Request $request)
    {
        $str = DB::table('tb_holidays_observances_leave')->insert([
            'company_id'		=>	$request->company_id,
            'holiday_title'		=>	$request->holiday_title,
            'start_date'		=>	$request->start_date,
            'end_date'			=>	$request->end_date,
            'remarks'			=>	$request->remarks,
            'status'			=>	$request->status,
      		'created_by'        => 	Auth::user()->id,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','New holiday/observance information has been successfully added.');
        return redirect()->back();
    }

    public function holidays_observances_update(Request $request)
    {
    	$start_date = new DateTime($request->start_date);
		$end_date = new DateTime($request->end_date);

		if ( $start_date > $end_date ) {
			Session::flash('failedMessage','Invalid Date Range..');
		}else{
	        $str = DB::table('tb_holidays_observances_leave')->where('id', '=', $request->id)->update([
	            'company_id'		=>	$request->company_id,
	            'holiday_title'		=>	$request->holiday_title,
	            'start_date'		=>	$request->start_date,
	            'end_date'			=>	$request->end_date,
	            'remarks'			=>	$request->remarks,
	            'status'			=>	$request->status,
	      		'created_by'        => 	Auth::user()->id,
	            'updated_at'		=>	Carbon::now()->toDateTimeString()
	        ]);

	        Session::flash('successMessage','Holiday/observance information has been successfully updated.');
	    }
        return redirect()->back();
    }

    public function holidays_observances_edit($id)
    {   
    	$id=base64_decode($id);
    	$info = DB::table('tb_holidays_observances_leave')->where('id', '=', $id)->first();
        return response()->json($info);
    }
    
    public function holidays_observances_destroy($id)
    {   
    	$id=base64_decode($id);
        	$info = DB::table('tb_holidays_observances_leave')->where('id', '=', $id)->delete();
        	Session::flash('successMessage','Selected Holidays/Observances has been successfully destroyed.');

        return redirect()->back();
    }

    public function leave_assign_view()
    {
        $user_company = Auth::user()->company_id;

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.company_name', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.company_name', 'asc')->get();
        }

        $department_list = DB::table('tb_department_list')
        ->select('tb_department_list.id','tb_department_list.department_name')
        ->orderBy('tb_department_list.department_name', 'ASC')
        ->where('status', '=', 1)
        ->get();

        $designation_list = DB::table('tb_designation_list')
        ->select('tb_designation_list.id','tb_designation_list.designation_name')
        ->orderBy('tb_designation_list.designation_name', 'ASC')
        ->where('status', '=', 1)
        ->get();

        return view('leave_management.assign_leave',compact('company_list', 'department_list', 'designation_list'));
    }

    public function get_ajax_employee_list($company_id, $department_id)
    {
    	if($department_id=='all'){
	        $employee_list = DB::table('tb_employee_list')
	        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
	        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
	        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
	        ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.emp_card_number', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_joining_date', 'tb_employee_list.emp_account_status', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
	        ->orderBy('tb_employee_list.employee_id', 'asc')
	        ->where([['tb_employee_list.company_id', '=', $company_id], ['tb_employee_list.emp_account_status', '=', 1]])
	        ->get();
    	}else{
    		$employee_list = DB::table('tb_employee_list')
	        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
	        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
	        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
	        ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.emp_card_number', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_joining_date', 'tb_employee_list.emp_account_status', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
	        ->orderBy('tb_employee_list.employee_id', 'asc')
	        ->where([['tb_employee_list.company_id', '=', $company_id], ['tb_employee_list.emp_department_id', '=', $department_id], ['tb_employee_list.emp_account_status', '=', 1]])
	        ->get();
    	}

        return view('leave_management.get_ajax_employee_list',compact('employee_list'));
    }
 
    public function get_ajax_leave_type($emp_id){
    	$employee_info = DB::table('tb_employee_list')
	        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
	        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
	        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
	        ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.company_id', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_joining_date', 'tb_employee_list.emp_account_status', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
	        ->orderBy('tb_employee_list.employee_id', 'asc')
	        ->where('tb_employee_list.id', '=', $emp_id)
	        ->first();
		
		$leave_type_list = DB::table('tb_employee_leave_type_setting')
	        ->leftjoin('tb_company_information','tb_employee_leave_type_setting.company_id','=','tb_company_information.id')
	        ->select('tb_employee_leave_type_setting.*', 'tb_company_information.company_name')
	        ->orderBy('leave_type_name', 'asc')
	        ->where('tb_employee_leave_type_setting.company_id', '=', $employee_info->company_id)
	        ->get();

        return view('leave_management.get_ajax_leave_type',compact('leave_type_list'));
    }

	public function get_ajax_employee_available_leave($leave_id,$emp_id)
    {
        $leave_type= DB::table('tb_employee_leave_type_setting')
			        ->where('id', '=', $leave_id)
			        ->where('status', '=', 1)
			        ->first(['total_leave_days']);

        $total_leave_taken=DB::table('tb_employee_leave_application')
		                    ->where('employee_id', '=', $emp_id)
		                    ->where('leave_type_id', '=', $leave_id)
		                    ->whereYear('created_at', Carbon::now()->year)
		                    ->sum('actual_days');

        $available = $leave_type->total_leave_days - $total_leave_taken;
        if ($available <= 0) {
            return response()->json(['error' => 'No leave available']);
        }else {
            return response()->json(['leave' => $available]);
        }
    }

	public function leave_assign_store(Request $request)
	{
        $str = DB::table('tb_employee_leave_application')->insert([
            'employee_id'		=>	$request->employee_id,
            'leave_type_id'		=>	$request->leave_type_id,
            'leave_starting_date'=>	$request->leave_starting_date,
            'leave_ending_date'	=>	$request->leave_ending_date,
            'actual_days'		=>	$request->actual_days,
            'description'		=>	$request->description,
            'status'			=>	0,
      		'created_by'        => 	Auth::user()->id,
            'created_at'		=>	Carbon::now()->toDateTimeString(),
            'updated_at'		=>	Carbon::now()->toDateTimeString()
        ]);

	    Session::flash('successMessage','Leave has been successfully submitted for approval.');
        return redirect()->back();
	}


    public function employee_new_leave_request()
    {
        $user_company = Auth::user()->company_id;

        $employee_details = DB::table('tb_employee_list')
        ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
        ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
        ->leftjoin('users','tb_employee_list.id','=','users.ref_id')
        ->select('tb_employee_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name', 'tb_gender_list.gender_name', 'users.user_type')
        ->where('tb_employee_list.id', '=', Auth::user()->ref_id)
        ->first();
        // dd($employee_details);
        $leave_type_list = DB::table('tb_employee_leave_type_setting')
            ->leftjoin('tb_company_information','tb_employee_leave_type_setting.company_id','=','tb_company_information.id')
            ->select('tb_employee_leave_type_setting.*', 'tb_company_information.company_name')
            ->orderBy('leave_type_name', 'asc')
            ->where('tb_employee_leave_type_setting.company_id', '=', $user_company)
            ->get();

        return view('leave_management.employee_new_leave_request',compact('employee_details', 'leave_type_list'));
    }

    
    public function pending_leave_list()
    {
        $user_company = Auth::user()->company_id;

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
            $leave_list = DB::table('tb_employee_leave_application')
            ->leftjoin('tb_employee_list','tb_employee_leave_application.employee_id','=','tb_employee_list.id')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->leftjoin('tb_employee_leave_type_setting','tb_employee_leave_application.leave_type_id','=','tb_employee_leave_type_setting.id')
            ->select('tb_employee_leave_application.*', 'tb_employee_list.employee_id as employee_main_id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_company_information.company_name', 'tb_employee_leave_type_setting.leave_type_name')
            ->where('tb_employee_leave_application.status', '=', 0)
            ->get();
        }else{  
            $leave_list = DB::table('tb_employee_leave_application')
            ->leftjoin('tb_employee_list','tb_employee_leave_application.employee_id','=','tb_employee_list.id')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->leftjoin('tb_employee_leave_type_setting','tb_employee_leave_application.leave_type_id','=','tb_employee_leave_type_setting.id')
            ->select('tb_employee_leave_application.*', 'tb_employee_list.employee_id as employee_main_id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_company_information.company_name', 'tb_employee_leave_type_setting.leave_type_name')
            ->where([['tb_employee_leave_application.status', '=', 0], ['tb_employee_list.company_id', '=', $user_company]])
            ->get();
        }
        // dd($leave_list);
        return view('leave_management.pending_leave_list',compact('leave_list'));
    }

    public function pending_leave_approve($id)
    {
        $id = base64_decode($id);
        $str = DB::table('tb_employee_leave_application')->where('id', '=', $id)->update([
            'status'            =>  1,
            'approved_by'       =>  Auth::user()->id,
            'updated_at'        =>  Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Leave has been successfully approved.');
        return redirect()->back();
    }

    public function pending_leave_reject($id)
    {
        $id = base64_decode($id);
        $str = DB::table('tb_employee_leave_application')->where('id', '=', $id)->update([
            'status'            =>  2,
            'approved_by'       =>  Auth::user()->id,
            'updated_at'        =>  Carbon::now()->toDateTimeString()
        ]);

        Session::flash('successMessage','Leave has been successfully rejected.');
        return redirect()->back();
    }

    public function approved_leave_list()
    {
        $user_company = Auth::user()->company_id;

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
            $leave_list = DB::table('tb_employee_leave_application')
            ->leftjoin('tb_employee_list','tb_employee_leave_application.employee_id','=','tb_employee_list.id')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->leftjoin('tb_employee_leave_type_setting','tb_employee_leave_application.leave_type_id','=','tb_employee_leave_type_setting.id')
            ->select('tb_employee_leave_application.*', 'tb_employee_list.employee_id as employee_main_id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_company_information.company_name', 'tb_employee_leave_type_setting.leave_type_name')
            ->where('tb_employee_leave_application.status', '=', 1)
            ->get();
        }else{  
            $leave_list = DB::table('tb_employee_leave_application')
            ->leftjoin('tb_employee_list','tb_employee_leave_application.employee_id','=','tb_employee_list.id')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->leftjoin('tb_employee_leave_type_setting','tb_employee_leave_application.leave_type_id','=','tb_employee_leave_type_setting.id')
            ->select('tb_employee_leave_application.*', 'tb_employee_list.employee_id as employee_main_id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_company_information.company_name', 'tb_employee_leave_type_setting.leave_type_name')
            ->where([['tb_employee_leave_application.status', '=', 1], ['tb_employee_list.company_id', '=', $user_company]])
            ->get();
        }
        // dd($leave_list);
        return view('leave_management.approved_leave_list',compact('leave_list'));
    }

    public function rejected_leave_list()
    {
        $user_company = Auth::user()->company_id;

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
            $leave_list = DB::table('tb_employee_leave_application')
            ->leftjoin('tb_employee_list','tb_employee_leave_application.employee_id','=','tb_employee_list.id')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->leftjoin('tb_employee_leave_type_setting','tb_employee_leave_application.leave_type_id','=','tb_employee_leave_type_setting.id')
            ->select('tb_employee_leave_application.*', 'tb_employee_list.employee_id as employee_main_id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_company_information.company_name', 'tb_employee_leave_type_setting.leave_type_name')
            ->where('tb_employee_leave_application.status', '=', 2)
            ->get();
        }else{  
            $leave_list = DB::table('tb_employee_leave_application')
            ->leftjoin('tb_employee_list','tb_employee_leave_application.employee_id','=','tb_employee_list.id')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_gender_list','tb_employee_list.emp_gender_id','=','tb_gender_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->leftjoin('tb_employee_leave_type_setting','tb_employee_leave_application.leave_type_id','=','tb_employee_leave_type_setting.id')
            ->select('tb_employee_leave_application.*', 'tb_employee_list.employee_id as employee_main_id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_company_information.company_name', 'tb_employee_leave_type_setting.leave_type_name')
            ->where([['tb_employee_leave_application.status', '=', 2], ['tb_employee_list.company_id', '=', $user_company]])
            ->get();
        }
        // dd($leave_list);
        return view('leave_management.rejected_leave_list',compact('leave_list'));
    }

    public function leave_balance()
    {
        $user_company = Auth::user()->company_id;

        if(checkPermission(['super_admin']) || checkPermission(['group_admin']) || checkPermission(['group_director']) ){
            $employee_list = DB::table('tb_employee_list')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.emp_card_number', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_joining_date', 'tb_employee_list.emp_account_status', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
            ->orderBy('tb_employee_list.employee_id', 'asc')
            ->get();
        }else{  
            $employee_list = DB::table('tb_employee_list')
            ->leftjoin('tb_department_list','tb_employee_list.emp_department_id','=','tb_department_list.id')
            ->leftjoin('tb_designation_list','tb_employee_list.emp_designation_id','=','tb_designation_list.id')
            ->leftjoin('tb_company_information','tb_employee_list.company_id','=','tb_company_information.id')
            ->select('tb_employee_list.id', 'tb_employee_list.emp_first_name', 'tb_employee_list.emp_last_name', 'tb_employee_list.emp_account_status', 'tb_employee_list.emp_card_number', 'tb_employee_list.emp_phone', 'tb_employee_list.emp_joining_date', 'tb_employee_list.emp_account_status', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_employee_list.employee_id', 'tb_company_information.company_name')
            ->orderBy('tb_employee_list.employee_id', 'asc')
            ->where('tb_employee_list.company_id', '=', $user_company)
            ->get();
        }
            $leave_type_setting = DB::table('tb_employee_leave_type_setting')->where('status', 1)->get();
        // dd($leave_list);
        return view('leave_management.leave_balance',compact('employee_list', 'leave_type_setting'));
    }

}
