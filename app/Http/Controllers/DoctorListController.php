<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DoctorListController extends Controller
{ 
    public function company_selector()
    {
       if(empty(Auth::user()->company_id)){
         redirect()->route('company_information.selector_view')->send();
       }
    }
    
  	public function doctor_list(){
        $this->company_selector();
        $user_company = Auth::user()->company_id;
        
        $doctor_list = DB::table('tb_doctor_list')
        ->leftjoin('tb_department_list','tb_doctor_list.doctor_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_doctor_list.doctor_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_company_information','tb_doctor_list.company_id','=','tb_company_information.id')
        ->select('tb_doctor_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_company_information.company_name')
        ->orderBy('tb_doctor_list.id', 'asc')
        ->where('tb_doctor_list.company_id', $user_company)
        ->get();
        
      	return view('doctor_list.list', compact('doctor_list'));
  	}

  	public function create()
  	{ 
      $department_list=DB::table('tb_department_list')->where('tb_department_list.status', '=', 1)->orderBy('tb_department_list.department_name', 'asc')->get();
      
      $designation_list=DB::table('tb_designation_list')->where('tb_designation_list.status', '=', 1)->orderBy('tb_designation_list.designation_name', 'asc')->get();

        $user_company = Auth::user()->company_id;
        if(!empty($aclList[14][1])){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.company_name', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.company_name', 'asc')->get();
        }
       return view('doctor_list.create', compact('department_list', 'designation_list', 'company_list'));
    }

  	public function store(Request $request){

        $this->company_selector();
        $user_company = Auth::user()->company_id;

      	$photoName = NULL;
      	if ($request->hasFile('doctor_photo')) {
    		$photoName = rand(0,999).time().'.'.$request->doctor_photo->getClientOriginalExtension();
          	$request->doctor_photo->move('doctor_photo', $photoName);
         }

  		DB::table('tb_doctor_list')->insert([
            'doctor_name' 				     => $request->doctor_name,
            'doctor_bangla_name' 		   => $request->doctor_bangla_name,
            'doctor_department_id' 		 => $request->doctor_department_id,
            'doctor_designation_id' 	 => $request->doctor_designation_id,
            'doctor_qualification' 		 => $request->doctor_qualification,
            'doctor_bangla_qualification' => $request->doctor_bangla_qualification,
            'doctor_email' 				     => $request->doctor_email,
            'doctor_joindate' 			   => $request->doctor_joindate,
            'doctor_phone' 				     => $request->doctor_phone,
            'doctor_registration_no'	 => $request->doctor_registration_no,
            'doctor_room' 				     => $request->doctor_room,
            'doctor_app_fee' 			     => $request->doctor_app_fee,
            'doctor_address' 			     => $request->doctor_address,
            'doctor_photo' 				     => $photoName,
            'remarks' 					       => $request->remarks,
            'company_id' 				       => auth::user()->company_id,
            'created_by' 				       => Auth::user()->id,
            'created_at'  				     => Carbon::now()->toDateTimeString(),
            'updated_at'  				     => Carbon::now()->toDateTimeString(),
        ]);

        Session::flash('successMessage','Doctor information has been successfully added.');
        return redirect()->back();
  	}

  	public function details($id){
  		$id = base64_decode($id);
        $doctor_details = DB::table('tb_doctor_list')
        ->leftjoin('tb_department_list','tb_doctor_list.doctor_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_doctor_list.doctor_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_company_information','tb_doctor_list.company_id','=','tb_company_information.id')
        ->select('tb_doctor_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_company_information.company_name')
        ->where('tb_doctor_list.id', $id)
        ->first();
        
      	return view('doctor_list.details', compact('doctor_details'));
  	}

  	public function edit($id)
  	{ 
  		$id = base64_decode($id);
  		$doctor_details = DB::table('tb_doctor_list')
        ->leftjoin('tb_department_list','tb_doctor_list.doctor_department_id','=','tb_department_list.id')
        ->leftjoin('tb_designation_list','tb_doctor_list.doctor_designation_id','=','tb_designation_list.id')
        ->leftjoin('tb_company_information','tb_doctor_list.company_id','=','tb_company_information.id')
        ->select('tb_doctor_list.*', 'tb_department_list.department_name', 'tb_designation_list.designation_name', 'tb_company_information.company_name')
        ->where('tb_doctor_list.id', $id)
        ->first();
      	$department_list=DB::table('tb_department_list')->where('tb_department_list.status', '=', 1)->orderBy('tb_department_list.department_name', 'asc')->get();
      
      	$designation_list=DB::table('tb_designation_list')->where('tb_designation_list.status', '=', 1)->orderBy('tb_designation_list.designation_name', 'asc')->get();

        $user_company = Auth::user()->company_id;
        if(!empty($aclList[14][1])){
        $company_list=DB::table('tb_company_information')->where('tb_company_information.status', '=', 1)->orderBy('tb_company_information.company_name', 'asc')->get();
        }else{
        $company_list=DB::table('tb_company_information')->where([['tb_company_information.status', '=', 1], ['tb_company_information.id', '=', $user_company]])->orderBy('tb_company_information.company_name', 'asc')->get();
        }
       return view('doctor_list.edit', compact('doctor_details', 'department_list', 'designation_list', 'company_list'));
    }

  	public function update(Request $request){

        $this->company_selector();
        $user_company = Auth::user()->company_id;

      	$photoName = NULL;
      	if ($request->hasFile('doctor_photo')) {
    		$photoName = rand(0,999).time().'.'.$request->doctor_photo->getClientOriginalExtension();
          	$request->doctor_photo->move('doctor_photo', $photoName);

  			DB::table('tb_doctor_list')->where('tb_doctor_list.id', $request->id)->update([
            	'doctor_photo' 	=> $photoName,
        	]);
         }

  		DB::table('tb_doctor_list')->where('tb_doctor_list.id', $request->id)->update([
            'doctor_name' 				=> $request->doctor_name,
            'doctor_bangla_name' 		=> $request->doctor_bangla_name,
            'doctor_department_id' 		=> $request->doctor_department_id,
            'doctor_designation_id' 	=> $request->doctor_designation_id,
            'doctor_qualification' 		=> $request->doctor_qualification,
            'doctor_bangla_qualification' => $request->doctor_bangla_qualification,
            'doctor_email' 				=> $request->doctor_email,
            'doctor_joindate' 			=> $request->doctor_joindate,
            'doctor_phone' 				=> $request->doctor_phone,
            'doctor_registration_no'	=> $request->doctor_registration_no,
            'doctor_room' 				=> $request->doctor_room,
            'doctor_app_fee' 			=> $request->doctor_app_fee,
            'doctor_address' 			=> $request->doctor_address,
            'remarks' 					=> $request->remarks,
            'company_id' 				=> auth::user()->company_id,
            'created_by' 				=> Auth::user()->id,
            'updated_at'  				=> Carbon::now()->toDateTimeString(),
        ]);


        Session::flash('successMessage','Doctor information has been successfully updated.');
        return redirect()->back();
  	}
}
