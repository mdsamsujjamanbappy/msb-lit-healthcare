<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeEducationController extends Controller
{
	  public function employee_education_store(Request $request)
	  {
	  	if ($request->hasFile('emp_attachment')) {
	      $emp_attachment = $request->ref_id."_".time().rand(0,9).'.'.$request->emp_attachment->getClientOriginalExtension();
	      $request->emp_attachment->move('employee_education', $emp_attachment);
	    }else{
	    	 $emp_attachment = NULL;
	    }

	    $employee_nominee = DB::table('tb_employee_education_info')->insert([
	      'emp_id'        	   	 =>  $request->ref_id,
	      'emp_exam_title'       =>  $request->emp_exam_title,
	      'emp_institution_name' =>  $request->emp_institution_name,
	      'emp_result'   		 =>  $request->emp_result,
	      'emp_scale'    		 =>  $request->emp_scale,
	      'emp_passing_year'     =>  $request->emp_passing_year,
	      'emp_attachment'       =>  $emp_attachment,
	      'created_by'         	 =>  Auth::user()->id,
	      'created_at'           =>  Carbon::now()->toDateTimeString(),
	      'updated_at'         	 =>  Carbon::now()->toDateTimeString()
	    ]);

	    Session::flash('successMessage','Employee education information has been successfully added.');
	    return redirect()->back();
	  }

	public function employee_education_destroy($id)
	{   
		$id=base64_decode($id);
		$employee_education_info = DB::table('tb_employee_education_info')->where('id', '=', $id)->first();
		$employee_education = DB::table('tb_employee_education_info')->where('id', '=', $id)->delete();
		
		if(!empty($employee_education)){
	    	if(!empty($employee_education_info->emp_attachment)){
	            if($employee_education_info->emp_attachment != "default.png"){
	              $file_path = "employee_education"."/".$employee_education_info->emp_attachment;
	              if(file_exists($file_path)){
	                unlink($file_path);
	              }
	            }
	        }
	    }

		Session::flash('successMessage','Employee education information has been successfully destroyed.');
	    return redirect()->back();
	}
	
}
