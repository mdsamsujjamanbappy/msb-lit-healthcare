<?php
function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->user_type);
    foreach ($permissions as $key => $value) {
        if($value == $userAccess){
            return true;
        }
    }
    return false;
}

function getMyPermission($id)
{
    switch ($id) {

            case 1:
            return 'super_admin';
            break;

            case 2:
            return 'group_admin';
            break;

            case 3:
            return 'group_director';
            break;

            case 4:
            return 'company_director';
            break;

            case 5:
            return 'company_admin';
            break;

            case 6:
            return 'company_accountant';
            break;

            case 7:
            return 'company_hr';
            break;

            case 8:
            return 'company_merchan_manager';
            break;

            case 9:
            return 'company_merchadiser';
            break;

            case 10:
            return 'company_comm_manager';
            break;

            case 11:
            return 'company_it_manager';
            break;

            case 12:
            return 'employee';
            break;

            case 13:
            return 'supplier';
            break;

            case 14:
            return 'buyer';
            break;

            case 15:
            return 'merchandise_visitor';
            break;
    }
}

function accountCurrency($amount){
    $user_company = Auth::user()->company_id;
    $currency_info = DB::table('tb_company_account_currency')->where([['company_id', $user_company], ['status', 1]])->first();
    if(!empty($currency_info)){
        return $currency_info->prefix .number_format($amount, $currency_info->decimal_point_number).$currency_info->suffix;
    }else{
        return number_format($amount);
    }
}