<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyAccountCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_company_account_currency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('company_id')->index()->nullable();
            $table->string('currency_code', 255)->nullable();
            $table->string('prefix', 20)->nullable();
            $table->string('suffix', 20)->nullable();
            $table->string('decimal_point_number', 20)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('created_by', 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_company_account_currency');
    }
}
