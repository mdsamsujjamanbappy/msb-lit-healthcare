<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientBillInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_patient_bill_information', function (Blueprint $table) {
            $table->id();
            $table->string('bill_id', 20)->nullable();
            $table->string('ref_id', 12)->nullable();
            $table->string('title', 150)->nullable();
            $table->tinyInteger('bill_type')->nullable();
            $table->string('bill_category_id', 8)->nullable();
            $table->string('bill_amount', 8)->nullable();
            $table->date('bill_date')->nullable();
            $table->text('bill_remarks')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_patient_bill_information');
    }
}
