<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInpatientTreatmentListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_inpatient_treatment_list', function (Blueprint $table) {
            $table->id();
            $table->string('inpatient_occupancies_id', 8)->nullable();
            $table->string('disease_name', 40)->nullable();
            $table->text('symptoms')->nullable();
            $table->text('remarks')->nullable();
            $table->date('next_checkup')->nullable();
            $table->tinyInteger('company_id')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_inpatient_treatment_list');
    }
}
