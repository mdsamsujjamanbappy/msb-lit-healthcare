<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pathology_test_list', function (Blueprint $table) {
            $table->id();
            $table->string('test_name', 255)->nullable();
            $table->string('test_category_id', 8)->index()->nullable();
            $table->string('price_amount', 20)->nullable();
            $table->string('test_location', 50)->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('company_id')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pathology_test_list');
    }
}
