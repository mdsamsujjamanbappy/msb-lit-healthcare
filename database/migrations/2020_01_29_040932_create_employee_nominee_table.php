<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeNomineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_nominee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('emp_id')->comment('emp_id will come from ("tb_employee_list.id") table')->nullable()->index();
            $table->string('nominee_name', 220);
            $table->text('nominee_details')->nullable();
            $table->text('nominee_photo')->nullable();
            $table->string('nominee_phone', 30)->nullable();
            $table->string('nominee_relation', 30)->nullable();
            $table->text('nominee_address');
            $table->text('nominee_attachment')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_nominee');
    }
}
