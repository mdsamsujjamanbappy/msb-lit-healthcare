<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_patient_list', function (Blueprint $table) {
            $table->id();
            $table->string('patient_id', 150)->unique()->index();
            $table->string('patient_name', 150)->nullable();
            $table->string('patient_father_name', 150)->nullable();
            $table->string('patient_mother_name', 150)->nullable();
            $table->string('patient_gender_id', 8)->nullable();
            $table->string('patient_age', 8)->nullable();
            $table->text('patient_address')->nullable();
            $table->string('patient_blood_group', 8)->nullable();
            $table->string('patient_phone', 50)->nullable();
            $table->string('patient_email', 50)->nullable();
            $table->string('patient_profession', 50)->nullable();
            $table->text('patient_photo')->nullable();
            $table->string('emergency_contact_name', 150)->nullable();
            $table->string('emergency_contact_phone', 50)->nullable();
            $table->string('emergency_contact_relation', 150)->nullable();
            $table->string('patient_remarks', 180)->nullable();
            $table->tinyInteger('company_id')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_patient_list');
    }
}
