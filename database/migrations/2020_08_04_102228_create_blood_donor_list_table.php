<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloodDonorListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_blood_donor_list', function (Blueprint $table) {
            $table->id();
            $table->string('donor_name', 255)->nullable();
            $table->string('donor_gender_id', 8)->nullable();
            $table->string('donor_age', 8)->nullable();
            $table->string('donor_phone', 20)->nullable();
            $table->string('donor_blood_group', 8)->nullable();
            $table->text('donor_address')->nullable();
            $table->date('last_donation_date')->nullable();
            $table->text('donor_remarks')->nullable();
            $table->tinyInteger('company_id')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_blood_donor_list');
    }
}
