<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_to_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('module_id')->index()->nullable();
            $table->integer('user_id')->index()->nullable();
            $table->integer('activity_id')->nullable();
            $table->boolean('status')->default(1);
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_to_users');
    }
}
