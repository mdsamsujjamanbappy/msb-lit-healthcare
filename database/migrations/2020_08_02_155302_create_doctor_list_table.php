<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_doctor_list', function (Blueprint $table) {
            $table->id();
            $table->string('doctor_name', 50)->nullable();
            $table->string('doctor_bangla_name', 255)->nullable();
            $table->integer('doctor_department_id')->index()->nullable();
            $table->integer('doctor_designation_id')->index()->nullable();
            $table->text('doctor_qualification')->nullable();
            $table->text('doctor_bangla_qualification')->nullable();
            $table->string('doctor_email')->index()->nullable();
            $table->date('doctor_joindate')->nullable();
            $table->string('doctor_phone', 30)->index()->nullable();
            $table->string('doctor_registration_no', 30)->nullable();
            $table->string('doctor_room', 30)->nullable();
            $table->string('doctor_app_fee', 30)->nullable();
            $table->text('doctor_address', 100)->nullable();
            $table->string('doctor_photo')->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('company_id')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_doctor_list');
    }
}
