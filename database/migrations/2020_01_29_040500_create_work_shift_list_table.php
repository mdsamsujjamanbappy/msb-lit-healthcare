<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkShiftListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_work_shift_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shift_name', 100)->nullable();
            $table->string('entry_time', 30)->nullable();
            $table->string('exit_time', 30)->nullable();
            $table->string('buffer_time', 30)->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('status')->comment('"1" is enable or  "0" disable')->default(1);
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_work_shift_list');
    }
}
