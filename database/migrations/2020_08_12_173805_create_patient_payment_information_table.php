<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientPaymentInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_patient_payment_information', function (Blueprint $table) {
            $table->id();
            $table->string('payment_id', 20)->nullable();
            $table->string('ref_id', 8)->nullable();
            $table->tinyInteger('payment_type')->nullable();
            $table->tinyInteger('payment_nature')->nullable();
            $table->string('payment_amount', 8)->nullable();
            $table->date('payment_date')->nullable();
            $table->text('payment_remarks')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_patient_payment_information');
    }
}
