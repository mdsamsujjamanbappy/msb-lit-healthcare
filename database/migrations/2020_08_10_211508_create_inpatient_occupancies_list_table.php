<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInpatientOccupanciesListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_inpatient_occupancies_list', function (Blueprint $table) {
            $table->id();
            $table->string('admission_id', 20)->nullable();
            $table->string('patient_id', 8)->nullable();
            $table->string('doctor_id', 8)->nullable();
            $table->string('accommodation_id', 8)->nullable();
            $table->text('admission_reason')->nullable();
            $table->date('admission_date')->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('company_id')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_inpatient_occupancies_list');
    }
}
