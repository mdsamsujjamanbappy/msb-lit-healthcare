<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeShiftWeekendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_employee_shift_weekend', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('shift_id')->nullable();
            $table->string('day_name', 150)->nullable();
            $table->tinyInteger('status')->comment('"1" is weekend or  "0" Office Day')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_employee_shift_weekend');
    }
}
