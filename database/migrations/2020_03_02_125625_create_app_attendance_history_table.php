<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppAttendanceHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_app_attendance_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("attendance_ref_id")->index()->nullable();
            $table->integer("emp_id")->index()->nullable();
            $table->date("attendance_date")->nullable();
            $table->time("check_in")->nullable();
            $table->time("check_out")->nullable();
            $table->string("check_in_lat", 40)->nullable();
            $table->string("check_in_long", 40)->nullable();
            $table->string("check_out_lat", 40)->nullable();
            $table->string("check_out_long", 40)->nullable();
            $table->string("check_in_ip", 20)->nullable();
            $table->string("check_out_ip", 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_app_attendance_history');
    }
}
