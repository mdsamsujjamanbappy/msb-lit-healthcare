<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref_id',255)->nullable();
            $table->tinyInteger('user_type');
            $table->integer('company_id')->nullable();;
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('login_ip',32)->nullable();
            $table->tinyInteger('status')->default(0)->comment('0 for inactive user and 1 for active user');
            $table->dateTime('last_login_at')->nullable();
            $table->bigInteger('created_by')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
