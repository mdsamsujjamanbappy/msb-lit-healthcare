<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInpatientTreatmentMedicineListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_inpatient_medicine_list', function (Blueprint $table) {
            $table->id();
            $table->string('inpatient_treatment_id', 8)->nullable();
            $table->string('medicine_name', 60)->nullable();
            $table->string('medicine_doses', 8)->nullable();
            $table->string('medicine_times', 8)->nullable();
            $table->string('medicine_taking_instruction', 30)->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_inpatient_medicine_list');
    }
}
