<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DepartmentListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tb_department_list')->insert([
            [  
                'id'                => 1,
                'department_name'   => "Production",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
            [  
                'id'                => 2,
                'department_name'   => "Merchandising",
                'remarks'      		=> "None",
                'status'            => 1,
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ],
        ]);
    }
}
