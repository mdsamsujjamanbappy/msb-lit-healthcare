@extends('layout.master')
@section('title','Accommodation List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Accommodation List</h4>
        </div>

        <div class="pagetitle-btn">
            @if(!empty($aclList[9][2]))
            <a href="" class="btn btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
            @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered " >
                    <thead>
                        <tr>
                            <th width="5%">SN</th>
                            <th>Accommodation Name/Number</th>
                            <th class="msb-txt-center">Category Name</th>
                            <th class="msb-txt-center">Rent Amount</th>
                            <th class="msb-txt-center">Company Name</th>
                            <th class="msb-txt-center">Status</th>
                            <th class="msb-txt-center" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($data_list as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$item->accommodation_name_number}}</td>
                            <td class="msb-txt-center">{{$item->category_name}}</td>
                            <td class="msb-txt-center">{{accountCurrency($item->rent_amount)}}</td>
                            <td class="msb-txt-center">{{$item->company_name}}</td>
                            <td class="msb-txt-center">
                                @if($item->status==1)
                                    <span style="color: green;">Active</span>
                                @else
                                    <span style="color: red;">Inactive</span>
                                @endif
                            </td>
                            <td class="msb-txt-center">
                            @if(!empty($aclList[9][3]))
                                <button type="button" value="{{base64_encode($item->id)}}" class="edit btn btn-sm btn-info" title="Edit Information"><i class="fa fa-edit"></i></button>
                            @endif
                            @if(!empty($aclList[9][4]))
                                <a class="btn btn-sm btn-danger" href="{{route('accommodation.destroy', base64_encode($item->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div id="add_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Accommodation </h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'accommodation.store']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>New Accommodation Information</h6>
                                        <hr>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Name/Number <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="accommodation_name_number" autocomplete="off" value="" required="" placeholder="Accommodation Name/Number">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row select_2_row_modal">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Category <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control mySelect2ModalAdd" name="accommodation_category_id" id="accommodation_category_id" data-placeholder="Select Status" required="">
                                                    <option value="">Select Category</option>
                                                    @foreach($category_list as $category)
                                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Rent Amount <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="number" step="any" class="form-control" name="rent_amount" autocomplete="off" required="" placeholder="Accommodation Rent Amount">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Remarks </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="remarks" id="remarks" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="select_2" data-placeholder="Select Status" required="">
                                                    <option value="">Select  Status</option>
                                                    <option selected="" value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><hr></div>

                                <div class="form-group text-center col-sm-12">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->

            <div id="edit_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-edit "></i> Edit Accommodation Information</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'accommodation.update']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Accommodation Information</h6>
                                    <hr> </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Name/Number <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="accommodation_name_number" id="accommodation_name_number_edit" autocomplete="off" required="" placeholder="Accommodation Name/Number">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row select_2_row_modal">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Category <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control mySelect2ModalUpdate" name="accommodation_category_id" id="accommodation_category_id_edit" data-placeholder="Select Status" required="">
                                                    <option value="">Select Category</option>
                                                    @foreach($category_list as $category)
                                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Rent Amount <span class="msb-txt-red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="number" step="any" class="form-control" name="rent_amount" autocomplete="off" required="" placeholder="Accommodation Rent Amount" id="rent_amount_edit">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Remarks </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="remarks" id="remarks_edit" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="status_edit" data-placeholder="Select Status" required="">
                                                    <option value="">Select  Status</option>
                                                    <option selected="" value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><br /></div>

                                <div class="form-group text-center col-sm-12">
                                    <input type="hidden" name="id" id="edit_id">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Update </button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    'use strict';

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('value');
            $.ajax({
             type: "GET",
             url:"{{url('/accommodation/edit')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 $("#edit_modal").modal('show');
                 $('#edit_id').val(response.id);
                 $('#accommodation_name_number_edit').val(response.accommodation_name_number);
                 $('#rent_amount_edit').val(response.rent_amount);
                 $('#remarks_edit').val(response.remarks);
                 $('#status_edit').val(response.status);
                 $('#accommodation_category_id_edit').val(response.accommodation_category_id).trigger('change');
             },
                error:function(response){
                    console.log(response);
                },
            })
        });

    });
</script>
@endsection