@extends('layout.master')
@section('title','Patient List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Patient List</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[19][2]))
            <a href="{{route('patient.new.create')}}" target="_BLANK" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Patient ID</th>
                            <th>Patient Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Blood Group</th>
                            <th>Emergency Contact</th>
                            <th>Emergency Contact Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($data_list as $data)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$data->patient_id}}</td>
                            <td>{{$data->patient_name}}</td>
                            <td>{{$data->patient_phone}}</td>
                            <td>{{$data->patient_email}}</td>
                            <td>{{$data->patient_blood_group}}</td>
                            <td>{{$data->emergency_contact_name}}</td>
                            <td>{{$data->emergency_contact_phone}}</td>
                            <td>
                            @if(!empty($aclList[19][5]))
                                <a target="_BLANK" href="{{route('patient.details', base64_encode($data->id))}}" class="btn btn-sm btn-primary" title="View Details"><i class="fa fa-eye"></i></a>
                            @endif
                            @if(!empty($aclList[19][3]))
                                <a target="_BLANK" href="{{route('patient.edit', base64_encode($data->id))}}" class="btn btn-sm btn-info" title="Edit Details"><i class="fa fa-edit"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection