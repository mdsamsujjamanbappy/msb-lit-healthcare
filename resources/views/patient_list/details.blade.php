@extends('layout.master')
@section('title','Patient Profile')
@section('extra_css')
@endsection

@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down">
                </div>
                <div class="tx-24 hidden-xs-down">

                </div>
            </div><!-- card-header -->
            <div class="card-body card_body_profile_1">
                <div class="card-profile-img">
                    @if(!empty($petient_details->patient_photo))
                        <img src="{{asset('patient_photo/'.$petient_details->patient_photo)}}" alt="">
                    @else
                        <img src="{{asset('patient_photo/default.png')}}" alt="">
                    @endif
                </div><!-- card-profile-img -->
                <h3 class="tx-normal tx-roboto tx-white">{{$petient_details->patient_name}}</h3>
                <div class="profile_content_1_1 d-flex wd-md-600 mg-md-l-auto mg-md-r-auto mg-b-25 mt-3">
                    <div class="p_left_1">
                        <p class=""><span>Patient ID :</span> <span>{{$petient_details->patient_id}}</span></p>
                    </div>
                    <div class="p_center_1">
                        <p class=""><span>Phone :</span> <span>{{$petient_details->patient_phone}}</span></p>
                    </div>
                    <div class="p_right_1">
                        <p class=""><span>Age :</span> <span>{{$petient_details->patient_age}}</span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="dib_button_profile mr-4">
                            @if(!empty($aclList[19][3]))
                            <a target="_BLANK" href="{{route('patient.edit', base64_encode($petient_details->id))}}" class="btn btn-sm btn-primary" title="Edit Details"><i class="fa fa-edit"></i> Edit Information</a>
                            @endif
                        </div>
                        <div class="dib_button_profile">
                        
                        </div>
                    </div>

                </div>
            </div><!-- card-body -->
        </div><!-- card -->
        <div class="pt-3 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist" id="my_tab_1">
                <li class="nav-item"><a class="nav-link active pb-3" data-toggle="tab" href="#profile" role="tab">Profile</a></li>
                <li class="nav-item "><a class="nav-link pb-3" data-toggle="tab" href="#nominee" role="tab">Prescriptions</a></li>
                <li class="nav-item "><a class="nav-link pb-3" data-toggle="tab" href="#nominee" role="tab">Billing History</a></li>
                <li class="nav-item "><a class="nav-link pb-3" data-toggle="tab" href="#nominee" role="tab">Payment History</a></li>
            </ul>
        </div>
        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="profile">
                <div class="row">
                    <div class="col-lg-12 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <p>Patient Information</p>
                            </div>
                            <div class="card-body card_body_2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Patient Name</span><span>: {{$petient_details->patient_name}} </span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Patient Phone </span><span>: {{$petient_details->patient_phone}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Patient Father Name</span><span>: {{$petient_details->patient_father_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Patient Mother Name</span><span>: {{$petient_details->patient_mother_name}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Gender</span><span>: {{$petient_details->gender_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Age</span><span>: {{$petient_details->patient_age}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Blood Group</span><span>: {{$petient_details->patient_blood_group}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Email</span><span>: {{$petient_details->patient_email}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Emr. Contact Name</span><span>: {{$petient_details->emergency_contact_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Emr. Contact Number</span><span>: {{$petient_details->emergency_contact_phone}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Emr. Contact's Relation</span><span>: {{$petient_details->emergency_contact_relation}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Patient Profession</span><span>: {{$petient_details->patient_profession}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Patient Address</span><span>: {{$petient_details->patient_address}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Patient Remarks</span><span>: {{$petient_details->patient_remarks}}</span></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- row -->
            </div><!-- tab-pane -->
            
            <div class="tab-pane fade" id="nominee">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-6">
                                    <p>Nominee</p>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="" class="btn  btn-info btn-sm  custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#employee_nominee_create_modal"><i class="fas fa-plus-circle"></i> Add New</a>
                                </div>
                            </div>
                            <div class="card-body card_body_2">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){

    });
</script>
@endsection