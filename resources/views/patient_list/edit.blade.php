\@extends('layout.master')
@section('title','Edit Patient Information')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Edit Patient Information</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[17][1]))
            <a href="{{route('patient.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Patient List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
               {!! Form::open(['method'=>'POST','route'=>'patient.update', 'files'=> true]) !!}
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Name <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->patient_name}}" name="patient_name" autocomplete="off" required="" placeholder="Patient Name">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Phone <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->patient_phone}}" name="patient_phone" autocomplete="off" placeholder="Patient Phone" required="" >
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Age <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="number" step="any" class="form-control" value="{{$petient_details->patient_age}}"  name="patient_age" autocomplete="off" placeholder="Patient Age" required="">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6  col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Patient Gender <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="patient_gender_id" id="patient_gender_id" required=""  data-placeholder="gender">
                                    <option value="">Select Gender</option>
                                    @foreach($gender_list as $item)
                                        <option @if($petient_details->patient_gender_id == $item->id) selected @endif value="{{$item->id}}">{{$item->gender_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Emergency Contact <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->emergency_contact_name}}"  name="emergency_contact_name" autocomplete="off" required="" placeholder="Type emergency contact name of the patient">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Contact's Number <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->emergency_contact_phone}}" name="emergency_contact_phone" autocomplete="off" placeholder="Type emergency contact's number of the patient" required="" >
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Contact's Relation <span class="msb-txt-red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->emergency_contact_relation}}" name="emergency_contact_relation" autocomplete="off" required="" placeholder="Type patient's relationship with the emergency contact">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Email</label>
                            <div class="col-sm-8 pl-0">
                                <input type="email" class="form-control" value="{{$petient_details->patient_email}}" name="patient_email" autocomplete="off" placeholder="Patient Email" >
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Father Name</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->patient_father_name}}" name="patient_father_name" autocomplete="off" placeholder="Patient Father Name">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Mother Name</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->patient_mother_name}}" name="patient_mother_name" autocomplete="off" placeholder="Patient Mother Name">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6  col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Blood Group</label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control select2-selection--single" name="patient_blood_group">
                                    <option value="">Select Blood Group</option>
                                    <option @if($petient_details->patient_blood_group == "A+") selected @endif value="A+">A+</option>
                                    <option @if($petient_details->patient_blood_group == "A-") selected @endif value="A-">A-</option>
                                    <option @if($petient_details->patient_blood_group == "AB+") selected @endif value="AB+">AB+</option>
                                    <option @if($petient_details->patient_blood_group == "AB-") selected @endif value="AB-">AB-</option>
                                    <option @if($petient_details->patient_blood_group == "B+") selected @endif value="B+">B+</option>
                                    <option @if($petient_details->patient_blood_group == "B-") selected @endif value="B-">B-</option>
                                    <option @if($petient_details->patient_blood_group == "O+") selected @endif value="O+">O+</option>
                                    <option @if($petient_details->patient_blood_group == "O-") selected @endif value="O-">O-</option>
                                </select>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Profession</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" value="{{$petient_details->patient_profession}}" name="patient_profession" autocomplete="off" placeholder="Patient Profession">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Address</label>
                            <div class="col-sm-8 pl-0">
                                <textarea class="form-control" name="patient_address" autocomplete="off" placeholder="Patient Address" rows="3">{{$petient_details->patient_address}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Remarks</label>
                            <div class="col-sm-8 pl-0">
                                <textarea class="form-control" name="patient_remarks" autocomplete="off" placeholder="Patient Remarks" rows="3">{{$petient_details->patient_remarks}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Patient Photo</label>
                            <div class="col-sm-8 pl-0">
                                <input type="file" class="form-control" id="photo" name="patient_photo">
                            </div>
                        </div>
                    </div>

                  </div>
                  <hr>
                 <div class="form-group text-center col-sm-12">
                    <input type="text" class="form-control" name="id" value="{{$petient_details->id}}">
                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit"><i class="fa fa-save"></i> Save Information</button>
                </div>
                {!! Form::close() !!}
                
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
@endsection