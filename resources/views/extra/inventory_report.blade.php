@extends('layout.master')

@section('title','Report')

@section('extra_css')


@endsection

@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Inventory Report List</h4>
        </div>

        <div class="pagetitle-btn">
            {{--            <a href="" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_designation_modal"> <i class="fas fa-plus-circle"></i> Add New</a>--}}
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1 ">
        <div class="br-section-wrapper pb-5">
            <div class="row report_page">
                <div class="col-md-3">
                    <div class="card my_panel_6">
                        <div class="card-body text-center">
                            <div class="icon_part">
                                <i class="fas fa-search-plus"></i>
                            </div>
                            <div class="content_part">
                                <p>Products list with stock </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('extra_js')

    <script>

        $(document).ready(function(){


        })


    </script>

@endsection



