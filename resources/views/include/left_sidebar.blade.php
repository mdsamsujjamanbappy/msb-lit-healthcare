<?php
$url=request()->route()->getName();
?>
<div class="br-sideleft sideleft-scrollbar">
    <label class="sidebar-label pd-x-10 mg-t-20 op-3"></label>
    <ul class="br-sideleft-menu">
        
        <li class="br-menu-item">
            <a href="{{route('dashboard')}}" class="br-menu-link @if( $url=='dashboard' || $url=='home' || $url=='dashboard') active @endif ">
                <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
                <span class="menu-item-label">Dashboard</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <br>

<!--         @if(!empty($aclList[6][1]) || !empty($aclList[24][1]) || !empty($aclList[25][1]) || !empty($aclList[26][1]) || !empty($aclList[29][1]))
        <br><span style="color: #dedede; font-weight: 500;">HR & ADMIN</span>
        @endif -->

        @if(!empty($aclList[20][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='appointment.in_patient.occupancies.list' || $url=='pathology.test.list' || $url == 'blood_bank.blood_group.summary_list') active @endif ">
                <i class="menu-item-icon fas fa-procedures tx-20"></i> &nbsp;
                <span class="menu-item-label">In-Patient</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[20][1]))
                <li class="sub-item"><a href="{{route('appointment.in_patient.occupancies.list')}}" class="sub-link @if($url=='appointment.in_patient.occupancies.list') active @endif "> Occupancies</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[21][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='patient.list' || $url=='pathology.test.list' || $url == 'blood_bank.blood_group.summary_list') active @endif ">
                <i class="menu-item-icon fas fa-stethoscope tx-20"></i> &nbsp;
                <span class="menu-item-label">Out-Patient</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[21][1]))
                <li class="sub-item"><a href="{{route('patient.list')}}" class="sub-link @if($url=='patient.list') active @endif "> Appointment List</a> </li>
            @endif
            @if(!empty($aclList[21][1]))
                <li class="sub-item"><a href="{{route('patient.list')}}" class="sub-link @if($url=='patient.list') active @endif "> Prescriptions List</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[19][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='patient.list' || $url=='pathology.test.list' || $url == 'blood_bank.blood_group.summary_list') active @endif ">
                <i class="menu-item-icon fas fa-tint tx-20"></i> &nbsp;
                <span class="menu-item-label">Patient Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[19][1]))
                <li class="sub-item"><a href="{{route('patient.list')}}" class="sub-link @if($url=='patient.list') active @endif "> Patient List</a> </li>
            @endif
            @if(!empty($aclList[19][2]))
                <li class="sub-item"><a href="{{route('patient.new.create')}}" class="sub-link @if($url=='patient.create') active @endif "> New Patient</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[18][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='pathology.test_category.list' || $url=='pathology.test.list') active @endif ">
                <i class="menu-item-icon fas fa-tint tx-20"></i> &nbsp;
                <span class="menu-item-label">Pathology Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[18][1]))
                <li class="sub-item"><a href="{{route('pathology.test.list')}}" class="sub-link @if($url=='pathology.test.list') active @endif "> Test List</a> </li>
            @endif
            @if(!empty($aclList[18][1]))
                <li class="sub-item"><a href="{{route('pathology.test_category.list')}}" class="sub-link @if($url=='pathology.test_category.list') active @endif "> Test Category List</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[17][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='blood_bank.donor.list' || $url=='blood_bank.donor.create' || $url == 'blood_bank.blood_group.summary_list') active @endif ">
                <i class="menu-item-icon fas fa-tint tx-20"></i> &nbsp;
                <span class="menu-item-label">Blood Bank</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[17][1]))
                <li class="sub-item"><a href="{{route('blood_bank.donor.list')}}" class="sub-link @if($url=='blood_bank.donor.list') active @endif "> Donor List</a> </li>
            @endif
            @if(!empty($aclList[17][1]))
                <li class="sub-item"><a href="{{route('blood_bank.blood_group.summary_list')}}" class="sub-link @if($url=='blood_bank.blood_group.summary_list') active @endif "> Blood group summary</a> </li>
            @endif
            @if(!empty($aclList[17][2]))
                <li class="sub-item"><a href="{{route('blood_bank.donor.create')}}" class="sub-link @if($url=='blood_bank.donor.create') active @endif "> Add New Donor</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[16][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='doctor.list' || $url=='doctor.new.create') active @endif ">
                <i class="menu-item-icon fas fa-user-md tx-20"></i> &nbsp;
                <span class="menu-item-label">Doctor Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[16][1]))
                <li class="sub-item"><a href="{{route('doctor.list')}}" class="sub-link @if($url=='doctor.list') active @endif "> Doctor List</a> </li>
            @endif
            @if(!empty($aclList[16][2]))
                <li class="sub-item"><a href="{{route('doctor.new.create')}}" class="sub-link @if($url=='doctor.new.create') active @endif "> Add New Doctor</a> </li>
            @endif

            @if(!empty($aclList[16][1]))
                <li class="sub-item"><a href="{{route('doctor.shift.list')}}" class="sub-link @if($url=='doctor.shift.list') active @endif "> Doctor Shift List</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[6][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='employee.list' || $url=='employee.new.create') active @endif ">
                <i class="menu-item-icon fa fa-users tx-20"></i> &nbsp;
                <span class="menu-item-label">Employee Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
            @if(!empty($aclList[6][1]))
                <li class="sub-item"><a href="{{route('employee.list')}}" class="sub-link @if($url=='employee.list') active @endif "> Employee List</a> </li>
            @endif
            @if(!empty($aclList[6][2]))
                <li class="sub-item"><a href="{{route('employee.new.create')}}" class="sub-link @if($url=='employee.new.create') active @endif "> Create Employee</a> </li>
            @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[7][1]) || !empty($aclList[7][2]) || !empty($aclList[7][3]) || !empty($aclList[7][8]) || !empty($aclList[11][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='employee.attendance.daily_attendance' || $url=='employee.attendance.myself_date_wise_attendance_view' || $url=='employee.attendance.myself_date_wise_attendance_data' || $url=='employee.attendance.import_from_excel' || $url=='employee.attendance.todays_attendance' || $url=='employee.attendance.daily_attendance_view' || $url=='employee.attendance.date_wise_view' || $url=='employee.attendance.daily_attendance_view' || $url == 'employee.attendance.date_wise_attendance_view' || $url == 'employee.attendance.date_wise_attendance_data' || $url == 'employee.attendance.give_attendance_by_app') active @endif ">
                <i class="menu-item-icon far fa-calendar-check tx-20"></i>
                <span class="menu-item-label">Attendance Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                @if(!empty($aclList[11][2]))
                    <li class="sub-item"><a href="{{route('employee.attendance.give_attendance_by_app')}}" class="sub-link @if($url=='employee.attendance.give_attendance_by_app') active @endif "> Give Attendance</a> </li>
                @endif
                @if(!empty($aclList[11][8]))
                    <li class="sub-item"><a href="{{route('employee.attendance.myself_date_wise_attendance_view')}}" class="sub-link @if($url=='employee.attendance.myself_date_wise_attendance_view' || $url=='employee.attendance.myself_date_wise_attendance_data') active @endif "> My Attendance History</a> </li>
                @endif

                @if(!empty($aclList[7][8]))
                    <li class="sub-item"><a href="{{route('employee.attendance.todays_attendance')}}" class="sub-link @if($url=='employee.attendance.todays_attendance') active @endif "> Today's History</a> </li>
                    <li class="sub-item"><a href="{{route('employee.attendance.daily_attendance_view')}}" class="sub-link @if($url=='employee.attendance.daily_attendance_view') active @endif ">Daily Attendance History</a> </li>
                    <li class="sub-item"><a href="{{route('employee.attendance.date_wise_attendance_view')}}" class="sub-link @if($url=='employee.attendance.date_wise_attendance_view' || $url == 'employee.attendance.date_wise_attendance_data') active @endif "> Date Wise History</a> </li>
                @endif
                @if(!empty($aclList[7][3]))
                    <li class="sub-item"><a href="{{route('employee.attendance.import_from_excel')}}" class="sub-link @if($url=='employee.attendance.import_from_excel') active @endif "> Import Attendance</a> </li>
                @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[8][1]) || !empty($aclList[8][2]) || !empty($aclList[8][3]) || !empty($aclList[8][8]) || !empty($aclList[8][9]) || !empty($aclList[11][1]) || !empty($aclList[13][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='employee.leave.type_list' || $url=='settings.leave.holidays_observances_list' || $url=='employee.leave.assign.view' || $url == 'employee.leave.new_request' || $url == 'employee.leave.pending.list' || $url == 'employee.leave.approved.list' || $url == 'employee.leave.rejected.list' ) active @endif ">
                <i class="menu-item-icon fas fa-charging-station tx-20"></i>
                <span class="menu-item-label">Leave Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                @if(!empty($aclList[11][2]))
                    <li class="sub-item"><a href="{{route('employee.leave.new_request')}}" class="sub-link @if($url=='employee.leave.new_request') active @endif "> Self Leave Request</a> </li>
                @endif
                @if(!empty($aclList[8][9]))
                    <li class="sub-item"><a href="{{route('employee.leave.pending.list')}}" class="sub-link @if($url=='employee.leave.pending.list') active @endif "> Pending Leave Request</a> </li>
                    <li class="sub-item"><a href="{{route('employee.leave.approved.list')}}" class="sub-link @if($url=='employee.leave.approved.list') active @endif "> Approved Leave Request</a> </li>
                    <li class="sub-item"><a href="{{route('employee.leave.rejected.list')}}" class="sub-link @if($url=='employee.leave.rejected.list') active @endif "> Rejected Leave Request</a> </li>
                @endif
                @if(!empty($aclList[8][8]))
                    <li class="sub-item"><a href="{{route('employee.leave.balance')}}" class="sub-link @if($url=='employee.leave.balance') active @endif "> Leave Balance </a> </li>
                @endif
                @if(!empty($aclList[8][2]))
                    <li class="sub-item"><a href="{{route('employee.leave.assign.view')}}" class="sub-link @if($url=='employee.leave.assign.view') active @endif "> Assign Leave </a> </li>
                @endif
                @if(!empty($aclList[13][1]))
                    <li class="sub-item"><a href="{{route('settings.leave.holidays_observances_list')}}" class="sub-link @if($url=='settings.leave.holidays_observances_list') active @endif "> Holidays and Observances </a> </li>
                    <li class="sub-item"><a href="{{route('employee.leave.type_list')}}" class="sub-link @if($url=='employee.leave.type_list') active @endif "> Leave Setting </a> </li>
                @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[9][1]) || !empty($aclList[10][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='company_information.list' || $url=='department.list' || $url=='designation.list' || $url=='gender.list' || $url =='accommodation.category.list' || $url =='accommodation.list') active @endif">
                <i class="menu-item-icon fa fa-cog tx-20"></i>
                <span class="menu-item-label">Master Settings</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub ">
                @if(!empty($aclList[10][1]))
                    <li class="sub-item"><a href="{{route('company_information.list')}}" class="sub-link @if($url=='company_information.list') active @endif"> Company List</a> </li>
                @endif
                @if(!empty($aclList[9][1]))
                    <li class="sub-item"><a href="{{route('department.list')}}" class="sub-link @if($url=='department.list') active @endif"> Department List</a> </li>
                    <li class="sub-item"><a href="{{route('designation.list')}}" class="sub-link @if($url=='designation.list') active @endif"> Designation List</a> </li>
                    <li class="sub-item"><a href="{{route('gender.list')}}" class="sub-link @if($url=='gender.list') active @endif"> Gender List</a> </li>
                    <li class="sub-item"><a href="{{route('work_shift.list')}}" class="sub-link @if($url=='work_shift.list') active @endif"> Work Shift List</a> </li>
                    <li class="sub-item"><a href="{{route('weekend_holiday.list')}}" class="sub-link @if($url=='weekend_holiday.list') active @endif"> Weekend Holiday List</a> </li>
                    <li class="sub-item"><a href="{{route('accommodation.category.list')}}" class="sub-link @if($url=='accommodation.category.list') active @endif"> Accommodation Category</a> </li>
                    <li class="sub-item"><a href="{{route('accommodation.list')}}" class="sub-link @if($url=='accommodation.list') active @endif"> Accommodation List</a> </li>
                @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[1][1]) || !empty($aclList[2][1]) || !empty($aclList[3][1]) || !empty($aclList[4][1]) || !empty($aclList[5][1]))
        <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub @if($url=='role.index' || $url=='role.create' || $url=='role.edit' || $url=='activity.index' || $url=='activity.create' || $url=='activity.edit' || $url=='module.index' || $url=='module.create' || $url=='module.edit' || $url=='role.access' || $url=='user.access') active @endif">
                <i class="menu-item-icon fas fa-universal-access tx-20"></i>
                <span class="menu-item-label">User Access Control</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub ">
                @if(!empty($aclList[1][1]))
                    <li class="sub-item"><a href="{{route('role.index')}}" class="sub-link @if($url=='role.index' || $url=='role.create' || $url=='role.edit') active @endif"> Role Management</a> </li>
                @endif
                @if(!empty($aclList[5][1]))
                    <li class="sub-item"><a href="{{route('activity.index')}}" class="sub-link @if($url=='activity.index' || $url=='activity.create' || $url=='activity.edit') active @endif"> Activity Management</a> </li>
                @endif
                @if(!empty($aclList[4][1]))
                    <li class="sub-item"><a href="{{route('module.index')}}" class="sub-link @if($url=='module.index' || $url=='module.create' || $url=='module.edit') active @endif"> Module Management</a></li>
                @endif
                @if(!empty($aclList[2][1]))
                    <li class="sub-item"><a href="{{route('role.access')}}" class="sub-link @if($url=='role.access') active @endif"> Role Access Control</a></li>
                @endif
                @if(!empty($aclList[3][1]))
                    <li class="sub-item"><a href="{{route('user.access')}}" class="sub-link @if($url=='user.access') active @endif"> User Access Control</a></li>
                @endif
            </ul>
        </li>
        @endif

        @if(!empty($aclList[11][1]))
        <li class="br-menu-item">
            <a href="{{route('employee.list.general')}}" class="br-menu-link @if($url=='employee.list.general') active @endif ">
                <i class="menu-item-icon icon ion-person-stalker tx-24"></i>
                <span class="menu-item-label">Employee List</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        @endif

        <li class="br-menu-item">
            <a href="{{url('logout')}}" class="br-menu-link">
                <i class="fas fa-sign-out-alt tx-18"></i>
                <span class="menu-item-label">Log out</span>
            </a>
        </li>

        @if(!empty($aclList[12][1]))
        <li class="br-menu-item" style="text-align: center; color: #aaa; margin-top: 10px; padding-top: 5px; border-top: 1px solid #555">
            <a href="{{route('update_logs')}}" class="br-menu-link">
                LIT Healthcare V1.0
            </a>
        </li>
        @endif

    </ul>
    <br>
</div>