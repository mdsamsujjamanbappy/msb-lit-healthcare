@extends('layout.master')
@section('title','Weekend Holiday List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Weekend Holiday List</h4>
        </div>

        <div class="pagetitle-btn">
            @if(!empty($aclList[26][2]))
            <a href="" class="btn btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
            @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered " >
                    <thead>
                        <tr>
                            <th width="5%">SN</th>
                            <th>Day Name</th>
                            <th>Company Name</th>
                            <th>Status</th>
                            <th width="15%" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($weekend_holiday as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$item->day_name}}</td>
                            <td>{{$item->company_name}}</td>
                            <td>
                                @if($item->status==1)
                                    <span style="color: red;">Offday</span>
                                @else
                                    <span style="color: green;">Workday</span>
                                @endif
                            </td>
                            <td style="text-align: center;">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection



