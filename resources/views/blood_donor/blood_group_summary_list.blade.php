@extends('layout.master')
@section('title','Blood Donor Details')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Blood Donor Details</h4>
        </div>

        <div class="pagetitle-btn">
         @if(!empty($aclList[17][1]))
            <a href="{{route('blood_bank.donor.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Donor List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Blood Group</th>
                        <th>Number of Donor</th>
                    </tr>
                    @foreach($blood_count as $bc)
                    <tr>
                        <td width="30%">{{$bc->donor_blood_group}}</td>
                        <td>{{$bc->blood_count}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
@endsection