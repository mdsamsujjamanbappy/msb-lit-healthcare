@extends('layout.master')
@section('title','Blood Donor List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Blood Donor List</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[17][2]))
            <a href="{{route('blood_bank.donor.create')}}" target="_BLANK" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="myDatatable25" class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th class="msb-txt-center">SN</th>
                            <th class="msb-txt-center">Donor Name</th>
                            <th class="msb-txt-center">Donor Phone</th>
                            <th class="msb-txt-center">Donor Age</th>
                            <th class="msb-txt-center">Gender</th>
                            <th class="msb-txt-center">Donor Blood Group</th>
                            <th class="msb-txt-center">Last Donation Date</th>
                            <th class="msb-txt-center">Company Name</th>
                            <th class="msb-txt-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($data_list as $data)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$data->donor_name}}</td>
                            <td class="msb-txt-center">{{$data->donor_phone}}</td>
                            <td class="msb-txt-center">{{$data->donor_age}}</td>
                            <td class="msb-txt-center">{{$data->gender_name}}</td>
                            <td class="msb-txt-center">{{$data->donor_blood_group}}</td>
                            <td class="msb-txt-center">@if(!empty($data->last_donation_date)){{date('d-m-Y', strtotime($data->last_donation_date))}} @endif</td>
                            <td class="msb-txt-center">{{$data->company_name}}</td>
                            <td class="msb-txt-center">
                            @if(!empty($aclList[17][5]))
                                <a target="_BLANK" href="{{route('blood_bank.donor.details', base64_encode($data->id))}}" class="btn btn-sm btn-primary" title="View Details"><i class="fa fa-eye"></i></a>
                            @endif
                            @if(!empty($aclList[17][3]))
                                <a target="_BLANK" href="{{route('blood_bank.donor.edit', base64_encode($data->id))}}" class="btn btn-sm btn-info" title="Edit Details"><i class="fa fa-edit"></i></a>
                            @endif
                            @if(!empty($aclList[17][4]))
                                <a href="{{route('blood_bank.donor.destroy', base64_encode($data->id))}}" onclick="return confirm('Are you sure to destroy?')" class="btn btn-sm btn-danger" title="Destroy Details"><i class="fa fa-trash-alt"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
@endsection