\@extends('layout.master')
@section('title','Add New Blood Donor')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> New Blood Donor</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[17][1]))
            <a href="{{route('blood_bank.donor.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Donor List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
               {!! Form::open(['method'=>'POST','route'=>'blood_bank.donor.store']) !!}
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Donor Name <span style="color:red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="donor_name" autocomplete="off" required="" placeholder="Donor Name">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Donor Phone</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control" name="donor_phone" autocomplete="off" placeholder="Donor Phone">
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-sm-6  col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Gender <span style="color:red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control" name="donor_gender_id" id="donor_gender_id" required=""  data-placeholder="gender">
                                    <option value="">Select Gender</option>
                                    @foreach($gender_list as $item)
                                        <option value="{{$item->id}}">{{$item->gender_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Donor Age</label>
                            <div class="col-sm-8 pl-0">
                                <input type="number" step="any" class="form-control" name="donor_age" autocomplete="off" placeholder="Donor Age">
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6  col-12">
                        <div class="form-group row select_2_row_modal">
                            <label for="form-label" class="col-sm-4 control-label form-label-1">Blood Group <span style="color:red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <select class="form-control select2-selection--single" required name="donor_blood_group">
                                    <option value="">Select Blood Group</option>
                                    <option value="A+">A+</option>
                                    <option value="A-">A-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                    <option value="B+">B+</option>
                                    <option value="B-">B-</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Last Donation Date</label>
                            <div class="col-sm-8 pl-0">
                                <input type="text" class="form-control myDatepicker" name="last_donation_date" autocomplete="off" placeholder="Last Donation Date">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Donor Address</label>
                            <div class="col-sm-8 pl-0">
                                <textarea class="form-control" name="donor_address" autocomplete="off" placeholder="Donor Address" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Donor Remarks</label>
                            <div class="col-sm-8 pl-0">
                                <textarea class="form-control" name="donor_remarks" autocomplete="off" placeholder="Donor Remarks" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                   
                  </div>
                  <hr>
                 <div class="form-group text-center col-sm-12">
                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Save Information</button>
                </div>
                {!! Form::close() !!}
                
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
@endsection