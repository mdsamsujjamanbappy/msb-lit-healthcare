@extends('layout.master')
@section('title','Blood Donor Details')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Blood Donor Details</h4>
        </div>

        <div class="pagetitle-btn">
         @if(!empty($aclList[17][1]))
            <a href="{{route('blood_bank.donor.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Donor List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <table class="table table-bordered table-striped responsive">
                    <tr>
                        <td width="30%">Donor Name</td>
                        <td><b>{{$donor_details->donor_name}}</b></td>
                    </tr>
                    <tr>
                        <td>Donor Phone</td>
                        <td><b>{{$donor_details->donor_phone}}</b></td>
                    </tr>
                    <tr>
                        <td>Donor Age</td>
                        <td><b>{{$donor_details->donor_age}}</b></td>
                    </tr>
                    <tr>
                        <td>Donor Gender</td>
                        <td><b>{{$donor_details->gender_name}}</b></td>
                    </tr>
                    <tr>
                        <td>Donor Blood Group</td>
                        <td><b>{{$donor_details->donor_blood_group}}</b></td>
                    </tr>
                    <tr>
                        <td>Last Donation Date</td>
                        <td><b>@if(!empty($donor_details->last_donation_date)){{date('d-m-Y', strtotime($donor_details->last_donation_date))}} @endif</b></td>
                    </tr>
                    <tr>
                        <td>Donor Address</td>
                        <td><b>{{$donor_details->donor_address}}</b></td>
                    </tr>
                    <tr>
                        <td>Donor Remarks</td>
                        <td><b>{{$donor_details->donor_remarks}}</b></td>
                    </tr>
                    <tr>
                        <td>Company Name</td>
                        <td><b>{{$donor_details->company_name}}</b></td>
                    </tr>
                    <tr>
                        <td>Created By</td>
                        <td><b>{{$donor_details->createdBy}}</b></td>
                    </tr>
                    <tr>
                        <td>Created At</td>
                        <td><b>@if(!empty($donor_details->created_at)){{date('d-m-Y', strtotime($donor_details->created_at))}} @endif</b></td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
@endsection