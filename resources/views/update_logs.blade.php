@extends('layout.master')
@section('title','Software Update Logs')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
                <div class="br-pagetitle">
                    <i class="icon far fa-list-alt"></i>
                    <div>
                        <h2><b>SOFTWARE RELEASE LOGS</b></h2>
                        <p class="mg-b-0">Released By - <b>Samsujjaman Bappy</b></p>
                    </div>
                </div>
                <hr>
                <div id="accordion2" class="accordion accordion-head-colored accordion-primary" role="tablist" aria-multiselectable="true">
                
                <div class="card">
                  <div class="card-header" role="tab" id="headingOne2">
                    <h6 class="mg-b-0">
                      <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne4">
                        Version 1.0
                      </a>
                    </h6>
                  </div><!-- card-header -->

                  <div id="collapseOne4" class="collapse show" role="tabpanel" aria-labelledby="headingOne2">
                    <div class="card-block pd-20">
                        <b>Release Date: 15/07/2020</b>
                        <br>
                        <b>Release Note:</b>
                        <br>
                        <div class="display_log">
                            <code>
                                * Bug Fixes. <br>
                            </code> 
                        </div>
                        
                    </div>
                  </div>
                </div>

              </div>

        </div>
    </div>
@endsection

@section('extra_js')
@endsection