@extends('layout.master')
@section('title','My Profile')
@section('extra_css')
@endsection

@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down">
                </div>
                <div class="tx-24 hidden-xs-down">

                </div>
            </div><!-- card-header -->
            <div class="card-body card_body_profile_1">
                <div class="card-profile-img">
                 @if($employee->emp_photo=='' && $employee->gender_name=='Male')
                   <img src="{{asset('employee_profile_image/male.png')}}" alt="">
                   @elseif($employee->emp_photo=='' && $employee->gender_name=='Female')
                   <img src="{{asset('employee_profile_image/female.png')}}" alt="">
                   @else
                   <img src="{{asset('employee_profile_image/'.$employee->emp_photo)}}" alt="">
                 @endif
                </div><!-- card-profile-img -->
                <h3 class="tx-normal tx-roboto tx-white">{{$employee->emp_first_name}} {{$employee->emp_last_name}}</h5>
                <div class="profile_content_1_1 d-flex wd-md-600 mg-md-l-auto mg-md-r-auto mg-b-25 mt-3">
                    <div class="p_left_1">
                        <p class=""><span>Employee ID :</span> <span>{{$employee->employee_id}}</span></p>
                    </div>
                    <div class="p_center_1">
                        <p class=""><span>Email :</span> <span>{{$employee->emp_email}}</span></p>
                    </div>
                    <div class="p_right_1">
                        <p class=""><span>Phone :</span> <span>{{$employee->emp_phone}}</span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="dib_button_profile">
                            <a href="" class="btn  btn-info btn-sm  custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#password_modal"> <i class="fas fa-edit"></i> Password</a>
                        </div>
                    </div>

                </div>
            </div><!-- card-body -->
        </div><!-- card -->
        <div class="pt-3 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist" id="my_tab_1">
                <li class="nav-item"><a class="nav-link active pb-3" data-toggle="tab" href="#profile" role="tab">Profile</a></li>
                <li class="nav-item "><a class="nav-link pb-3" data-toggle="tab" href="#nominee" role="tab">Nominee</a></li>
                <li class="nav-item"><a class="nav-link pb-3" data-toggle="tab" href="#education" role="tab">Educational Information</a></li>
                <li class="nav-item"><a class="nav-link pb-3" data-toggle="tab" href="#working_history" role="tab">Employement History</a></li>
                <li class="nav-item"><a class="nav-link pb-3" data-toggle="tab" href="#additional_information" role="tab">Addtional Information</a></li>
                <li class="nav-item "><a class="nav-link pb-3" data-toggle="tab" href="#downloads" role="tab">Download</a></li>
            </ul>
        </div>
        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="profile">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                   <li style="color:red">{{ $error }}</li>
                               @endforeach
                             @endif
                             @if(Session::has('success'))
                             <script>
                                 var msg =' <?php echo Session::get('success');?>'
                                 swal(msg, "", "success");
                             </script>
                             @endif
                             @if(Session::has('error'))
                             <script>
                                 var msg =' <?php echo Session::get('error');?>'
                                 swal(msg, "", "error");
                             </script>
                             @endif
                            <div class="card-header card_header_2 d-flex">
                                <p>Personal Information</p>
                                <!-- <a href="" class="btn  btn-info btn-sm ml-auto custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#update_modal"> <i class="fas fa-edit"></i> Edit</a> -->
                            </div>
                            <div class="card-body card_body_2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Name</span><span>: {{$employee->emp_first_name}} {{$employee->emp_last_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Employee ID</span><span>: {{$employee->employee_id}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Father Name</span><span>: {{$employee->emp_father_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Mother Name</span><span>: {{$employee->emp_mother_name}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Email</span><span>: {{$employee->emp_email}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Phone</span><span>: {{$employee->emp_phone}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Gender</span><span>: {{$employee->gender_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Department</span><span>: {{$employee->department_name}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Designation</span><span>: {{$employee->designation_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Birth Date</span><span>: {{date('d-m-Y',strtotime($employee->emp_dob))}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Company</span><span>: {{$employee->company_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Blood Group</span><span>: {{$employee->emp_blood_group}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Joining Date</span><span>: {{date('d-m-Y',strtotime($employee->emp_joining_date))}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Religion</span><span>: {{$employee->emp_religion}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Marital Status</span><span>: {{$employee->emp_marital_status}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>IPBX Extension</span><span>: {{$employee->emp_ipbx_extension}}</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Current Address</span><span>: {{$employee->emp_current_address}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>User Type</span><span>:
                                            <?php 
                                            if($employee->user_type==1){
                                                echo "Super Admin";
                                            }elseif($employee->user_type==2){
                                                echo "Group Admin";
                                            }elseif($employee->user_type==3){
                                                echo "Group Director";
                                            }elseif($employee->user_type==4){
                                                echo "Company Director";
                                            }elseif($employee->user_type==5){
                                                echo "Company Admin";
                                            }elseif($employee->user_type==6){
                                                echo "Company Accountant";
                                            }elseif($employee->user_type==7){
                                                echo "Company HR";
                                            }elseif($employee->user_type==8){
                                                echo "Company Merchandise Manager";
                                            }elseif($employee->user_type==9){
                                                echo "Company Merchandiser";
                                            }elseif($employee->user_type==10){
                                                echo "Company Commercial Manager";
                                            }elseif($employee->user_type==11){
                                                echo "Company IT Manager";
                                            }elseif($employee->user_type==12){
                                                echo "Employee";
                                            }elseif($employee->user_type==13){
                                                echo "Supplier";
                                            }elseif($employee->user_type==14){
                                                echo "Buyer";
                                            }else{
                                                echo "Not Assigned Yet";
                                            }
                                             ?>
                                        </span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Parmanent Address</span><span>: {{$employee->emp_parmanent_address}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Probation Period</span><span>: {{$employee->emp_probation_period}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>National ID</span><span>: {{$employee->emp_nid}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Nationality</span><span>: {{$employee->emp_nationality}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Bank Account</span><span>: {{$employee->emp_bank_account}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Bank Info</span><span>: {{$employee->emp_bank_info}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Card Number(Poxy)</span><span>: {{$employee->emp_card_number}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Account Status</span><span>: @if($employee->emp_account_status==1) <b style="color:green">Active </b> @else <b style="color:red">Inactive </b> @endif </span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Date Of Discontinuation</span><span>: {{$employee->date_of_discontinuation}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Reason Of Discontinuation</span><span>: {{$employee->reason_of_discontinuation}}</span></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- row -->
            </div><!-- tab-pane -->
            
            <div class="tab-pane fade" id="education">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-6">
                                    <p>Education</p>
                                </div>
                                <div class="col-6 text-right">
                                   
                                </div>
                            </div>
                            <div class="card-body card_body_2">
                                @forelse ($employee_education as $item)
                                <div class="education_col mb-4">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="education_title">{{$item->emp_exam_title}}</p>
                                        </div>
                                        <div class="col-6">
                                           
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Institution</span><span>: {{$item->emp_institution_name}}</span></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span>Scale</span><span>: {{$item->emp_scale}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>CGPA</span><span>: {{$item->emp_result}}</span></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span>Passing Year</span><span>:{{$item->emp_passing_year}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                        <p class="cerification_p"><span>Attachment</span><span>:
                                            @if($item->emp_attachment=='')
                                                No attachment Found.
                                            @else
                                                <a href="{{asset('employee_education_attachment/'.$item->emp_attachment)}}"> Download  <i class="fas fa-download"></i></a></span>
                                            @endif
                                          </p>
                                        </div>

                                    </div>
                                </div>
                                @empty
                                    <p>No Educational History Found. </p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="nominee">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-6">
                                    <p>Nominee</p>
                                </div>
                                <div class="col-6 text-right">
                                </div>
                            </div>
                            <div class="card-body card_body_2">

                                @forelse ($employee_nominee as $item)
                                <div class="education_col mb-4">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="education_title"></p>
                                        </div>
                                        <div class="col-6">
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Name</span><span>: {{$item->nominee_name}}</span></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span>Phone</span><span>: {{$item->nominee_phone}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Relation</span><span>: {{$item->nominee_relation}}</span></p>
                                        </div> 
                                        <div class="col-md-6">
                                            <p><span>Details</span><span>: {{$item->nominee_details}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Address</span><span>: {{$item->nominee_address}}</span></p>
                                        </div> 
                                        <div class="col-md-6">
                                            <p class="cerification_p"><span>Attachment</span><span>: <a href="{{asset('employee_nominee_attachment/'.$item->nominee_attachment)}}">Download  <i class="fas fa-download"></i></a></span></p>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <p>No Nominee Information Found.</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="working_history">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-6">
                                    <p>Employement History</p>
                                </div>
                                <div class="col-6 text-right">
                                   
                                </div>
                            </div>
                            <div class="card-body card_body_2">
                                @forelse ($working_history as $item)
                                <div class="education_col mb-4">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="education_title"></p>
                                        </div>
                                        <div class="col-6">
                                            <div class="education_btn_part_1">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Company</span><span>: {{$item->wh_company_name}}</span></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span>Position</span><span>: {{$item->wh_designation}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Joining Date</span><span>: {{date('d-F-Y',strtotime($item->wh_joining_date))}}</span></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><span>Resign Date</span><span>: {{date('d-F-Y',strtotime($item->wh_resign_date))}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><span>Description</span><span>: {{$item->wh_description}}</span></p>
                                        </div>
                                        <div class="col-md-6">
                                        <p class="cerification_p"><span>Attachment</span><span>: <a href="{{asset('employee_employement_attachment/'.$item->wh_attachment)}}">Download  <i class="fas fa-download"></i></a></span></p>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <p>No Employement History Found.</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="additional_information">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-6">
                                    <p>Additional Information</p>
                                </div>
                                <div class="col-6 text-right">
                                    
                                </div>
                            </div>
                            <div class="card-body card_body_2">
                                @forelse ($additional_information as $item)
                                <div class="education_col mb-4">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="education_title"></p>
                                        </div>
                                        <div class="col-6">
                                            <div class="education_btn_part_1">
                                                <a href="#" id="{{$item->id}}" class="btn  btn-info btn-sm  custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium work_edit"> <i class="fas fa-edit"></i> Edit</a>
                                                <a href="#" id="{{$item->id}}" class="btn  btn-danger btn-sm  custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium work_del"> <i class="far fa-trash-alt"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p><span>Title</span><span>: {{$item->title}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p><span>Description</span><span>: {{$item->description}}</span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                        <p class="cerification_p"><span>Attachment</span><span>: <a href="{{asset('employee_employement_attachment/'.$item->attachment)}}">Download  <i class="fas fa-download"></i></a></span></p>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <p>No Additional Information Found.</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="downloads">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-8">
                                    <p>Download</p>
                                </div>
                                <div class="col-4 text-right">
                                    
                                </div>
                            </div>
                            <div class="card-body card_body_2">
                                


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="password_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update Password </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <span id="form_result"></span>
                    <div class="modal_body_inner">

                        <div id="img_upload_div">
                        {!! Form::open(['method'=>'POST','route'=>'employee.my_profile_password_update']) !!}
                        <div class="row m-0 mt-3">
                            <div class="col-sm-9 m-auto">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">New Password <span style="color:red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" name="new_password" id="new_password" class="form-control pd-y-12" autocomplete="off" required="" placeholder="New Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">Confirm New Password <span style="color:red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" name="confirm_new_password" id="confirm_new_password" class="form-control pd-y-12" autocomplete="off" required="" placeholder="Confirm New Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-4 control-label form-label-1">
                                    <?php
                                        $fst = rand( 0, 9);
                                        $snd = rand( 0, 9);
                                        echo $fst ." + ". $snd;
                                        $sum = $fst+$snd;
                                     ?>
                                     = </label>
                                    <div class="col-sm-8 pl-0">
                                        <input type="text" name="security_system" hidden="" value="{{$sum}}" >
                                        <input type="text" name="security_user" class="form-control pd-y-12" autocomplete="off" required="" >
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" id="department_add_btn" type="submit">Update Password</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                        </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

@endsection

@section('extra_js')

<script>
    $(document).ready(function(){
        $('input').attr('autocomplete', 'off');
        if($().select2) {
            $('#emp_department_id_edit').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#update_modal .modal-content'),
                placeholder: "Select a Department"
            });

            $('#emp_designation_id_edit').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#update_modal .modal-content'),
                placeholder: "Select a Designation"
            });

            $('#emp_company_id_edit').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#update_modal .modal-content'),
                placeholder: "Select a Company"
            });

            $('#emp_blood_group').select2({
                minimumResultsForSearch: '',
                dropdownParent: $('#update_modal .modal-content'),
                placeholder: "Select a Blood Group"
            });
        }

    });
</script>
@endsection































