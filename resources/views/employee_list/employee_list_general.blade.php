@extends('layout.master')
@section('title','Employee List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Employee List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered  display responsive" >
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Employee Name</th>
                            <th>Company Name</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>IPBX Ext.</th>
                            <th>Blood Group</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($employee_list_general as $data)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$data->emp_first_name}} {{$data->emp_last_name}}</td>
                            <td>{{$data->company_name}}</td>
                            <td>{{$data->department_name}}</td>
                            <td>{{$data->designation_name}}</td>
                            <td>{{$data->emp_phone}}</td>
                            <td>{{$data->emp_email}}</td>
                            <td>{{$data->emp_ipbx_extension}}</td>
                            <td>{{$data->emp_blood_group}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            pageLength: 25
        });
    });
</script>
@endsection



