@extends('layout.master')
@section('title','Permission Error')
@section('extra_css')
@endsection
@section('content')
    <div class="d-flex" style="padding-top: 25px;">
      <div class="wd-lg-70p wd-xl-50p pd-x-40">
        <h5 class="tx-xs-28 tx-normal tx-danger mg-b-20 lh-5"><b>The page your are looking for has not been found.</b></h5>
        <p class="tx-16 mg-b-30">The page you are looking for might have been Permission error or unavailable.</p>

      </div>
    </div><!-- ht-100v -->
@endsection

@section('extra_js')
@endsection



