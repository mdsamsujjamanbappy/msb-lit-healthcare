@extends('layout.master')
@section('title','Module List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fas fa-list"></i> Module List</h4>
        </div>

        <div class="pagetitle-btn">
            @if(!empty($aclList[5][1]))
                <a href="{{route('module.create')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
            @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="myDatatable25" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">SL</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th class="text-center">Module ID</th>
                            <th class="text-center">Created By</th>
                            <th class="text-center">Updated By</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($modules as $module)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$module->name}}</td>
                        <td>{{$module->description}}</td>
                        <td class="text-center">{{$module->id}}</td>
                        <td class="text-center">{{$module->createdBy->name ?? '' }}</td>
                        <td class="text-center">{{$module->updatedBy->name ?? '' }}</td>
                        <td class="text-center">
                           @if($module->status == 1)
                                <span class="text-primary">Active</span>
                            @else
                                <span class="text-danger">Inactive</span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if(!empty($aclList[4][3]))
                                <a class="btn btn-sm btn-info text-white" href="{{route('module.edit', ($module->id))}}" title="Edit">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection



