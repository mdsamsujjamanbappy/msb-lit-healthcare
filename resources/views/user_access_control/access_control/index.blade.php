@extends('layout.master')
@section('title','Role Based Access')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon fas fa-list"></i> Role Based Access</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <form method="post" action="{{url('/roleacl')}}" class="form-horizontal">
                    @csrf
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group select2-parent">
                                <label>Select Role</label>
                                <select name="role_id" id="role_id" class="mySelect2 form-control js-source-states single-select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    @foreach($roleList as $key => $name)
                                        <option value="{{$key}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="access-control-setup">
                        <!--AJAX content will be loaded here-->
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
    <script type="text/javascript" lang="javascript">
        $(document).ready(function () {

            $("#role_id").change(function () {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{!! URL::to('roleAclSetup') !!}",
                    data: {role_id: $("#role_id").val(), _token : _token},
                    dataType: "text",
                    cache: false,
                    success:
                        function (data) {
                            $("#access-control-setup").html(data);
                            $(".cancel").click(function () {
                                history.go(-1);
                            });
                        }
                });
                return false;
            });

            //check/uncheck all
            $(document).on("change", '#access_table .m_activity', function () {
                var columnId = $(this).data('column-id');
                if ($(this).prop('checked')) {
                    $('.activity_' + columnId).prop('checked', true);
                } else {
                    $('.activity_' + columnId).prop('checked', false);
                }
            });

            $(document).on("change", '.activitycell', function () {
                var columnId = $(this).data('column-id');
                if ($('.activity_' + columnId + ':checked').length == $('.activity_' + columnId).length) {
                    $('#activity_header_' + columnId).prop('checked', true);
                } else {
                    $('#activity_header_' + columnId).prop('checked', false);
                }
            });

        });
    </script>
@endsection