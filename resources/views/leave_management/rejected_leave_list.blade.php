@extends('layout.master')
@section('title','Rejected Leave Request List')
@section('extra_css')
<style type="text/css" media="screen">
  th, td { white-space: nowrap; }
</style>
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Rejected Leave Request List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered stripe table-hover" >
                    <thead>
                        <tr>
                            <th style="background: #fff;background: #f1f1f1c9;">SN</th>
                            <th style="background: #fff;background: #f1f1f1c9;">Employee ID</th>
                            <th style="background: #fff;background: #f1f1f1c9;">Employee Name</th>
                            <th style="background: #fff;background: #f1f1f1c9;">Company Name</th>
                            <th style="text-align: center;">Leave Type</th>
                            <th style="text-align: center;">Leave Start Date</th>
                            <th style="text-align: center;">Leave End Date</th>
                            <th style="text-align: center;">Total Days</th>
                            <th style="text-align: center;">Request Date</th>
                            <th style="text-align: center;">Attachment</th>
                            <th style="text-align: center;">Remarks</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;  background: #fff;" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($leave_list as $item)
                        <tr>
                            <td style="text-align: center;">{{++$i}}</td>
                            <td style="text-align: center;">{{$item->employee_main_id}}</td>
                            <td style="text-align: center;">{{$item->emp_first_name}} {{$item->emp_last_name}}</td>
                            <td style="text-align: center;">{{$item->company_name}}</td>
                            <td style="text-align: center;">{{$item->leave_type_name}}</td>
                            <td style="text-align: center;">@if(!empty($item->leave_starting_date)) {{date('d-m-Y', strtotime($item->leave_starting_date))}} @endif</td>
                            <td style="text-align: center;">@if(!empty($item->leave_ending_date)) {{date('d-m-Y', strtotime($item->leave_ending_date))}} @endif</td>
                            <td style="text-align: center;">{{$item->actual_days}}</td>
                            <td style="text-align: center;">@if(!empty($item->created_at)) {{date('d-m-Y', strtotime($item->created_at))}} @endif</td>
                            <td style="text-align: center;">-</td>
                            <td style="text-align: center;">{{$item->description}}</td>
                            <td style="text-align: center;">
                                <?php 
                                    if($item->status==0){
                                        echo "<span style='color: blue;'>Pending</span>";
                                    }elseif($item->status==1){
                                        echo "<span style='color: green;'>Approved</span>";
                                    }elseif($item->status==2){
                                        echo "<span style='color: red;'>Rejected</span>";
                                    }
                                ?>
                            </td>
                            <td style="text-align: center; background: #fff;">
                            @if(!empty($aclList[25][9]))
                                <a class="btn btn-sm btn-success" href="{{route('employee.leave.pending.approve', base64_encode($item->id))}}" onclick="return confirm('Are you sure to Approve?')" title="Approve"><i class="fa fa-check-square"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection



