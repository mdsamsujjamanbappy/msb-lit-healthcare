@extends('layout.master')
@section('title','Leave Balance List')
@section('extra_css')
<style type="text/css" media="screen">
  th, td { white-space: nowrap; }
</style>
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Leave Balance List</h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                <table id="datatable" class="table row-border order-column table-bordered stripe table-hover" >
                    <thead>
                        <tr>
                            <th valign="top" style="text-align: center; background: #f1f1f1c9;" rowspan="2">SN</th>
                            <th valign="top" style="text-align: center; background: #f1f1f1c9;" rowspan="2">Employee ID</th>
                            <th valign="top" style="text-align: center; background: #f1f1f1c9;" rowspan="2">Employee Name</th>
                            <th valign="top" style="text-align: center; background: #f1f1f1c9;" rowspan="2">Company Name</th>
                            @foreach($leave_type_setting as $item)
                             <th colspan="3" style="text-align: center;  background: #fff;" >{{$item->leave_type_name}}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($leave_type_setting as $item)
                             <th style="text-align: center;  background: #ffeb3b61;" >Total</th>
                             <th style="text-align: center;  background: #ff222226;" >Taken</th>
                             <th style="text-align: center;  background: #00ff0a54;" >Available</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($employee_list as $item)
                        <tr>
                            <td style="text-align: center;">{{++$i}}</td>
                            <td style="text-align: center;">{{$item->employee_id}}</td>
                            <td style="text-align: center;">{{$item->emp_first_name}} {{$item->emp_last_name}}</td>
                            <td style="text-align: center;">{{$item->company_name}}</td>
                            @foreach($leave_type_setting as $lts)
                            <?php 
                                $leaveLists = DB::table('tb_employee_leave_application')
                                ->where([['employee_id', '=', $item->id], ['leave_type_id', '=', $lts->id]])
                                ->whereYear('leave_starting_date', date('Y'))
                                ->where('status', '=', 1)->sum('actual_days');
                             ?>
                             <td style="text-align: center;  background: #ffeb3b1f;" >{{$lts->total_leave_days}}</td>
                             <td style="text-align: center;  background: #ff22220d;" >{{$leaveLists}}</td>
                             <td style="text-align: center;  background: #00ff0a1f;" >{{(($lts->total_leave_days)-($leaveLists))}}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            pageLength: 25
        });
    });
</script>
@endsection



