@extends('layout.master')
@section('title','New Doctor')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> New Doctor</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[16][1]))
            <a href="{{route('doctor.list')}}" target="_BLANK" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-list"></i> Doctor List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
            {!! Form::open(['method'=>'POST', 'route'=>'doctor.new.store', 'files'=> true]) !!}
            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_name" class="control-label top-padding-6">Doctor Name <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input autocomplete="off"  id="doctor_name" maxlength="150" name="doctor_name" placeholder="Doctor Name" type="text" class="form-control" required/></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_name" class="control-label top-padding-6">Doctor Bengali Name</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input autocomplete="off"  id="doctor_name" maxlength="150" name="doctor_bangla_name" placeholder="Doctor Bengali Name" type="text" class="form-control"/></div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="contact_number" class="control-label top-padding-6">Department <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                             <select class="form-control mySelect2" name="doctor_department_id" required>
                                 <option value="">Select Department</option>
                                 @foreach($department_list as $departments)
                                 <option value="{{$departments->id}}">{{$departments->department_name}}</option>
                                 @endforeach
                             </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="contact_number" class="control-label top-padding-6">Designation <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control mySelect2" name="doctor_designation_id" required>
                                    <option value="">Select Designation</option>
                                    @foreach($designation_list as $designations)
                                        <option value="{{$designations->id}}">{{$designations->designation_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_qualification" class="control-label top-padding-6">Qualification</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i>
                                    <textarea id="doctor_qualification" name="doctor_qualification" rows="5" autocomplete="off" class="form-control" placeholder="Qualification"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="qualification" class="control-label top-padding-6">Qualification Bengali</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i>
                                    <textarea id="qualification" name="doctor_bangla_qualification" rows="5" autocomplete="off" class="form-control" placeholder="Qualification in Bengali" ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_email" class="control-label top-padding-6">Email <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input id="doctor_email" type="email" autocomplete="off" name="doctor_email" class="form-control" placeholder="Doctor Email Address" required /></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="date_of_birth" class="control-label top-padding-6">Password <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input type="password" autocomplete="off" name="doctor_password" class="form-control" required placeholder="Password" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_phone" class="control-label top-padding-6">Phone <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input id="doctor_phone" autocomplete="off" maxlength="150" name="doctor_phone" type="text" class="form-control" required placeholder="Doctor Phone" /></div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_app_fee" class="control-label top-padding-6">Appointment Fee <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input id="doctor_app_fee" maxlength="150" autocomplete="off" name="doctor_app_fee" type="number" step="any" class="form-control" placeholder="Appointment Fee Amount " required /></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_room" class="control-label top-padding-6">Room No</label>
                            </div>
                            <div class="col-sm-9">
                             <input type="text" name="doctor_room" id="doctor_room" class="form-control" placeholder="Doctor's Room">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_registration_no" class="control-label top-padding-6">Registration No <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right"><i data-hover="tooltip"></i><input id="doctor_registration_no" autocomplete="off" name="doctor_registration_no" placeholder="Registration number" type="text" class="form-control" required/></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_joindate" class="control-label top-padding-6">Join Date <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="doctor_joindate"  id="doctor_joindate" autocomplete="off" class="form-control myDatepicker" placeholder="Date of Joining" required >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="doctor_address" class="control-label top-padding-6">Address <span style="color:red">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right">
                                   <textarea class="form-control" rows="5" name="doctor_address" placeholder="Doctor Address" required ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="photo">Photo</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" id="photo" name="doctor_photo">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="remarks" class="control-label top-padding-6">Remarks</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-icon right">
                                   <textarea class="form-control" rows="5" name="remarks" placeholder="Remarks" required ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group text-center col-sm-12">
                    <hr>
                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> Save Information</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection