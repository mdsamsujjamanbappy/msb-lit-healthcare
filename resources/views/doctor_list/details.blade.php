@extends('layout.master')
@section('title','Doctor Profile')
@section('extra_css')
@endsection

@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down">
                </div>
                <div class="tx-24 hidden-xs-down">

                </div>
            </div><!-- card-header -->
            <div class="card-body card_body_profile_1">
                <div class="card-profile-img">
                    <img src="{{asset('doctor_photo/'.$doctor_details->doctor_photo)}}" alt="">
                </div><!-- card-profile-img -->
                <h3 class="tx-normal tx-roboto tx-white">{{$doctor_details->doctor_name}}</h3>
                <div class="profile_content_1_1 d-flex wd-md-600 mg-md-l-auto mg-md-r-auto mg-b-25 mt-3">
                    <div class="p_left_1">
                        <p class=""><span>Join date :</span> <span>{{$doctor_details->doctor_joindate}}</span></p>
                    </div>
                    <div class="p_center_1">
                        <p class=""><span>Email :</span> <span>{{$doctor_details->doctor_email}}</span></p>
                    </div>
                    <div class="p_right_1">
                        <p class=""><span>Phone :</span> <span>{{$doctor_details->doctor_phone}}</span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="dib_button_profile mr-4">
                            @if(!empty($aclList[16][3]))
                            <a target="_BLANK" href="{{route('doctor.edit', base64_encode($doctor_details->id))}}" class="btn btn-sm btn-primary" title="Edit Details"><i class="fa fa-edit"></i> Edit Information</a>
                            @endif
                        </div>
                        <div class="dib_button_profile">
                        
                        </div>
                    </div>

                </div>
            </div><!-- card-body -->
        </div><!-- card -->
        <div class="pt-3 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist" id="my_tab_1">
                <li class="nav-item"><a class="nav-link active pb-3" data-toggle="tab" href="#profile" role="tab">Profile</a></li>
                <li class="nav-item "><a class="nav-link pb-3" data-toggle="tab" href="#nominee" role="tab">Nominee</a></li>
            </ul>
        </div>
        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="profile">
                <div class="row">
                    <div class="col-lg-11 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <p>Personal Information</p>
                            </div>
                            <div class="card-body card_body_2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Doctor Name</span><span>: {{$doctor_details->doctor_name}} </span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Doctor Bengali Name</span><span>: {{$doctor_details->doctor_bangla_name}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Department Name</span><span>: {{$doctor_details->department_name}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Designation Name</span><span>: {{$doctor_details->designation_name}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Qualification</span><span>: {{$doctor_details->doctor_qualification}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Qualification Bengali</span><span>: {{$doctor_details->doctor_bangla_qualification}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Appointment Fee</span><span>: {{$doctor_details->doctor_app_fee}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Room No</span><span>: {{$doctor_details->doctor_room}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Registration No</span><span>: {{$doctor_details->doctor_registration_no}}</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <p><span>Address</span><span>: {{$doctor_details->doctor_address}}</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><span>Remarks</span><span>: {{$doctor_details->remarks}}</span></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div><!-- row -->
            </div><!-- tab-pane -->
            
            <div class="tab-pane fade" id="nominee">
                <div class="row">
                    <div class="col-lg-9 m-auto">
                        <div class="card">
                            <div class="card-header card_header_2 d-flex">
                                <div class="col-6">
                                    <p>Nominee</p>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="" class="btn  btn-info btn-sm  custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#employee_nominee_create_modal"><i class="fas fa-plus-circle"></i> Add New</a>
                                </div>
                            </div>
                            <div class="card-body card_body_2">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){

    });
</script>
@endsection