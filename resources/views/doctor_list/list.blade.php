@extends('layout.master')
@section('title','Doctor List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Doctor List</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[16][2]))
            <a href="{{route('doctor.new.create')}}" target="_BLANK" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Doctor Name</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Phone</th>
                            <th>Joining Date</th>
                            <th>Registration Number</th>
                            <th>Status</th>
                            <th>Company Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($doctor_list as $data)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$data->doctor_name}}</td>
                            <td>{{$data->department_name}}</td>
                            <td>{{$data->designation_name}}</td>
                            <td>{{$data->doctor_phone}}</td>
                            <td>@if(!empty($data->doctor_joindate)){{date('d-m-Y', strtotime($data->doctor_joindate))}} @endif</td>
                            <td>{{$data->doctor_registration_no}}</td>
                            <td>
                                @if($data->status==1)
                                    <span style="color: green;">Active</span>
                                @else
                                    <span style="color: red;">Inactive</span>
                                @endif
                            </td>
                            <td>{{$data->company_name}}</td>
                            <td>
                            @if(!empty($aclList[16][5]))
                                <a target="_BLANK" href="{{route('doctor.details', base64_encode($data->id))}}" class="btn btn-sm btn-primary" title="View Details"><i class="fa fa-eye"></i></a>
                            @endif
                            @if(!empty($aclList[16][3]))
                                <a target="_BLANK" href="{{route('doctor.edit', base64_encode($data->id))}}" class="btn btn-sm btn-info" title="Edit Details"><i class="fa fa-edit"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection