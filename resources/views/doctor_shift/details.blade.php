@extends('layout.master')
@section('title','Doctor Shift Details')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Doctor Shift Details</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[16][1]))
            <a href="{{route('doctor.shift.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-list"></i> Doctor Shift List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <div class="row">
                    <div class="col-md-4">
                        @if(!empty($shiftDatasingle->doctor_photo))
                        <center>
                            <img width="220px" height="220px" src="{{asset('doctor_photo/'. $shiftDatasingle->doctor_photo)}}" alt="" class="img-responsive" />
                        </center>
                        <hr>
                        @endif
                        <table class="table table-striped table-hover">
                            <tbody>
                            <tr>
                                <td width="30%">Name</td>
                                <td>{{$shiftDatasingle->doctor_name}}</td>
                            </tr>
                            <tr>
                                <td>Department</td>
                                <td>{{$shiftDatasingle->department_name}}</td>
                            </tr>
                            <tr>
                                <td>Designation</td>
                                <td>{{$shiftDatasingle->designation_name}}</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>{{$shiftDatasingle->doctor_phone}}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>{{$shiftDatasingle->doctor_address}}</td>
                            </tr>
                            <tr>
                                <td>Application Fee</td>
                                <td>{{$shiftDatasingle->doctor_app_fee}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    @if($shiftDatasingle->status==1)
                                      <span class="label label-success">Active</span>
                                       @else
                                        <span class="label label-danger">Inactive</span>
                                     @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="col-md-8">
                        <table class="table table-bordered">
                            <tr>
                                <th class="msb-bg-table-td">Week Day</th>
                                <th class="msb-bg-table-td">Shift Time</th>
                            </tr>
                        @foreach($shiftData as $shiftdata)
                            <tr>
                                <td>{{$shiftdata->shift_day}}</td>
                                <td>
                                    @if($shiftdata->start_time=='')
                                    Not available
                                   @else
                                    {{$shiftdata->start_time}} to {{$shiftdata->end_time}}
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection