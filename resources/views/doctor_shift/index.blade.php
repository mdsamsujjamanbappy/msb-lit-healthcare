@extends('layout.master')
@section('title','Doctor Shift List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Doctor Shift List</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[16][2]))
            <a href="{{route('doctor.shift.create')}}" target="_BLANK" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-plus-circle"></i> Add New</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">

                 <table id="myDatatable25" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Department</th>
                            <th>Doctor Name</th>
                            <th>Doctor Phone</th>
                            <th>Doctor Email</th>
                            <th>Registration No</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         @php $sn=0; @endphp
                          @foreach($shiftdata as $shift)
                              @php $sn++; @endphp
                            <tr>
                                <td>{{$sn}}</td>
                                <td>{{$shift->doctor_details->department_name}}</td>
                                <td>{{$shift->doctor_details->doctor_name}}</td>
                                <td>{{$shift->doctor_details->doctor_phone}}</td>
                                <td>{{$shift->doctor_details->doctor_email}}</td>
                                <td>{{$shift->doctor_details->doctor_registration_no}}</td>
                                <td width="15%"><a href="{{route('doctor.shift.view', base64_encode($shift->doctor_id))}}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> View Shift </a>  <a onclick="return confirm('are you sure??')" href="{{route('doctor.shift.delete', base64_encode($shift->doctor_id))}}" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    });
</script>
@endsection