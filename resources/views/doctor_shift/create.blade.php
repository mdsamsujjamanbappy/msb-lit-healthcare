@extends('layout.master')
@section('title','New Doctor Shift')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> New Doctor Shift</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[16][1]))
            <a href="{{route('doctor.shift.list')}}" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium"> <i class="fas fa-list"></i> Doctor Shift List</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
            {!! Form::open(['method'=>'POST', 'route'=>'doctor.shift.store']) !!}
            <div class="row">
                <div class="col-md-12" style="padding: 0">
                    <div class="form-group">
                        <label>Select Doctor</label>
                        <select id="doctor_id" class="form-control doctor_shift" name="doctor_id" required>
                            <option value="">Select</option>
                            @foreach($doctor_list as $doctors)
                                <option value="{{$doctors->id}}">{{$doctors->doctor_name}}({{$doctors->doctor_phone}})</option>
                            @endforeach
                        </select>
                    </div>
                    <hr>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tr>
                            <th>Week day</th>
                            <th>Shift Start Time</th>
                            <th>Shift End Time</th>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select id="shift_day_id" name="day_select[]" class="form-control">
                                        <option value="Saturday">Saturday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time">
                                        <input type='time' name="shift_start[]"  class="form-control s_dat" value="" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time">
                                        <input type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select name="day_select[]" id="shift_day_id" class="form-control">
                                        <option value="Sunday">Sunday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time2">
                                        <input type='time' name="shift_start[]"   class="form-control s_dat" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time2">
                                        <input type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select name="day_select[]" id="shift_day_id" class="form-control">
                                        <option value="Monday">Monday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time3">
                                        <input type='time' name="shift_start[]"   class="form-control s_dat" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time3">
                                        <input type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select name="day_select[]" id="shift_day_id" class="form-control">
                                        <option value="Tuesday">Tuesday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time4">
                                        <input type='time' name="shift_start[]"   class="form-control s_dat" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time4">
                                        <input style="height: 29px;" type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select name="day_select[]" id="shift_day_id" class="form-control">
                                        <option value="Wednesday">Wednesday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time5">
                                        <input type='time' name="shift_start[]"   class="form-control s_dat" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time5">
                                        <input type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select name="day_select[]" id="shift_day_id" class="form-control">
                                        <option value="Thursday">Thursday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time6">
                                        <input type='time' name="shift_start[]"   class="form-control s_dat" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time6">
                                        <input type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <select name="day_select[]" id="shift_day_id" class="form-control">
                                        <option value="Friday">Friday</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="start_shift_time7">
                                        <input type='time' name="shift_start[]"   class="form-control s_dat" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class='input-group date' id="end_shift_time7">
                                        <input type='time' name="shift_end[]"  class="form-control" />
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-time"></span>
                                          </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
               <button style="width: 100%;" type="submit" class="btn btn-success add_btn"><i class="fa fa-check"></i> Add Doctor Shift</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection