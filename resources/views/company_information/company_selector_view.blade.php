@extends('layout.master')
@section('title','Company Selector')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Company Selector</h4>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
                @if(!empty($aclList[14][1]))
                <table class="table table-hover table-bordered">
                    <tbody>
                        @foreach($company_list as $ci)
                        <tr>
                            <td>@if(Auth::user()->company_id==$ci->id) <span style="color:white; padding: 7px 5px; font-size: 16px;" class="bg-success"><b>{{$ci->company_name}}</b></span>@else  <a href="{{route('company_information.global_settings_company', base64_encode($ci->id))}}" onclick="return confirm('Are you sure to select?')" title="Select this company" class="btn btn-white">{{$ci->company_name}}</a> @endif </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <h3 style="color: red;"><b>Please contact with Super Admin/ Admin.</b></h3>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    });
</script>
@endsection