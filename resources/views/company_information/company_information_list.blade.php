@extends('layout.master')
@section('title','Company List')
@section('extra_css')
@endsection
@section('content')

    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Company List</h4>
        </div>

        <div class="pagetitle-btn">
            <a href="" class="btn  btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="company_information" class="table table-striped table-bordered " >
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Company Logo</th>
                            <th>Company Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Primary Address</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($company_info as $ci)
                        <tr>
                            <td>{{++$i}}</td>
                            <td><img width="60px" height="60px" class="" src="{{asset('company_logo/'.$ci->company_logo)}}" alt="{{$ci->company_logo}}"></td>
                            <td>{{$ci->company_name}}</td>
                            <td>{{$ci->company_phone}}</td>
                            <td>{{$ci->company_email}}</td>
                            <td>{{$ci->company_address1}}</td>
                            <td>
                                @if($ci->status==1)
                                    <span style="color: green;">Active</span>
                                @else
                                    <span style="color: red;">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <button type="button" value="{{base64_encode($ci->id)}}" class="view_details btn btn-sm btn-success" title="View Details"><i class="fa fa-eye"></i></button>
                                <button type="button" value="{{base64_encode($ci->id)}}" class="edit btn btn-sm btn-info" title="Edit Information"><i class="fa fa-edit"></i></button>
                                <a class="btn btn-sm btn-danger" href="{{route('company_information.destroy', base64_encode($ci->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div id="add_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Company </h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'company_information.new.store','files'=>true]) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Company Information</h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Name</label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="company_name" autocomplete="off" value="" required="" placeholder="Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Tagline </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="company_tagline" value=""  autocomplete="off" required="" placeholder="Tagline">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Phone </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="company_phone" value=""  autocomplete="off" required="" placeholder="Phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Email </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="email" class="form-control" name="company_email" value=""  autocomplete="off" required="" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Office Address1 </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="company_address1" id="company_address1" cols="" rows="2" class="form-control" placeholder="Address1"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Office Address2 </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="company_address2" id="company_address2" cols="" rows="2" class="form-control" placeholder="Address2"></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Logo </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="file" class="form-control" name="company_logo" value=""  autocomplete="off" placeholder="Logo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="select_2" data-placeholder="Select Status">
                                                    <option value="">Select  Status</option>
                                                    <option value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><hr></div>

                                <div class="form-group text-center col-sm-12">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->



            <div id="details_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-list"></i> Details Company Information</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Company Information</h6>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Name</label>
                                            <div class="col-sm-9 pl-0">
                                                <input type="text" class="form-control" id="details_company_name" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Tagline </label>
                                            <div class="col-sm-9 pl-0">
                                                <input type="text" class="form-control" id="details_company_tagline" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Phone </label>
                                            <div class="col-sm-9 pl-0">
                                                <input type="text" class="form-control" id="details_company_phone" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Email </label>
                                            <div class="col-sm-9 pl-0">
                                                <input type="email" class="form-control" id="details_company_email" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Office Address1 </label>
                                            <div class="col-sm-9 pl-0">
                                                <textarea id="details_company_address1" class="form-control" disabled ></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1"> Office Address2 </label>
                                            <div class="col-sm-9 pl-0">
                                                <textarea id="details_company_address2" class="form-control" disabled ></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Logo </label>
                                            <div class="col-sm-9 pl-0">
                                                <input class="form-control" id="details_company_logo" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-3 control-label form-label-1">Status </label>
                                            <div class="col-sm-9 pl-0">
                                                <select class="form-control" id="details_status" disabled >
                                                    <option value="">Select  Status</option>
                                                    <option value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><hr></div>

                                <div class="form-group text-center col-sm-12">
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->



            <div id="edit_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-edit "></i> Edit Company Information</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'company_information.update','files'=>true]) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Company Information</h6>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Name</label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="company_name" autocomplete="off" id="edit_company_name" required="" placeholder="Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Tagline </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="company_tagline" id="edit_company_tagline"  autocomplete="off" placeholder="Tagline">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Phone </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="company_phone" id="edit_company_phone"  autocomplete="off" required="" placeholder="Phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Email </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="email" class="form-control" name="company_email" id="edit_company_email"  autocomplete="off" required="" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Office Address1 </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="company_address1" id="edit_company_address1" rows="2" class="form-control" placeholder="Address1" required="" ></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Office Address2 </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="company_address2" id="edit_company_address2" cols="" rows="2" class="form-control" placeholder="Address2"></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Logo </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="file" class="form-control" name="company_logo" id="company_logo"  autocomplete="off" placeholder="Logo">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="edit_status" data-placeholder="Select Status">
                                                    <option value="">Select  Status</option>
                                                    <option value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><hr></div>

                                <div class="form-group text-center col-sm-12">
                                    <input type="hidden" name="id" id="edit_id">
                                    <input type="hidden" name="edit_company_logo" id="edit_company_logo">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Update </button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){

        $('#company_information').DataTable();

        $(document).on('click', '.view_details', function(){
            var id = $(this).attr('value');
            $.ajax({
             type: "GET",
             url:"{{url('company/details')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 // console.log(response);
                 $("#details_modal").modal('show');
                 $('#details_id').val(response.id);
                 $('#details_company_logo').val(response.company_logo);
                 $('#details_company_name').val(response.company_name);
                 $('#details_company_tagline').val(response.company_tagline);
                 $('#details_company_phone').val(response.company_phone);
                 $('#details_company_email').val(response.company_email);
                 $('#details_company_address1').val(response.company_address1);
                 $('#details_company_address2').val(response.company_address2);
                 $('#details_status').val(response.status);
             },
                error:function(response){
                    console.log(response);
                },
            })
        });


        $(document).on('click', '.edit', function(){
            var id = $(this).attr('value');
            $.ajax({
             type: "GET",
             url:"{{url('company/edit')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 // console.log(response);
                 $("#edit_modal").modal('show');
                 $('#edit_id').val(response.id);
                 $('#edit_company_logo').val(response.company_logo);
                 $('#edit_company_name').val(response.company_name);
                 $('#edit_company_tagline').val(response.company_tagline);
                 $('#edit_company_phone').val(response.company_phone);
                 $('#edit_company_email').val(response.company_email);
                 $('#edit_company_address1').val(response.company_address1);
                 $('#edit_company_address2').val(response.company_address2);
                 $('#edit_status').val(response.status);
             },
                error:function(response){
                    console.log(response);
                },
            })
        });
    });
</script>
@endsection



