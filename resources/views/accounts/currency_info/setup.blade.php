@extends('layout.master')
@section('title','Accounts Currency Setup')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Accounts Currency Setup </h4>
        </div>

        <div class="pagetitle-btn">
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                {!! Form::open(['method'=>'POST', 'route'=>'accounts.currency_info.setup.update']) !!}
                    <div class="row">
                
                         <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Currency Code <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="@if(!empty($currency_info->currency_code)){{$currency_info->currency_code}}@endif" name="currency_code" autocomplete="off" placeholder="Ledger Name" required="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Prefix</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="@if(!empty($currency_info->prefix)){{$currency_info->prefix}}@endif" name="prefix" autocomplete="off" placeholder="Prefix" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Suffix</label>
                                <div class="col-sm-8 pl-0">
                                    <input type="text" class="form-control" value="@if(!empty($currency_info->suffix)){{$currency_info->suffix}}@endif" name="suffix" autocomplete="off" placeholder="Suffix" >
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="form-label" class="col-sm-3 control-label form-label-1">Decimal Point Limit <span class="msb-txt-red">*</span></label>
                                <div class="col-sm-8 pl-0">
                                    <input type="number" class="form-control" value="@if(!empty($currency_info->decimal_point_number)){{$currency_info->decimal_point_number}}@endif" name="decimal_point_number" autocomplete="off" placeholder="Decimal Point Limit"  required="" min="1" max="8">
                                </div>
                            </div>
                        </div>

                        
                    </div>
                    <hr>
                    <div class="form-group text-center col-sm-12">
                        <input type="" name="id" value="@if(!empty($currency_info->id)){{$currency_info->id}}@endif" hidden >
                        <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"> <i class="fa fa-save"></i> @if(!empty($currency_info->id))Update @else Save @endif  Information</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('extra_js')
@endsection
