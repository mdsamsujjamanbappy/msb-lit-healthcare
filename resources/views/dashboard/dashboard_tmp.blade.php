@extends('layout.master')

@section('title','Dashboard')

@section('extra_css')
@endsection

@section('content')

    <h1 style="padding: 20px;">Welcome <b>{{Auth::user()->name}}</b></h1>
    <?php if(checkPermission(['super_admin']) || checkPermission(['group_admin'])){ ?>
    <?php }else{ ?>
        <br>
        <?php if (checkPermission(['merchandise_visitor'])){}else{ ?>
        <a href="{{route('employee.attendance.give_attendance_by_app')}}" class="btn btn-primary ml-3 pl-5 pr-5 tx-uppercase tx-bold tx-spacing-6 tx-14"> <br> <i class="fas fa-clock"></i> Give Attendace <br><br></a>
        <?php } ?>
    <?php } ?>

    

@endsection

@section('extra_js')

    <script src="{{asset('theme/')}}/lib/rickshaw/vendor/d3.min.js"></script>
    <script src="{{asset('theme/')}}/lib/rickshaw/vendor/d3.layout.min.js"></script>
    <script src="{{asset('theme/')}}/lib/rickshaw/rickshaw.min.js"></script>
    <script src="{{asset('theme/')}}/lib/jquery.flot/jquery.flot.js"></script>
    <script src="{{asset('theme/')}}/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="{{asset('theme/')}}/lib/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="{{asset('theme/')}}/lib/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="{{asset('theme/')}}/lib/echarts/echarts.min.js"></script>
    <script src="{{asset('theme/')}}/js/dashboard.js"></script>

@endsection



