@extends('layout.master')
@section('title','Dashboard')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle">
        <i class="icon ion-ios-home-outline"></i>
        <div>
            <h4>Dashboard</h4>
            <p class="mg-b-0">Do bigger things with System</p>
        </div>
    </div>

    <div class="br-pagebody">
        <div class="row row-sm row-cards-one">
            <div class="col-sm-6 col-xl-4">
                <div class="mycard bg7 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fa fa-users tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", $employee_count)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Employee</span>
                        </div>
                    </div>
                    <div id="ch1" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-sm-t-0">
                <div class="mycard bg1 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fab fa-pinterest tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch3" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-xl-t-0">
                <div class="mycard bg4 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-check-double tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch2" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->

        <div class="row row-sm mg-t-20  row-cards-one">
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-xl-t-0">
                <div class="mycard bg5 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-store tx-60 lh-0 tx-white op-9"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total </p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-xl-t-0">
                <div class="mycard bg2 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fab fa-pinterest tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10"></p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-xl-t-0">
                <div class="mycard bg6 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-sync tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10"></p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum </span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->

        <div class="row row-sm row-cards-one">
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-xl-t-0">
                <div class="mycard bg3 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-layer-group tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch4" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-4">
                <div class="mycard bg7 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fa fa-users tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch1" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-4 mg-t-20 mg-sm-t-0">
                <div class="mycard bg1 rounded overflow-hidden">
                    <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                        <i class="fas fa-industry tx-60 lh-0 tx-white op-8"></i>
                        <div class="mg-l-20">
                            <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white mg-b-10">Total</p>
                            <p class="tx-28 tx-white tx-lato tx-bold mg-b-0 lh-1">{{sprintf("%03d", 0)}}</p>
                            <span class="tx-14 tx-roboto tx-white">Lorem Ipsum</span>
                        </div>
                    </div>
                    <div id="ch3" class="ht-50 tr-y-1"></div>
                </div>
            </div><!-- col-3 -->
        </div><!-- row -->
    </div>
@endsection

@section('extra_js')
    <script src="{{asset('theme/')}}/lib/jquery.flot/jquery.flot.js"></script>
    <script src="{{asset('theme/')}}/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="{{asset('theme/')}}/lib/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="{{asset('theme/')}}/lib/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="{{asset('theme/')}}/lib/echarts/echarts.min.js"></script>
    <script src="{{asset('theme/')}}/js/dashboard.js"></script>
@endsection