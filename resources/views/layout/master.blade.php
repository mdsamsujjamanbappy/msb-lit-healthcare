<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Lit Healthcare">
    <meta name="author" content="Samsujjaman Bappy">
    <link rel='shortcut icon' type='image/x-icon' href="{{asset('theme/img/favicon.png')}}">

    <title>
        @yield('title')
    </title>

    <!-- vendor css -->

    @include('include.css')
    @yield('extra_css')

</head>

<body >
<!-- ########## START: LEFT PANEL ########## -->
@include('include.logo')
<!-- br-sideleft -->
@include('include.left_sidebar')
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<!-- br-header -->
@include('include.header_top')
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: RIGHT PANEL ########## -->
<!-- br-sideright -->
{{-- @include('include.right_sidebar') --}}
<!-- ########## END: RIGHT PANEL ########## --->

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    @yield('content')
    @include('include.footer')
</div>

@include('include.js')
@yield('extra_js')

<script>
    $(function(){
        'use strict'
        $(window).resize(function(){
            minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
                // show only the icons and hide left menu label by default
                $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
                $('body').addClass('collapsed-menu');
                $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
                $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
                $('body').removeClass('collapsed-menu');
                $('.show-sub + .br-menu-sub').slideDown();
            }
        }
    });
</script>
</body>
</html>