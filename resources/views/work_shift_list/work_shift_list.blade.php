@extends('layout.master')
@section('title','Work Shift List')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Work Shift List</h4>
        </div>

        <div class="pagetitle-btn">
            @if(!empty($aclList[26][2]))
            <a href="" class="btn btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
            @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered " >
                    <thead>
                        <tr>
                            <th width="5%">SN</th>
                            <th>Work Shift Name</th>
                            <th>Entry Time</th>
                            <th>Exit Time</th>
                            <th>Buffer Time</th>
                            <th>Remarks</th>
                            <th>Weekend</th>
                            <th>Status</th>
                            <th width="15%" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($work_shift_list as $item)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$item->shift_name}}</td>
                            <td>{{date('h:i a', strtotime($item->entry_time))}}</td>
                            <td>{{date('h:i a', strtotime($item->exit_time))}}</td>
                            <td>{{date('h:i a', strtotime($item->buffer_time))}}</td>
                            <td>{{$item->remarks}}</td>
                            <td style="color:red;">
                                <?php 
                                    $employee_shift_weekend = DB::table('tb_employee_shift_weekend')->where([['shift_id', '=', $item->id], ['status', '=', 1]])->get();
                                    foreach($employee_shift_weekend as $data){
                                        echo $data->day_name.", ";
                                    }
                                 ?>
                                <button type="button" value="{{base64_encode($item->id)}}" class="edit_weekend pull-right" title="Edit Weekends"> + </button>
                            </td>
                            <td>
                                @if($item->status==1)
                                    <span style="color: green;">Active</span>
                                @else
                                    <span style="color: red;">Inactive</span>
                                @endif
                            </td>
                            <td style="text-align: center;">
                            @if(!empty($aclList[26][3]))
                                <button type="button" value="{{base64_encode($item->id)}}" class="edit btn btn-sm btn-info" title="Edit Information"><i class="fa fa-edit"></i></button>
                            @endif
                            @if(!empty($aclList[26][4]))
                                <a class="btn btn-sm btn-danger" href="{{route('work_shift.destroy', base64_encode($item->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div id="add_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Work Shift </h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'work_shift.new.store']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>New Work Shift Information</h6>
                                        <hr>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Work Shift Name <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="shift_name" autocomplete="off" value="" required="" placeholder="Work Shift Name">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Entry Time <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="time" class="form-control" name="entry_time" autocomplete="off" value="" required="" placeholder="Entry Time ">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Exit Time <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="time" class="form-control" name="exit_time" autocomplete="off" value="" required="" placeholder="Exit Time ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Buffer Time <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="time" class="form-control" name="buffer_time" autocomplete="off" value="" required="" placeholder="Buffer Time ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Remarks </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="remarks" id="remarks" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Weekends </label>
                                            <div class="col-sm-8 pl-0">
                                                @foreach($weekend_holiday as $item)
                                                    <input type="checkbox" @if($item->status==1) checked @endif value="{{$item->day_name}}" name="weekends[]"> {{$item->day_name}} <br>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="select_2" data-placeholder="Select Status" required="">
                                                    <option value="">Select  Status</option>
                                                    <option selected="" value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><br></div>

                                <div class="form-group text-center col-sm-12">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->


            <div id="edit_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-edit "></i> Edit Work Shift Information</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'work_shift.update']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Work Shift Information</h6>
                                    <hr>

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Work Shift Name <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="text" class="form-control" name="shift_name" id="shift_name_edit" autocomplete="off" value="" required="" placeholder="Work Shift Name">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Entry Time <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="time" class="form-control" name="entry_time" id="entry_time_edit" autocomplete="off" value="" required="" placeholder="Entry Time ">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Exit Time <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="time" class="form-control" name="exit_time" id="exit_time_edit" autocomplete="off" value="" required="" placeholder="Exit Time ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Buffer Time <span style="color:red">*</span></label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="time" class="form-control" name="buffer_time" id="buffer_time_edit" autocomplete="off" value="" required="" placeholder="Buffer Time ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Remarks </label>
                                            <div class="col-sm-8 pl-0">
                                                <textarea name="remarks" id="remarks_edit" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1">Status </label>
                                            <div class="col-sm-8 pl-0">
                                                <select class="form-control" name="status" id="status_edit" data-placeholder="Select Status" required="">
                                                    <option value="">Select  Status</option>
                                                    <option value='1'>Active</option>
                                                    <option value='0'>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12"><br /></div>

                                <div class="form-group text-center col-sm-12">
                                    <input type="hidden" name="id" id="edit_id">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Update </button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->

            <div id="edit_weekend_modal" class="modal fade effect-sign">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-edit "></i> Edit Work Shift Information</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <span id="form_result"></span>
                            <div class="modal_body_inner">
                                {!! Form::open(['method'=>'POST','route'=>'work_shift.weekend.update']) !!}
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h6>Work Shift Weekend Information</h6>
                                    <hr>

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Weekend Days <span style="color:red">*</span> : </label>
                                            <div class="col-sm-8 pl-0">
                                                <input type="checkbox"  value="Saturday" id="saturday_edit" name="weekends[]"> Saturday <br>
                                                <input type="checkbox"  value="Sunday" id="Sunday_edit" name="weekends[]"> Sunday <br>
                                                <input type="checkbox"  value="Monday" id="Monday_edit" name="weekends[]"> Monday <br>
                                                <input type="checkbox"  value="Tuesday" id="Tuesday_edit" name="weekends[]"> Tuesday <br>
                                                <input type="checkbox"  value="Wednesday" id="Wednesday_edit" name="weekends[]"> Wednesday <br>
                                                <input type="checkbox"  value="Thursday" id="Thursday_edit" name="weekends[]"> Thursday <br>
                                                <input type="checkbox"  value="Friday" id="Friday_edit" name="weekends[]"> Friday <br>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-12"><br /></div>

                                <div class="form-group text-center col-sm-12">
                                    <input type="hidden" name="id" id="edit_shift_id">
                                    <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Update </button>
                                    <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->

        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){

        $('#datatable').DataTable();

        $(document).on('click', '.edit', function(){
            var id = $(this).attr('value');
            $.ajax({
             type: "GET",
             url:"{{url('work_shift/edit')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 console.log(response);
                 $("#edit_modal").modal('show');
                 $('#edit_id').val(response.id);
                 $('#shift_name_edit').val(response.shift_name);
                 $('#entry_time_edit').val(response.entry_time);
                 $('#exit_time_edit').val(response.exit_time);
                 $('#buffer_time_edit').val(response.buffer_time);
                 $('#remarks_edit').val(response.remarks);
                 $('#status_edit').val(response.status);
             },
                error:function(response){
                    console.log(response);
                },
            })
        });


        $(document).on('click', '.edit_weekend', function(){
            var id = $(this).attr('value');
            $("#edit_weekend_modal").modal('show');
            $.ajax({
             type: "GET",
             url:"{{url('work_shift/weekend/edit')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 console.log(response);
                 $("#edit_weekend_modal").modal('show');
                 $('#edit_shift_id').val(response.shift_id);
                 
                 if(response.Saturday=='Saturday'){
                    $('#saturday_edit').prop('checked', true);
                 }else{
                    $('#saturday_edit').prop('checked', false);
                 }

                 if(response.Sunday=='Sunday'){
                    $('#Sunday_edit').prop('checked', true);
                 }else{
                    $('#Sunday_edit').prop('checked', false);
                 }

                 if(response.Monday=='Monday'){
                    $('#Monday_edit').prop('checked', true);
                 }else{
                    $('#Monday_edit').prop('checked', false);
                 }

                 if(response.Tuesday=='Tuesday'){
                    $('#Tuesday_edit').prop('checked', true);
                 }else{
                    $('#Tuesday_edit').prop('checked', false);
                 }

                 if(response.Wednesday=='Wednesday'){
                    $('#Wednesday_edit').prop('checked', true);
                 }else{
                    $('#Wednesday_edit').prop('checked', false);
                 }

                 if(response.Thursday=='Thursday'){
                    $('#Thursday_edit').prop('checked', true);
                 }else{
                    $('#Thursday_edit').prop('checked', false);
                 }

                 if(response.Friday=='Friday'){
                    $('#Friday_edit').prop('checked', true);
                 }else{
                    $('#Friday_edit').prop('checked', false);
                 }
             },
                error:function(response){
                    console.log(response);
                },
            })
        });
    });
</script>
@endsection



