
@section('extra_js')
    <script>
        $(document).ready(function(){

            //Employee attendance list start
            $('#employee_attendance_list').DataTable({
                processing: true,
                serverSide: true,
                "pageLength": 25,
                ajax:{
                    url: "{{ route('employee.attendance.todays_attendance') }}",
                    data:{
                        "search_employee": "{{Request::get('search_employee')}}",
                        "search_from": "{{Request::get('search_from')}}",
                        "search_to": "{{Request::get('search_to')}}"
                    }
                },
                columns:[
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'entry_date',
                        name: 'entry_date',
                    },
                    {
                        data: 'day',
                        render: function(data, type, row, meta){
                            if (row.status == 'Weekend') {
                                return '<span style="color:Blue">'+data+'</span>'
                            }else{
                                return data
                            }
                        }
                    },
                    {
                        data: 'employee_id',
                        name: 'employee_id'
                    },
                    {
                        data: 'employee_name',
                        name: 'employee_name'
                    },
                    {
                        data: 'shift',
                        name: 'shift'
                    },
                    {
                        data: 'start_time',
                        name: 'start_time'
                    },
                    {
                        data: 'in_time',
                        name: 'in_time'
                    },
                    {
                        data: 'late',
                        render: function(data, type, row, meta){
                            if (Boolean(data)) {
                                return '<span style="color:red">'+data+'</span>'
                            }else{
                                return data
                            }
                        }
                    },
                    {
                        data: 'out_time',
                        render: function(data, type, row, meta){
                            return "{{ date('h:i a', strtotime("+data+")) }}"
                        }
                    },
                    {
                        data: 'status',
                        name: 'status',
                        render: function(data, type, full, meta){
                            if (data == 'Absent') {
                                return '<span style="background:red;color: #fff;padding: 3px">'+data+'</span>'
                            }
                            
                            if (data == 'Late') {
                                return '<span style="background:#9c27b0;color: #fff;padding: 3px">'+data+'</span>'
                            }

                            if (data == 'Present-Weekend') {
                                return '<span style="background:green;color: #fff;padding: 3px">'+data+'</span>'
                            }

                            if (data == 'Weekend') {
                                return '<span style="background:#00a8ff;color: #fff;padding: 3px">'+data+'</span>'
                            }

                            if (data == 'Present') {
                                return '<span style="background:green;color: #fff;padding: 3px">'+data+'</span>'
                            }

                            if (data == 'Leave') {
                                return '<span style="background:#273c75;color: #fff;padding: 3px">'+data+'</span>'
                            }
                        }
                    }
                ]
            });
            //Employee attendance list end

            if($().select2) {
                $('.select2').select2({
                    dropdownParent: $('#add_search_modal'),
                    placeholder: "--Select One--",
                    allowClear:true
                });
            }

            // date picker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd-mm-yy',
                autoclose: true
            });

        });
    </script>
@endsection



