@extends('layout.master')

@section('title','Attendance Importer')

@section('extra_css')
    {{--    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">--}}

@endsection
@section('content')
{{ Html::script('theme/js/sweetalert.min.js') }}
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Attendance Importer</h4>
        </div>

        <div class="pagetitle-btn">
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">

               {!! Form::open(['method'=>'POST','route'=>'employee.attendance.import_from_excel_store', 'files'=>true]) !!}
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group row">
                            <label for="form-label" class="col-sm-4 control-label form-label-1"> Attendance File <span style="color:red">*</span></label>
                            <div class="col-sm-8 pl-0">
                                <input type="file" class="form-control" name="excel_file" autocomplete="off" required="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group row">
                             <button class="btn btn-lg btn-teal" type="submit">Import</button>
                        </div>
                    </div>
                </div>

            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection
