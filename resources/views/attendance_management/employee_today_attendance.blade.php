@extends('layout.master')
@section('title','Todays Attendance Report')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> Today's Attendance History</h4>
        </div>

        <div class="pagetitle-btn">
        </div>

    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper table-wrapper-1">
                <table id="datatable" class="table table-bordered display responsive">
                    <thead>
                        <tr>
                            <th style="text-align: center;">SL</th>
                            <th style="text-align: center;">Date</th>
                            <th style="text-align: center;">Day</th>
                            <th style="text-align: center;">Employee ID</th>
                            <th style="text-align: center;">Employee Name</th>
                            <th style="text-align: center;">Shift</th>
                            <th style="text-align: center;">Start Time</th>
                            <th style="text-align: center;">In Time</th>
                            <th style="text-align: center;">Late</th>
                            <th style="text-align: center;">End Time</th>
                            <th style="text-align: center;">Out Time</th>
                            <th style="text-align: center;">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i=0; @endphp
                        @foreach($attendance_data as $data)
                        <tr>
                            <td style="text-align: center;">{{++$i}}</td>
                            <td style="text-align: center;">{{$data['entry_date']}}</td>
                            <td style="text-align: center;">{{$data['day']}}</td>
                            <td style="text-align: center;">{{$data['employee_id']}}</td>
                            <td style="text-align: center;">{{$data['employee_name']}}</td>
                            <td style="text-align: center;">{{$data['shift']}}</td>
                            <td style="text-align: center;">
                             @if(!empty($data['start_time']))
                                {{date('h:i a', strtotime($data['start_time']))}}
                            @endif</td>
                            <td style="text-align: center;">
                            @if(!empty($data['in_time']))
                                {{date('h:i a', strtotime($data['in_time']))}}
                            @endif
                            </td>
                            <td style="text-align: center;">{{$data['late']}}</td>
                            <td style="text-align: center;">
                            @if(!empty($data['end_time']))
                                {{date('h:i a', strtotime($data['end_time']))}}
                            @endif
                            </td>
                            <td style="text-align: center;">
                            @if(!empty($data['out_time'])&&($data['in_time']!=$data['out_time']))
                                {{date('h:i a', strtotime($data['out_time']))}}
                            @else

                                @if(!empty($data['in_time']))
                                    <span style="color:red;">Not Found</span>
                                @endif
                                
                            @endif
                            </td>
                            <td style="text-align: center;">
                                <?php 
                                    if ($data['status'] == 'Absent') {
                                        echo '<span style="background:red;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }
                                    
                                    if ($data['status'] == 'Late') {
                                        echo '<span style="background:#9c27b0;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Present-Weekend') {
                                        echo '<span style="background:green;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Weekend') {
                                        echo '<span style="background:#00a8ff;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Present') {
                                        echo '<span style="background:green;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }

                                    if ($data['status'] == 'Leave') {
                                        echo '<span style="background:#273c75;color: #fff;padding: 3px">'.$data['status'].'</span>';
                                    }
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div>

                </div>
            </div><!-- table-wrapper -->
        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable({
            pageLength:25
        });
    });
</script>
@endsection