@extends('layout.master')
@section('title','In Patient Occupancies')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
        <div>
            <h4> <i class="icon icon ion-ios-bookmarks-outline"></i> In Patient Occupancies</h4>
        </div>

        <div class="pagetitle-btn">
        @if(!empty($aclList[20][2]))
            <a href="" class="btn btn-info btn-sm custom-btn-1 btn-1 tx-11 tx-uppercase pd-y-8 pd-x-18 tx-mont tx-medium" data-toggle="modal" data-target="#add_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
        @endif
        </div>
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="table-wrapper scrollme">
            
                <table id="datatable" class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th class="msb-txt-center">SN</th>
                            <th class="msb-txt-center">Admission ID</th>
                            <th class="msb-txt-center">Patient ID</th>
                            <th class="msb-txt-center">Patient Name</th>
                            <th class="msb-txt-center">Accommodation Category</th>
                            <th class="msb-txt-center">Accommodation</th>
                            <th class="msb-txt-center">Admission Date</th>
                            <th class="msb-txt-center">Ref. Doctor Name</th>
                            <th class="msb-txt-center">Status</th>
                            <th class="msb-txt-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=0; @endphp
                    @foreach($data_list as $data)
                        <tr>
                            <td class="msb-txt-center">{{++$i}}</td>
                            <td class="msb-txt-center">{{$data->admission_id}}</td>
                            <td class="msb-txt-center">{{$data->patientId}}</td>
                            <td class="msb-txt-center">{{$data->patient_name}}</td>
                            <td class="msb-txt-center">{{$data->category_name}}</td>
                            <td class="msb-txt-center">{{$data->accommodation_name_number}}</td>
                            <td class="msb-txt-center">@if(!empty($data->admission_date)){{date('d-m-Y', strtotime($data->admission_date))}} @endif</td>
                            <td class="msb-txt-center">{{$data->doctor_name}}</td>
                            <td class="msb-txt-center">
                                @if($data->status==1)
                                    <span class="badge badge-success">Admitted</span>
                                @elseif($data->status==2)
                                    <span class="badge badge-warning">Discharged</span>
                                @elseif($data->status==3)
                                    <span class="badge badge-danger">Closed</span>
                                @endif
                            </td>
                            <td class="msb-txt-center">
                            @if(!empty($aclList[20][5]))
                                <a target="_BLANK" href="{{route('appointment.in_patient.occupancies.details', base64_encode($data->id))}}" class="btn btn-sm btn-primary" title="View Details"><i class="fa fa-eye"></i></a>
                            @endif
                            @if(!empty($aclList[20][3]))
                                <a target="_BLANK" href="{{route('appointment.in_patient.occupancies.edit', base64_encode($data->id))}}" class="btn btn-sm btn-info" title="Edit Details"><i class="fa fa-edit"></i></a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="add_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Add New Occupancies </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20 ">
                    <span id="form_result"></span>
                    <div class="modal_body_inner ">
                        {!! Form::open(['method'=>'POST','route'=>'appointment.in_patient.occupancies.store']) !!}
                        <div class="row">
                            <div class="col-12 mb-3">
                                <h6>New Occupancies Information</h6>
                                <hr>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row select_2_row_modal">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Patient <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control mySelect2ModalAdd" name="patient_id" id="patient_id" data-placeholder="Select Patient" required="">
                                            <option value="">Select a Patient</option>
                                            @foreach($patient_list as $item)
                                                <option value="{{$item->id}}"> {{$item->patient_id}} >> {{$item->patient_name}} >> {{$item->patient_phone}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row select_2_row_modal">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Accommodation <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control mySelect2ModalAdd" name="accommodation_id" id="accommodation_id" data-placeholder="Select Accommodation" required="">
                                            <option value="">Select a Accommodation</option>
                                            @foreach($accommodation_list as $item)
                                                <option value="{{$item->id}}"> {{$item->category_name}} >> {{$item->accommodation_name_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row select_2_row_modal">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1">Reference Doctor <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <select class="form-control mySelect2ModalAdd" name="doctor_id" id="doctor_id" data-placeholder="Select Doctor" required="">
                                            <option value="">Select a Doctor</option>
                                            @foreach($doctor_list as $item)
                                                <option value="{{$item->id}}">{{$item->department_name}} >> {{$item->doctor_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1"> Admission Date <span class="msb-txt-red">*</span></label>
                                    <div class="col-sm-8 pl-0">
                                        <input step="text" class="form-control myDatepicker" value="{{date('Y-m-d')}}" name="admission_date" autocomplete="off" required="" placeholder=" Admission Date">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1"> Reason of Admission </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="admission_reason" id="admission_reason" cols="" rows="3" class="form-control" placeholder="Reason of Admission"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="form-label" class="col-sm-3 control-label form-label-1"> Remarks </label>
                                    <div class="col-sm-8 pl-0">
                                        <textarea name="remarks" id="remarks" cols="" rows="3" class="form-control" placeholder="Remarks"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12"><hr></div>

                        <div class="form-group text-center col-sm-12">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
</script>
@endsection