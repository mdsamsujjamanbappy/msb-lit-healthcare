@extends('layout.master')
@section('title','In Patient Occupancies')
@section('extra_css')
@endsection
@section('content')
    <div class="br-pagetitle my-pagetitle">
    </div>
    <div class="br-pagebody br-pagebody-1">
        <div class="br-section-wrapper">
            <div class="row">
                <div class="col-sm-4">

                    <div class="card bd-0">
                      <div class="card-header tx-medium bd-0 tx-white bg-info">
                        Admission Information
                      </div><!-- card-header -->
                      <div class="card-body bd bd-t-0 rounded-bottom">
                        <ul class="to_do">
                          <li style="color: black;"><h3> Admission ID: <b>{{$details->admission_id}}</b> </h3></li>
                          <li>Admission Date: <b>@if(!empty($details->admission_date)){{date('d-m-Y', strtotime($details->admission_date))}} @endif</b></li>
                          <li>Accommodation: <b>{{$details->category_name}} >> {{$details->accommodation_name_number}}</b></li>
                          <li>Ref. Doctor Name: <b>{{$details->doctor_name}} ({{$details->doctor_phone}})</b></li>
                          <li>Patient ID: <b><a target="_BLANK" href="{{route('patient.details', base64_encode($details->pId))}}" title="">{{$details->patientId}}</a></b></li>
                          <li>Patient Name: <b>{{$details->patient_name}}</b></li>
                          <li>Patient Phone: <b>{{$details->patient_phone}}</b></li>
                          <li>Patient Address: <b>{{$details->patient_address}}</b></li>
                          <li>Emergency Contact: <b>{{$details->emergency_contact_name}}</b></li>
                          <li>Contact's Number: <b>{{$details->emergency_contact_phone}} ({{$details->emergency_contact_relation}})</b></li>
                        </ul>
                      </div><!-- card-body -->
                    </div><!-- card -->

                </div>
                <div class="col-sm-8">

                    <div class="card bd-0">
                        <div class="card-header tx-medium bd-0 tx-white bg-dark">
                            <div class="row">
                                <div class="col-sm-6">
                                    Treatment History
                                </div>
                                <div class="col-sm-6 msb-txt-right">
                                    <a href="" class="btn btn-info btn-sm " data-toggle="modal" data-target="#add_treatment_modal"> <i class="fas fa-plus-circle"></i> Add New</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body bd bd-t-0 rounded-bottom">
                            <table id="datatable" class="table table-striped table-bordered" >
                                <thead>
                                    <tr>
                                        <th class="msb-txt-center">SN</th>
                                        <th class="msb-txt-center">Disease</th>
                                        <th class="msb-txt-center">Next Checkup</th>
                                        <th class="msb-txt-center">Created By</th>
                                        <th class="msb-txt-center">Created At</th>
                                        <th class="msb-txt-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=0; @endphp
                                @foreach($treatment_list as $item)
                                    <tr>
                                        <td class="msb-txt-center">{{++$i}}</td>
                                        <td class="msb-txt-center">{{$item->disease_name}}</td>
                                        <td class="msb-txt-center">@if(!empty($item->next_checkup)){{date('d-m-Y', strtotime($item->next_checkup))}} @endif</td>
                                        <td class="msb-txt-center">{{$item->createdBy}}</td>
                                        <td class="msb-txt-center">@if(!empty($item->created_at)){{date('d-m-Y', strtotime($item->created_at))}} @endif</td>
                                        <td class="msb-txt-center">
                                            <button type="button" value="{{base64_encode($item->id)}}" class="treatment_details btn btn-sm btn-link" title="Treatment Details Information"><i class="fa fa-eye"></i></button>
                                            <a class="btn btn-sm btn-link msb-txt-red" href="{{route('department.destroy', base64_encode($item->id))}}" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12"><hr></div>
                <div class="col-sm-6">

                    <div class="card bd-0">
                        <div class="card-header tx-medium bd-0 tx-white bg-danger">
                            Bill History
                        </div>
                        <div class="card-body bd bd-t-0 rounded-bottom">
                            
                            <table class="myDatatable table table-striped table-bordered" >
                                <thead>
                                    <tr>
                                        <th class="msb-txt-center">SN</th>
                                        <th class="msb-txt-center">Bill Category</th>
                                        <th class="msb-txt-center">Bill Amount</th>
                                        <th class="msb-txt-center">Bill For</th>
                                        <th class="msb-txt-center">Bill Date</th>
                                        <th class="msb-txt-center">Created By</th>
                                        <th class="msb-txt-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=0; @endphp
                                @foreach($bill_list as $item)
                                    <tr>
                                        <td class="msb-txt-center">{{++$i}}</td>
                                        <td class="msb-txt-center">{{$item->bill_category_id}}</td>
                                        <td class="msb-txt-center">{{$item->bill_amount}}</td>
                                        <td class="msb-txt-center">{{$item->bill_title}}</td>
                                        <td class="msb-txt-center">@if(!empty($item->created_at)){{date('d-m-Y', strtotime($item->created_at))}} @endif</td>
                                        <td class="msb-txt-center">{{$item->createdBy}}</td>
                                        <td class="msb-txt-center">
                                            <a class="btn btn-sm btn-link msb-txt-red" href="#" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">

                    <div class="card bd-0">
                        <div class="card-header tx-medium bd-0 tx-white bg-success">
                            Payment History
                        </div>
                        <div class="card-body bd bd-t-0 rounded-bottom">
                            
                            <table class="myDatatable table table-striped table-bordered" >
                                <thead>
                                    <tr>
                                        <th class="msb-txt-center">SN</th>
                                        <th class="msb-txt-center">Payment ID</th>
                                        <th class="msb-txt-center">Payment Amount</th>
                                        <th class="msb-txt-center">Payment Date</th>
                                        <th class="msb-txt-center">Received By</th>
                                        <th class="msb-txt-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=0; @endphp
                                @foreach($payment_list as $item)
                                    <tr>
                                        <td class="msb-txt-center">{{++$i}}</td>
                                        <td class="msb-txt-center">{{$item->payment_id}}</td>
                                        <td class="msb-txt-center">{{$item->payment_amount}}</td>
                                        <td class="msb-txt-center">@if(!empty($item->payment_date)){{date('d-m-Y', strtotime($item->payment_date))}} @endif</td>
                                        <td class="msb-txt-center">{{$item->createdBy}}</td>
                                        <td class="msb-txt-center">
                                            <a class="btn btn-sm btn-link msb-txt-red" href="#" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="add_treatment_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> New Treatment Information</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20 ">
                    <span id="form_result"></span>
                    <div class="modal_body_inner ">
                        {!! Form::open(['method'=>'POST','route'=>'appointment.in_patient.occupancies.treatment.store']) !!}
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form-label" class="control-label form-label-1"> Disease <span class="msb-txt-red">*</span></label>
                                    <input step="text" class="form-control" name="disease_name" autocomplete="off" required="" placeholder="Type Disease">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form-label" class="control-label form-label-1"> Symptoms <span class="msb-txt-red">*</span></label>
                                    <textarea name="symptoms" id="symptoms" required="" cols="" rows="3" class="form-control" placeholder="Type Symptoms"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12" id="dynamic_field">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="form-label" class="control-label form-label-1"> <h5>Medicine</h5> </label>
                                    </div>
                                    <div class="col-sm-6 msb-txt-right">
                                        <button  type="button" onclick="addRow()" class="btn btn-info btn-sm" > <i class="fa fa-plus-circle"></i> Add row</button>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-1 mt-1">
                                        <input step="text" class="form-control  locked_input" id="medicine_name" name="medicine_name[]" autocomplete="off" required="" placeholder="Medicine Name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input step="text" class="form-control locked_input" id="medicine_doses" name="medicine_doses[]" autocomplete="off" required="" placeholder="Doses">
                                    </div>
                                    <div class="col-sm-6">
                                        <input step="text" class="form-control locked_input" id="medicine_times" name="medicine_times[]" autocomplete="off" required="" placeholder="Times">
                                    </div>
                                    <div class="col-sm-12 mt-1">
                                        <input step="text" class="form-control locked_input" id="medicine_taking_instruction" name="medicine_taking_instruction[]" maxlength="28" autocomplete="off" placeholder="Medicine Taking Instructions">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form-label" class="control-label form-label-1"> Remarks </label>
                                    <textarea name="remarks" id="remarks" cols="" rows="3" class="form-control" placeholder="Type Remarks"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="form-label" class="control-label form-label-1"> Next Checkup <span class="msb-txt-red">*</span></label>
                                    <input step="text" class="form-control myDatepicker" name="next_checkup" autocomplete="off" required="" placeholder="Next Checkup Date">
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12"><hr></div>

                        <div class="form-group text-center col-sm-12">
                            <input type="hidden" name="inpatient_occupancies_id" value="{{$details->id}}">
                            <button class="btn btn-info btn-sm custom-btn-1 ml-2" type="submit"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-danger btn-sm custom-btn-1 tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div id="treatment_details_modal" class="modal fade effect-sign">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-15 pd-x-25 modal_header_1">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-plus-circle"></i> Treatment Details Information</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                </div>
                <div class="modal-body pd-20 ">
                    <span id="form_result"></span>
                    <div class="modal_body_inner ">
                        <div id="treatment_details_table">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

@endsection
@section('extra_js')
{{ Html::script('theme/js/bootstrap3-typeahead.js') }}
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });
    
    var url2 = "{{ route('ajax.typehead.medicine_name_list') }}";
    var url3 = "{{ route('ajax.typehead.medicine_taking_instruction_list') }}";
    var count = 1;
    function addRow()
    {
        var medicine_name = $('#medicine_name').val();
        var medicine_doses = $('#medicine_doses').val();
        var medicine_times = $('#medicine_times').val();
        if(medicine_name!='' && medicine_doses!='' && medicine_times!=''){
            count++;
            $('.locked_input').attr('disabled', true);
            $( ".locked_input" ).removeClass( "medicine_name" );

          var tr='<div class="form-group row row_number'+(count)+'"><div class="col-sm-11 mb-1 mt-1"><input step="text" class="form-control medicine_name locked_input" name="medicine_name[]" id="medicine_name'+(count)+'" autocomplete="off" required="" placeholder="Medicine Name"></div><div class="col-sm-1 msb-txt-left"><button type="button" value="row_number'+(count)+'" class="btn-sm btn-danger remove" onclick="deleteRow()"><i class="fa fa-times"></i></button></div><div class="col-sm-6"><input step="text" class="form-control locked_input" name="medicine_doses[]" id="medicine_doses'+(count)+'" autocomplete="off" required="" placeholder="Doses"></div><div class="col-sm-6"><input step="text" class="form-control locked_input" id="medicine_times'+(count)+'" name="medicine_times[]" autocomplete="off" required="" placeholder="Times"></div><div class="col-sm-12 mt-1"><input step="text" class="form-control locked_input" id="medicine_taking_instruction'+(count)+'"  name="medicine_taking_instruction[]" autocomplete="off" placeholder="Medicine Taking Instructions"></div></div>';

            $('#dynamic_field').append(tr);

          $('#medicine_name'+(count)).typeahead({
              source:  function (query, process) {
                return $.get(url2, { query: query }, function (data) {
                    return process(data);
                });
              }
          });

          $('#medicine_taking_instruction'+(count)).typeahead({
              source:  function (query, process) {
                return $.get(url3, { query: query }, function (data) {
                    return process(data);
                });
              }
          });
        }else{
            alert('Please insert medicine name, doses, times first.');
        }
    }

    $(document).on("wheel", "input[type=number]", function (e) {
        $(this).blur();
    });

    function deleteRow(id)
    {
        $(document).on('click', '.remove', function()
        {
            var id = $(this).attr('value');
            $('.'+id).remove();
            return false;
        });
    }
    $('#medicine_name').typeahead({
          source:  function (query, process) {
            return $.get(url2, { query: query }, function (data) {
                  return process(data);
            });
          }
      });

    $('#medicine_taking_instruction').typeahead({
          source:  function (query, process) {
            return $.get(url3, { query: query }, function (data) {
                  return process(data);
            });
          }
      });

    $(document).on('click', '.treatment_details', function(){
        var id = $(this).attr('value');
        $.ajax({
         type: "GET",
         url:"{{url('/appointment/in_patient/occupancies/treatment/details')}}"+"/"+id,
         success:function(response){
             $("#treatment_details_modal").modal('show');
             $('#treatment_details_table').html(response);
         },
            error:function(response){
                console.log(response);
            },
        })
    });
</script>
@endsection