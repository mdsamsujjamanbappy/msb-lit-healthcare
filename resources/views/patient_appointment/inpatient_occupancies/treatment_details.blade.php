
<table class="table table-striped table-bordered" >
    <tr>
        <td width="25%">Disease</td>
        <td>{{$treatment_details->disease_name}}</td>
    </tr>
    <tr>
        <td>Symptoms</td>
        <td>{{$treatment_details->symptoms}}</td>
    </tr>
    <tr>
        <td>Next Checkup</td>
        <td>@if(!empty($treatment_details->next_checkup)){{date('d-m-Y', strtotime($treatment_details->next_checkup))}} @endif</td>
    </tr>
    <tr>
        <td>Remarks</td>
        <td>{{$treatment_details->remarks}}</td>
    </tr>
</table>
<table class="myDatatable table table-striped table-bordered" >
    <thead>
        <tr>
            <th class="msb-txt-center">SN</th>
            <th class="msb-txt-center">Medicine Name</th>
            <th class="msb-txt-center">Doses</th>
            <th class="msb-txt-center">Times</th>
            <th class="msb-txt-center">Instruction</th>
            <th class="msb-txt-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @php $i=0; @endphp
    @foreach($medicine_list as $item)
        <tr>
            <td class="msb-txt-center">{{++$i}}</td>
            <td class="msb-txt-center">{{$item->medicine_name}}</td>
            <td class="msb-txt-center">{{$item->medicine_doses}}</td>
            <td class="msb-txt-center">{{$item->medicine_times}}</td>
            <td class="msb-txt-center">{{$item->medicine_taking_instruction}}</td>
            <td class="msb-txt-center">
                <a class="btn btn-sm btn-link msb-txt-red" href="#" onclick="return confirm('Are you sure to destroy?')" title="Destroy"><i class="fa fa-trash-alt"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>