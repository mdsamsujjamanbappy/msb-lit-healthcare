<?php
Route::get('/login', 'Auth\MSBLoginController@login_view')->name('login.view');
Route::post('/login', 'Auth\MSBLoginController@login')->name('login');
Route::get('/logout', 'Auth\MSBLoginController@logout');
Route::get('/welcome-mail', 'DashboardController@mail');

Route::group(['middleware' => 'auth'], function () {

    Route::resource("/activity","ActivityController");
    Route::resource("/module","ModuleController");
    Route::resource("/role","RoleController");
    Route::resource("/user","UserController");
    
    Route::get('role-access',"RoleAccessController@index")->name('role.access');
    Route::post('roleAclSetup', 'RoleAccessController@roleAclSetup');
    Route::post('roleacl', 'RoleAccessController@save');

    Route::get('user-access', 'UserAccessController@index')->name('user.access');
    Route::post('userAclSetup', 'UserAccessController@userAclSetup');
    Route::post('useracl', 'UserAccessController@save');

    Route::get('/check_permission', 'UserAccessController@check_permission')->name('login.check_permission');

    Route::get('/update_logs', ['uses'=>'DashboardController@update_logs'])->name('update_logs');    
    Route::get('/home', ['uses'=>'DashboardController@dashboard'])->name('home');	
    Route::get('/', ['uses'=>'DashboardController@dashboard'])->name('dashboard'); 
    Route::get('/dashboard', ['uses'=>'DashboardController@dashboard'])->name('dashboard');	

    Route::get('/employee/profile', ['uses'=>'EmployeeListController@my_profile'])->name('employee.my_profile');
    Route::post('/employee/profile/password/update', ['uses'=>'EmployeeListController@my_profile_password_update'])->name('employee.my_profile_password_update');

    // Manage Company
    Route::get('/company/list', ['uses'=>'CompanyInformationController@company_information_list'])->name('company_information.list');   
    Route::post('/company/new/store', ['uses'=>'CompanyInformationController@store'])->name('company_information.new.store');   
    Route::get('/company/edit/{id}', ['uses'=>'CompanyInformationController@edit'])->name('company_information.edit');
    Route::get('/company/details/{id}', ['uses'=>'CompanyInformationController@details'])->name('company_information.details');
    Route::post('/company/update', ['uses'=>'CompanyInformationController@update'])->name('company_information.update');    
    Route::get('/company/destroy/{id}', ['uses'=>'CompanyInformationController@destroy'])->name('company_information.destroy');

    Route::get('/global_settings/company/setup/{id}',['uses'=>'CompanyInformationController@global_settings_company'])->name('company_information.global_settings_company');

    // Manage Department
    Route::get('/department/list', ['uses'=>'DepartmentController@department_list'])->name('department.list');
    Route::post('/department/new/store', ['uses'=>'DepartmentController@store'])->name('department.new.store');
    Route::get('/department/edit/{id}', ['uses'=>'DepartmentController@edit'])->name('department.edit');
    Route::post('/department/update', ['uses'=>'DepartmentController@update'])->name('department.update');
    Route::get('/department/destroy/{id}', ['uses'=>'DepartmentController@destroy'])->name('department.destroy');
    
    // Manage Designation
    Route::get('/designation/list', ['uses'=>'DesignationController@designation_list'])->name('designation.list'); 
    Route::post('/designation/new/store', ['uses'=>'DesignationController@store'])->name('designation.new.store');
    Route::get('/designation/edit/{id}', ['uses'=>'DesignationController@edit'])->name('designation.edit');
    Route::post('/designation/update', ['uses'=>'DesignationController@update'])->name('designation.update');    
    Route::get('/designation/destroy/{id}', ['uses'=>'DesignationController@destroy'])->name('designation.destroy');
     
    // Manage Gender
    Route::get('/gender/list', ['uses'=>'GenderController@gender_list'])->name('gender.list'); 
    Route::post('/gender/new/store', ['uses'=>'GenderController@store'])->name('gender.new.store');
    Route::get('/gender/edit/{id}', ['uses'=>'GenderController@edit'])->name('gender.edit');
    Route::post('/gender/update', ['uses'=>'GenderController@update'])->name('gender.update');    
    Route::get('/gender/destroy/{id}', ['uses'=>'GenderController@destroy'])->name('gender.destroy');

    // Manage Work Shift 
    Route::get('/work_shift/list', ['uses'=>'EmployeeWorkShiftController@work_shift_list'])->name('work_shift.list'); 
    Route::post('/work_shift/new/store', ['uses'=>'EmployeeWorkShiftController@store'])->name('work_shift.new.store');
    Route::get('/work_shift/edit/{id}', ['uses'=>'EmployeeWorkShiftController@edit'])->name('work_shift.edit');
    Route::post('/work_shift/update', ['uses'=>'EmployeeWorkShiftController@update'])->name('work_shift.update');    
    Route::get('/work_shift/weekend/edit/{id}', ['uses'=>'EmployeeWorkShiftController@weekend_edit'])->name('work_shift.weekend.edit');
    Route::post('/work_shift/weekend/update', ['uses'=>'EmployeeWorkShiftController@weekend_update'])->name('work_shift.weekend.update');    
    Route::get('/work_shift/destroy/{id}', ['uses'=>'EmployeeWorkShiftController@destroy'])->name('work_shift.destroy');

    // Manage Weekend Holiday
    Route::get('/weekend_holiday/list', ['uses'=>'WeekendHolidayController@weekend_holiday_list'])->name('weekend_holiday.list'); 

    // Manage Accommodation Category
    Route::get('/accommodation/category/list', ['uses'=>'AccommodationCategoryListController@list'])->name('accommodation.category.list');
    Route::post('/accommodation/category/new/store', ['uses'=>'AccommodationCategoryListController@store'])->name('accommodation.category.store');
    Route::get('/accommodation/category/edit/{id}', ['uses'=>'AccommodationCategoryListController@edit'])->name('accommodation.category.edit');
    Route::post('/accommodation/category/update', ['uses'=>'AccommodationCategoryListController@update'])->name('accommodation.category.update');
    Route::get('/accommodation/category/destroy/{id}', ['uses'=>'AccommodationCategoryListController@destroy'])->name('accommodation.category.destroy');

    // Manage Accommodation List
    Route::get('/accommodation/list', ['uses'=>'AccommodationListController@list'])->name('accommodation.list');
    Route::post('/accommodation/new/store', ['uses'=>'AccommodationListController@store'])->name('accommodation.store');
    Route::get('/accommodation/edit/{id}', ['uses'=>'AccommodationListController@edit'])->name('accommodation.edit');
    Route::post('/accommodation/update', ['uses'=>'AccommodationListController@update'])->name('accommodation.update');
    Route::get('/accommodation/destroy/{id}', ['uses'=>'AccommodationListController@destroy'])->name('accommodation.destroy');


    // Manage Employee
    Route::get('/employee/list', ['uses'=>'EmployeeListController@employee_list'])->name('employee.list');   
    Route::get('/employee/details/{id}', ['uses'=>'EmployeeListController@employee_details'])->name('employee.details');
    Route::post('/employee/update', ['uses'=>'EmployeeListController@update_employee_information'])->name('employee.information.update');    
    Route::get('/employee/new/create', ['uses'=>'EmployeeListController@new_employee'])->name('employee.new.create');   
    Route::POST('/employee/new/store', ['uses'=>'EmployeeListController@store_new_employee'])->name('employee.new.store');   
    Route::post('employee/profile/photo/update', ['uses' => 'EmployeeListController@employee_profile_photo'])->name('employee.profile.photo.update');
    Route::post('/employee/password/update', ['uses'=>'EmployeeListController@employee_password_update'])->name('employee.employee_password_update');
    Route::post('/employee/access_role/create', ['uses'=>'EmployeeListController@employee_access_role_create'])->name('employee.employee_access_role_create');
    Route::post('/employee/access_role/update', ['uses'=>'EmployeeListController@employee_access_role_update'])->name('employee.employee_access_role_update');
 
    Route::post('/employee/nominee/store', ['uses'=>'EmployeeNomineeController@employee_nominee_store'])->name('employee_nominee.store');
    Route::get('/employee/nominee/destroy/{id}', ['uses'=>'EmployeeNomineeController@employee_nominee_destroy'])->name('employee_nominee.destroy');

    Route::post('/employee/education/store', ['uses'=>'EmployeeEducationController@employee_education_store'])->name('employee_education.store');
    Route::get('/employee/education/destroy/{id}', ['uses'=>'EmployeeEducationController@employee_education_destroy'])->name('employee_education.destroy');

    Route::post('/employee/employment/store', ['uses'=>'EmployeeEmploymentController@employee_employment_store'])->name('employee_employment.store');
    Route::get('/employee/employment/destroy/{id}', ['uses'=>'EmployeeEmploymentController@employee_employment_destroy'])->name('employee_employment.destroy');

    Route::post('/employee/additional_info/store',['uses'=>'EmployeeAdditionalInfoController@employee_additional_info_store'])->name('employee_additional_info.store');
    Route::get('/employee/additional_info/destroy/{id}',['uses'=>'EmployeeAdditionalInfoController@employee_additional_info_destroy'])->name('employee_additional_info.destroy');
    
    Route::get('/employee/list/general',['uses'=>'EmployeeListController@employee_list_general'])->name('employee.list.general');   

    //Employee Attendance Management
    Route::get('/employee/attendance/history/today',['uses'=>'EmployeeAttendanceController@todays_attendance'])->name('employee.attendance.todays_attendance');   
    Route::get('/employee/attendance/history/daily_view',['uses'=>'EmployeeAttendanceController@daily_attendance_view'])->name('employee.attendance.daily_attendance_view');   
    Route::post('/employee/attendance/history/daily',['uses'=>'EmployeeAttendanceController@daily_attendance'])->name('employee.attendance.daily_attendance');   
    Route::get('/employee/attendance/import/excel',['uses'=>'EmployeeAttendanceImporterController@import_from_excel'])->name('employee.attendance.import_from_excel');   
    Route::post('/employee/attendance/import/excel/store',['uses'=>'EmployeeAttendanceImporterController@import_from_excel_store'])->name('employee.attendance.import_from_excel_store');   

    Route::get('/employee/attendance/history/date_wise_view',['uses'=>'EmployeeAttendanceController@date_wise_attendance_view'])->name('employee.attendance.date_wise_attendance_view');   
    Route::POST('/employee/attendance/history/date_wise_data',['uses'=>'EmployeeAttendanceController@date_wise_attendance_data'])->name('employee.attendance.date_wise_attendance_data');

    Route::get('/employee/attendance/history/myself',['uses'=>'EmployeeAttendanceController@myself_date_wise_attendance_view'])->name('employee.attendance.myself_date_wise_attendance_view');   
    Route::POST('/employee/attendance/history/myself_data',['uses'=>'EmployeeAttendanceController@myself_date_wise_attendance_data'])->name('employee.attendance.myself_date_wise_attendance_data');

    Route::get('/employee/attendance/give_attendance/app',['uses'=>'EmployeeAttendanceController@give_attendance_by_app'])->name('employee.attendance.give_attendance_by_app');   
    Route::post('/employee/attendance/give_attendance/app/checkin',['uses'=>'EmployeeAttendanceController@give_attendance_by_app_checkin'])->name('employee.attendance.give_attendance_by_app.checkin');   
    Route::post('/employee/attendance/give_attendance/app/checkout',['uses'=>'EmployeeAttendanceController@give_attendance_by_app_checkout'])->name('employee.attendance.give_attendance_by_app.checkout');   
    Route::get('/test',['uses'=>'EmployeeAttendanceImporterController@test'])->name('employee.attendance.test');   

    //Employee Leave Management
    Route::get('/settings/leave/type_list',['uses'=>'EmployeeLeaveManagementController@leave_type_list'])->name('employee.leave.type_list');   
    Route::post('/settings/leave/type_list/store',['uses'=>'EmployeeLeaveManagementController@leave_type_store'])->name('settings.leave.type_list.store');   
    Route::get('/settings/leave/type_list/edit/{id}',['uses'=>'EmployeeLeaveManagementController@leave_type_edit'])->name('settings.leave.type_list.edit');   
    Route::post('/settings/leave/type_list/update',['uses'=>'EmployeeLeaveManagementController@leave_type_update'])->name('settings.leave.type_list.update');   
    Route::get('/settings/leave/type_list/destroy/{id}',['uses'=>'EmployeeLeaveManagementController@leave_type_destroy'])->name('settings.leave.type_list.destroy');   

    //Employee Holiday Management
    Route::get('/settings/leave/holidays_observances/list',['uses'=>'EmployeeLeaveManagementController@holidays_list'])->name('settings.leave.holidays_observances_list');   
    Route::post('/settings/leave/holidays_observances/store',['uses'=>'EmployeeLeaveManagementController@holiday_store'])->name('settings.leave.holidays_observances.store');   
    Route::get('/settings/leave/holidays_observances/edit/{id}',['uses'=>'EmployeeLeaveManagementController@holidays_observances_edit'])->name('settings.leave.holidays_observances.edit');   
    Route::post('/settings/leave/holidays_observances/update',['uses'=>'EmployeeLeaveManagementController@holidays_observances_update'])->name('settings.leave.holidays_observances.update');   
    Route::get('/settings/leave/holidays_observances/destroy/{id}',['uses'=>'EmployeeLeaveManagementController@holidays_observances_destroy'])->name('settings.leave.holidays_observances.destroy');   

    // Assign Leave 
    Route::get('/employee/leave/assign/form',['uses'=>'EmployeeLeaveManagementController@leave_assign_view'])->name('employee.leave.assign.view');   
    Route::get('/ajax/get_employee/company_department/{company_id}/{department_id}',['uses'=>'EmployeeLeaveManagementController@get_ajax_employee_list'])->name('ajax.employee_list.company_department');   
    Route::get('/ajax/assign_leave/get_leave_type/{id}','EmployeeLeaveManagementController@get_ajax_leave_type')->name('ajax.assign_leave.get_leave_type');
    Route::get('/ajax/assign_leave/get_employee_available_leave/{leave_id}/{emp_id}','EmployeeLeaveManagementController@get_ajax_employee_available_leave')->name('ajax.assign_leave.get_employee_available_leave');
    Route::post('/employee/leave/assign/store',['uses'=>'EmployeeLeaveManagementController@leave_assign_store'])->name('employee.leave.assign.store');   
    Route::get('/employee/leave/pending/list',['uses'=>'EmployeeLeaveManagementController@pending_leave_list'])->name('employee.leave.pending.list');   
    Route::get('/employee/leave/pending/approve/{id}',['uses'=>'EmployeeLeaveManagementController@pending_leave_approve'])->name('employee.leave.pending.approve');   
    Route::get('/employee/leave/pending/reject/{id}',['uses'=>'EmployeeLeaveManagementController@pending_leave_reject'])->name('employee.leave.pending.reject');   
    Route::get('/employee/leave/approved/list',['uses'=>'EmployeeLeaveManagementController@approved_leave_list'])->name('employee.leave.approved.list');   
    Route::get('/employee/leave/rejected/list',['uses'=>'EmployeeLeaveManagementController@rejected_leave_list'])->name('employee.leave.rejected.list');  
    Route::get('/employee/leave/balance',['uses'=>'EmployeeLeaveManagementController@leave_balance'])->name('employee.leave.balance');   

    // Employee Leave Request 
    Route::get('/employee/leave/new_request',['uses'=>'EmployeeLeaveManagementController@employee_new_leave_request'])->name('employee.leave.new_request');

    // Account Management
    Route::get('/company_information/company_selector_view',['uses'=>'CompanyInformationController@company_selector_view'])->name('company_information.selector_view');
    Route::get('/company/accounts/currency_info/setup',['uses'=>'CompanyAccountCurrencyController@currency_setup'])->name('accounts.currency_info.setup');
    Route::post('/company/accounts/currency_info/setup/update',['uses'=>'CompanyAccountCurrencyController@currency_setup_update'])->name('accounts.currency_info.setup.update');

    // Doctor Management
    Route::get('/doctor/list', ['uses'=>'DoctorListController@doctor_list'])->name('doctor.list');   
    Route::get('/doctor/details/{id}', ['uses'=>'DoctorListController@details'])->name('doctor.details');
    Route::get('/doctor/edit/{id}', ['uses'=>'DoctorListController@edit'])->name('doctor.edit');
    Route::post('/doctor/update', ['uses'=>'DoctorListController@update'])->name('doctor.update');    
    Route::get('/doctor/new/create', ['uses'=>'DoctorListController@create'])->name('doctor.new.create');
    Route::POST('/doctor/new/store', ['uses'=>'DoctorListController@store'])->name('doctor.new.store');   

    // Doctor Shift Management
    Route::get('/doctor/shift/list', ['uses'=>'DoctorShiftListController@index'])->name('doctor.shift.list');
    Route::get('/doctor/shift/add', ['uses'=>'DoctorShiftListController@create'])->name('doctor.shift.create');
    Route::get('/doctor/shift/check/{id}', ['uses'=>'DoctorShiftListController@CheckDoctorShift'])->name('doctor.shift.create.check');
    Route::post('/doctor/shift/store', ['uses'=>'DoctorShiftListController@ShiftStore'])->name('doctor.shift.store');
    Route::get('/doctor/shift/show/{id}', ['uses'=>'DoctorShiftListController@ShiftShow'])->name('doctor.shift.view');
    Route::get('/doctor/shift/delete/{id}', ['uses'=>'DoctorShiftListController@ShiftDelete'])->name('doctor.shift.delete');

    // Blood Bank Management
    Route::get('/blood_bank/donor/list', ['uses'=>'BloodBankController@list'])->name('blood_bank.donor.list');   
    Route::get('/blood_bank/donor/details/{id}', ['uses'=>'BloodBankController@details'])->name('blood_bank.donor.details');
    Route::get('/blood_bank/donor/edit/{id}', ['uses'=>'BloodBankController@edit'])->name('blood_bank.donor.edit');
    Route::post('/blood_bank/donor/update', ['uses'=>'BloodBankController@update'])->name('blood_bank.donor.update');    
    Route::get('/blood_bank/donor/create', ['uses'=>'BloodBankController@create'])->name('blood_bank.donor.create');
    Route::POST('/blood_bank/donor/store', ['uses'=>'BloodBankController@store'])->name('blood_bank.donor.store');   
    Route::get('/blood_bank/donor/destroy/{id}', ['uses'=>'BloodBankController@destroy'])->name('blood_bank.donor.destroy');
    Route::get('/blood_bank/blood_group/summary_list', ['uses'=>'BloodBankController@blood_group_summary_list'])->name('blood_bank.blood_group.summary_list');

    // Pathology 
    // Manage Test Category
    Route::get('/pathology/test_category/list', ['uses'=>'PathologyTestCategoryController@list'])->name('pathology.test_category.list');
    Route::post('/pathology/test_category/new/store', ['uses'=>'PathologyTestCategoryController@store'])->name('pathology.test_category.store');
    Route::get('/pathology/test_category/edit/{id}', ['uses'=>'PathologyTestCategoryController@edit'])->name('pathology.test_category.edit');
    Route::post('/pathology/test_category/update', ['uses'=>'PathologyTestCategoryController@update'])->name('pathology.test_category.update');
    Route::get('/pathology/test_category/destroy/{id}', ['uses'=>'PathologyTestCategoryController@destroy'])->name('pathology.test_category.destroy');

    // Manage Test List
    Route::get('/pathology/test/list', ['uses'=>'PathologyTestListController@list'])->name('pathology.test.list');
    Route::post('/pathology/test/new/store', ['uses'=>'PathologyTestListController@store'])->name('pathology.test.store');
    Route::get('/pathology/test/edit/{id}', ['uses'=>'PathologyTestListController@edit'])->name('pathology.test.edit');
    Route::post('/pathology/test/update', ['uses'=>'PathologyTestListController@update'])->name('pathology.test.update');
    Route::get('/pathology/test/destroy/{id}', ['uses'=>'PathologyTestListController@destroy'])->name('pathology.test.destroy');

    // Manage Patient List
    Route::get('/patient/list', ['uses'=>'PatientListController@list'])->name('patient.list');
    Route::get('/patient/new/create', ['uses'=>'PatientListController@create'])->name('patient.new.create');
    Route::get('/patient/details/{id}', ['uses'=>'PatientListController@details'])->name('patient.details');
    Route::post('/patient/new/store', ['uses'=>'PatientListController@store'])->name('patient.store');
    Route::get('/patient/edit/{id}', ['uses'=>'PatientListController@edit'])->name('patient.edit');
    Route::post('/patient/update', ['uses'=>'PatientListController@update'])->name('patient.update');
    Route::get('/patient/destroy/{id}', ['uses'=>'PatientListController@destroy'])->name('patient.destroy');

    //Get Ajax Typehead Medicine Name
    Route::get('/ajax/typehead/medicine_name_list',['uses'=>'InPatientOccupanciesController@medicine_name_list'])->name('ajax.typehead.medicine_name_list');
    Route::get('/ajax/typehead/medicine_taking_instruction_list',['uses'=>'InPatientOccupanciesController@medicine_taking_instruction_list'])->name('ajax.typehead.medicine_taking_instruction_list');

    // Manage in patient occupancies
    Route::get('/appointment/in_patient/occupancies/list', ['uses'=>'InPatientOccupanciesController@list'])->name('appointment.in_patient.occupancies.list');
    Route::post('/appointment/in_patient/occupancies/new/store', ['uses'=>'InPatientOccupanciesController@store'])->name('appointment.in_patient.occupancies.store');
    Route::get('/appointment/in_patient/occupancies/details/{id}', ['uses'=>'InPatientOccupanciesController@details'])->name('appointment.in_patient.occupancies.details');
    Route::get('/appointment/in_patient/occupancies/edit/{id}', ['uses'=>'InPatientOccupanciesController@edit'])->name('appointment.in_patient.occupancies.edit');
    Route::post('/appointment/in_patient/occupancies/update', ['uses'=>'InPatientOccupanciesController@update'])->name('appointment.in_patient.occupancies.update');
    Route::get('/appointment/in_patient/occupancies/destroy/{id}', ['uses'=>'InPatientOccupanciesController@destroy'])->name('appointment.in_patient.occupancies.destroy');

    Route::post('/appointment/in_patient/occupancies/treatment/store', ['uses'=>'InPatientOccupanciesController@treatment_store'])->name('appointment.in_patient.occupancies.treatment.store');
    Route::get('/appointment/in_patient/occupancies/treatment/details/{id}', ['uses'=>'InPatientOccupanciesController@treatment_details'])->name('appointment.in_patient.occupancies.treatment.details');

});

