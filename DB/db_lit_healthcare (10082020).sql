-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2020 at 06:07 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lit_healthcare`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `name`, `description`, `created_by`, `updated_by`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'List', NULL, 1, 2, 1, NULL, '2020-01-24 00:52:00', '2020-07-07 05:03:52'),
(2, 'Create', NULL, 1, 1, 1, NULL, '2020-01-24 00:52:10', '2020-01-24 00:52:10'),
(3, 'Edit', NULL, 1, 1, 1, NULL, '2020-01-24 00:52:20', '2020-03-02 03:23:32'),
(4, 'Delete', NULL, 1, 1, 1, NULL, '2020-01-24 00:52:30', '2020-01-24 00:52:30'),
(5, 'Details', NULL, 1, 1, 1, NULL, '2020-01-24 00:52:39', '2020-01-24 00:52:39'),
(6, 'Search', NULL, 1, 1, 1, NULL, '2020-01-24 00:52:50', '2020-01-24 00:52:50'),
(7, 'Print/Download', NULL, 1, 1, 1, NULL, '2020-01-24 00:53:01', '2020-01-24 00:53:01'),
(8, 'View (Report)', NULL, 1, 1, 1, NULL, '2020-01-24 00:53:01', '2020-07-09 04:14:10'),
(9, 'Approval', NULL, 1, 1, 1, NULL, '2020-01-24 00:53:01', '2020-01-24 00:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_09_034113_create_company_information_table', 1),
(5, '2020_01_17_040304_create_activities_table', 1),
(6, '2020_01_17_040924_create_modules_table', 1),
(7, '2020_01_17_041254_create_module_to_activities_table', 1),
(8, '2020_01_17_041749_create_module_to_roles_table', 1),
(9, '2020_01_17_041946_create_module_to_users_table', 1),
(10, '2020_01_17_042141_create_roles_table', 1),
(11, '2020_01_29_035326_create_employee_list_table', 1),
(12, '2020_01_29_040341_create_department_list_table', 1),
(13, '2020_01_29_040400_create_designation_list_table', 1),
(14, '2020_01_29_040427_create_gender_list_table', 1),
(15, '2020_01_29_040500_create_work_shift_list_table', 1),
(16, '2020_01_29_040705_create_employee_education_info_table', 1),
(17, '2020_01_29_040818_create_employee_others_info_table', 1),
(18, '2020_01_29_040833_create_employee_work_history_table', 1),
(19, '2020_01_29_040932_create_employee_nominee_table', 1),
(20, '2020_02_13_100716_create_attendance_history_table', 1),
(21, '2020_02_13_111257_create_weekend_holiday_table', 1),
(22, '2020_02_29_053017_create_employee_leave_application_table', 1),
(23, '2020_02_29_091207_create_attendance_history_tmp_table', 1),
(24, '2020_03_02_125625_create_app_attendance_history_table', 1),
(25, '2020_03_03_153047_create_employee_shift_weekend_table', 1),
(26, '2020_03_03_175107_create_employee_leave_type_setting_table', 1),
(27, '2020_03_07_124155_create_holidays_observances_leave_table', 1),
(28, '2020_03_11_170541_create_jobs_table', 1),
(31, '2020_08_02_155302_create_doctor_list_table', 2),
(32, '2020_08_02_212118_create_doctor_shift_list_table', 3),
(35, '2020_08_04_102228_create_blood_donor_list_table', 4),
(36, '2020_08_05_101657_create_test_category_list_table', 5),
(37, '2020_08_05_101710_create_test_list_table', 5),
(38, '2020_06_29_133303_create_company_account_currency_table', 6),
(42, '2020_08_05_214850_create_patient_list_table', 7),
(43, '2020_08_08_155055_create_accommodation_category_list_table', 8),
(45, '2020_08_08_155107_create_accommodation_list_table', 9),
(46, '2020_08_10_211508_create_inpatient_occupancies_list_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `status`, `deleted_at`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'ACCESS CONTROL >> Role Management', NULL, 1, NULL, 1, 1, '2020-01-24 00:53:44', '2020-01-24 00:53:44'),
(2, 'ACCESS CONTROL >> Role Access Control', NULL, 1, NULL, 1, 1, '2020-01-24 00:54:32', '2020-01-24 00:54:32'),
(3, 'ACCESS CONTROL >> User Access Control', NULL, 1, NULL, 1, 1, '2020-01-24 00:54:59', '2020-01-24 00:54:59'),
(4, 'ACCESS CONTROL >> Module Management', NULL, 1, NULL, 1, 1, '2020-01-24 00:55:37', '2020-01-24 00:55:37'),
(5, 'ACCESS CONTROL >> Activity Management', NULL, 1, NULL, 1, 1, '2020-01-24 00:55:58', '2020-01-24 00:55:58'),
(6, 'HR & ADMIN >> Employee Management', NULL, 1, NULL, 2, 2, '2020-07-08 05:45:47', '2020-07-08 05:45:47'),
(7, 'HR & ADMIN >> Attendance Management', NULL, 1, NULL, 2, 2, '2020-07-08 05:46:57', '2020-07-08 05:46:57'),
(8, 'HR & ADMIN >> Leave Management', NULL, 1, NULL, 2, 2, '2020-07-08 05:47:30', '2020-07-08 05:47:30'),
(9, 'HR & ADMIN >> Master Settings', NULL, 1, NULL, 2, 2, '2020-07-08 05:49:24', '2020-07-08 05:49:24'),
(10, 'HR & ADMIN >> Master Settings >> Company Management', NULL, 1, NULL, 2, 2, '2020-07-09 04:28:04', '2020-07-09 04:28:04'),
(11, 'Employee Panel General', NULL, 1, NULL, 2, 2, '2020-07-09 04:37:33', '2020-07-09 04:37:33'),
(12, 'SOFTWARE RELEASE LOGS', NULL, 1, NULL, 2, 2, '2020-07-09 04:40:29', '2020-07-09 04:40:29'),
(13, 'HR & ADMIN >> Leave Settings + Holidays', NULL, 1, NULL, 2, 2, '2020-07-14 03:02:09', '2020-07-14 03:02:09'),
(14, '*** GROUP COMPANY ACCESS ***', NULL, 1, NULL, 2, 2, '2020-07-14 03:17:33', '2020-07-14 03:17:33'),
(15, 'HR & ADMIN >> Employee Management >> Assign Role', NULL, 1, NULL, 2, 2, '2020-07-14 05:53:41', '2020-07-14 05:53:41'),
(16, 'HR & ADMIN >> Doctor Management', NULL, 1, NULL, 1, 1, '2020-08-02 09:40:56', '2020-08-02 09:40:56'),
(17, 'Blood Bank Management', NULL, 1, NULL, 1, 1, '2020-08-04 04:05:27', '2020-08-04 04:05:27'),
(18, 'Pathology => Test Management', NULL, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(19, 'Patient Management', NULL, 1, NULL, 1, 1, '2020-08-05 08:46:21', '2020-08-05 08:46:21'),
(20, 'In-Patient Department', NULL, 1, NULL, 1, 1, '2020-08-08 04:41:13', '2020-08-08 04:41:13'),
(21, 'Out-Patient Department', NULL, 1, NULL, 1, 1, '2020-08-08 04:41:23', '2020-08-08 04:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `module_to_activities`
--

CREATE TABLE `module_to_activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_to_activities`
--

INSERT INTO `module_to_activities` (`id`, `module_id`, `activity_id`, `status`, `deleted_at`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 2, 2, '2020-07-14 23:17:30', '2020-07-14 23:17:30'),
(2, 1, 2, 1, NULL, 2, 2, '2020-07-14 23:17:30', '2020-07-14 23:17:30'),
(3, 1, 3, 1, NULL, 2, 2, '2020-07-14 23:17:30', '2020-07-14 23:17:30'),
(4, 1, 4, 1, NULL, 2, 2, '2020-07-14 23:17:30', '2020-07-14 23:17:30'),
(5, 2, 2, 1, NULL, 2, 2, '2020-07-07 23:52:12', '2020-07-07 23:52:12'),
(6, 2, 4, 1, NULL, 2, 2, '2020-07-07 23:52:12', '2020-07-07 23:52:12'),
(7, 2, 8, 1, NULL, 2, 2, '2020-07-07 23:52:12', '2020-07-07 23:52:12'),
(8, 2, 3, 1, NULL, 2, 2, '2020-07-07 23:52:12', '2020-07-07 23:52:12'),
(9, 2, 1, 1, NULL, 2, 2, '2020-07-07 23:52:12', '2020-07-07 23:52:12'),
(10, 3, 2, 1, NULL, 1, 1, '2020-01-24 00:54:59', '2020-01-24 00:54:59'),
(11, 3, 4, 1, NULL, 1, 1, '2020-01-24 00:54:59', '2020-01-24 00:54:59'),
(12, 3, 3, 1, NULL, 1, 1, '2020-01-24 00:54:59', '2020-01-24 00:54:59'),
(13, 3, 1, 1, NULL, 1, 1, '2020-01-24 00:54:59', '2020-01-24 00:54:59'),
(14, 4, 2, 1, NULL, 1, 1, '2020-01-24 00:55:37', '2020-01-24 00:55:37'),
(15, 4, 4, 1, NULL, 1, 1, '2020-01-24 00:55:37', '2020-01-24 00:55:37'),
(16, 4, 3, 1, NULL, 1, 1, '2020-01-24 00:55:37', '2020-01-24 00:55:37'),
(17, 4, 1, 1, NULL, 1, 1, '2020-01-24 00:55:37', '2020-01-24 00:55:37'),
(18, 5, 2, 1, NULL, 1, 1, '2020-01-24 00:55:58', '2020-01-24 00:55:58'),
(19, 5, 4, 1, NULL, 1, 1, '2020-01-24 00:55:58', '2020-01-24 00:55:58'),
(20, 5, 3, 1, NULL, 1, 1, '2020-01-24 00:55:58', '2020-01-24 00:55:58'),
(21, 5, 1, 1, NULL, 1, 1, '2020-01-24 00:55:58', '2020-01-24 00:55:58'),
(22, 14, 1, 1, NULL, 1, 1, '2020-07-28 16:42:42', '2020-07-28 16:42:42'),
(23, 6, 1, 1, NULL, 1, 1, '2020-07-28 16:45:41', '2020-07-28 16:45:41'),
(24, 6, 2, 1, NULL, 1, 1, '2020-07-28 16:45:41', '2020-07-28 16:45:41'),
(25, 6, 3, 1, NULL, 1, 1, '2020-07-28 16:45:41', '2020-07-28 16:45:41'),
(26, 6, 4, 1, NULL, 1, 1, '2020-07-28 16:45:41', '2020-07-28 16:45:41'),
(27, 6, 5, 1, NULL, 1, 1, '2020-07-28 16:45:41', '2020-07-28 16:45:41'),
(28, 6, 7, 1, NULL, 1, 1, '2020-07-28 16:45:41', '2020-07-28 16:45:41'),
(29, 7, 1, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(30, 7, 2, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(31, 7, 3, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(32, 7, 4, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(33, 7, 5, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(34, 7, 7, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(35, 7, 8, 1, NULL, 1, 1, '2020-07-28 16:46:14', '2020-07-28 16:46:14'),
(36, 8, 1, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(37, 8, 2, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(38, 8, 3, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(39, 8, 4, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(40, 8, 5, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(41, 8, 7, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(42, 8, 8, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(43, 8, 9, 1, NULL, 1, 1, '2020-07-28 16:46:41', '2020-07-28 16:46:41'),
(44, 9, 1, 1, NULL, 1, 1, '2020-07-28 16:47:19', '2020-07-28 16:47:19'),
(45, 9, 2, 1, NULL, 1, 1, '2020-07-28 16:47:19', '2020-07-28 16:47:19'),
(46, 9, 3, 1, NULL, 1, 1, '2020-07-28 16:47:19', '2020-07-28 16:47:19'),
(47, 9, 4, 1, NULL, 1, 1, '2020-07-28 16:47:19', '2020-07-28 16:47:19'),
(48, 9, 5, 1, NULL, 1, 1, '2020-07-28 16:47:19', '2020-07-28 16:47:19'),
(49, 9, 7, 1, NULL, 1, 1, '2020-07-28 16:47:19', '2020-07-28 16:47:19'),
(50, 15, 1, 1, NULL, 1, 1, '2020-07-28 16:48:00', '2020-07-28 16:48:00'),
(51, 15, 2, 1, NULL, 1, 1, '2020-07-28 16:48:00', '2020-07-28 16:48:00'),
(52, 15, 3, 1, NULL, 1, 1, '2020-07-28 16:48:00', '2020-07-28 16:48:00'),
(53, 13, 1, 1, NULL, 1, 1, '2020-07-28 16:48:29', '2020-07-28 16:48:29'),
(54, 13, 2, 1, NULL, 1, 1, '2020-07-28 16:48:29', '2020-07-28 16:48:29'),
(55, 13, 3, 1, NULL, 1, 1, '2020-07-28 16:48:29', '2020-07-28 16:48:29'),
(56, 13, 4, 1, NULL, 1, 1, '2020-07-28 16:48:29', '2020-07-28 16:48:29'),
(57, 10, 1, 1, NULL, 1, 1, '2020-07-28 16:49:07', '2020-07-28 16:49:07'),
(58, 10, 2, 1, NULL, 1, 1, '2020-07-28 16:49:07', '2020-07-28 16:49:07'),
(59, 10, 3, 1, NULL, 1, 1, '2020-07-28 16:49:07', '2020-07-28 16:49:07'),
(60, 10, 4, 1, NULL, 1, 1, '2020-07-28 16:49:07', '2020-07-28 16:49:07'),
(61, 12, 1, 1, NULL, 1, 1, '2020-07-28 16:49:25', '2020-07-28 16:49:25'),
(62, 11, 1, 1, NULL, 1, 1, '2020-07-28 16:55:51', '2020-07-28 16:55:51'),
(63, 11, 2, 1, NULL, 1, 1, '2020-07-28 16:55:51', '2020-07-28 16:55:51'),
(64, 11, 7, 1, NULL, 1, 1, '2020-07-28 16:55:51', '2020-07-28 16:55:51'),
(65, 11, 8, 1, NULL, 1, 1, '2020-07-28 16:55:51', '2020-07-28 16:55:51'),
(71, 16, 1, 1, NULL, 1, 1, '2020-08-02 15:07:29', '2020-08-02 15:07:29'),
(72, 16, 2, 1, NULL, 1, 1, '2020-08-02 15:07:29', '2020-08-02 15:07:29'),
(73, 16, 3, 1, NULL, 1, 1, '2020-08-02 15:07:29', '2020-08-02 15:07:29'),
(74, 16, 4, 1, NULL, 1, 1, '2020-08-02 15:07:29', '2020-08-02 15:07:29'),
(75, 16, 5, 1, NULL, 1, 1, '2020-08-02 15:07:29', '2020-08-02 15:07:29'),
(76, 16, 8, 1, NULL, 1, 1, '2020-08-02 15:07:29', '2020-08-02 15:07:29'),
(77, 17, 1, 1, NULL, 1, 1, '2020-08-04 04:05:27', '2020-08-04 04:05:27'),
(78, 17, 2, 1, NULL, 1, 1, '2020-08-04 04:05:27', '2020-08-04 04:05:27'),
(79, 17, 3, 1, NULL, 1, 1, '2020-08-04 04:05:27', '2020-08-04 04:05:27'),
(80, 17, 4, 1, NULL, 1, 1, '2020-08-04 04:05:27', '2020-08-04 04:05:27'),
(81, 17, 5, 1, NULL, 1, 1, '2020-08-04 04:05:27', '2020-08-04 04:05:27'),
(82, 18, 1, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(83, 18, 2, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(84, 18, 3, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(85, 18, 4, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(86, 18, 5, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(87, 18, 8, 1, NULL, 1, 1, '2020-08-05 04:23:11', '2020-08-05 04:23:11'),
(88, 19, 1, 1, NULL, 1, 1, '2020-08-05 08:46:21', '2020-08-05 08:46:21'),
(89, 19, 2, 1, NULL, 1, 1, '2020-08-05 08:46:21', '2020-08-05 08:46:21'),
(90, 19, 3, 1, NULL, 1, 1, '2020-08-05 08:46:21', '2020-08-05 08:46:21'),
(91, 19, 4, 1, NULL, 1, 1, '2020-08-05 08:46:21', '2020-08-05 08:46:21'),
(92, 19, 5, 1, NULL, 1, 1, '2020-08-05 08:46:21', '2020-08-05 08:46:21'),
(93, 20, 1, 1, NULL, 1, 1, '2020-08-08 04:41:13', '2020-08-08 04:41:13'),
(94, 20, 2, 1, NULL, 1, 1, '2020-08-08 04:41:13', '2020-08-08 04:41:13'),
(95, 20, 3, 1, NULL, 1, 1, '2020-08-08 04:41:13', '2020-08-08 04:41:13'),
(96, 20, 4, 1, NULL, 1, 1, '2020-08-08 04:41:13', '2020-08-08 04:41:13'),
(97, 21, 1, 1, NULL, 1, 1, '2020-08-08 04:41:23', '2020-08-08 04:41:23'),
(98, 21, 2, 1, NULL, 1, 1, '2020-08-08 04:41:23', '2020-08-08 04:41:23'),
(99, 21, 3, 1, NULL, 1, 1, '2020-08-08 04:41:23', '2020-08-08 04:41:23'),
(100, 21, 4, 1, NULL, 1, 1, '2020-08-08 04:41:23', '2020-08-08 04:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `module_to_roles`
--

CREATE TABLE `module_to_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_to_roles`
--

INSERT INTO `module_to_roles` (`id`, `module_id`, `role_id`, `activity_id`, `status`, `deleted_at`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(3, 1, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(4, 1, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(5, 2, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(6, 2, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(7, 2, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(8, 2, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(9, 2, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(10, 3, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(11, 3, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(12, 3, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(13, 3, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(14, 4, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(15, 4, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(16, 4, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(17, 4, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(18, 5, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(19, 5, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(20, 5, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(21, 5, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(22, 6, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(23, 6, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(24, 6, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(25, 6, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(26, 6, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(27, 6, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(28, 7, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(29, 7, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(30, 7, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(31, 7, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(32, 7, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(33, 7, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(34, 7, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(35, 8, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(36, 8, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(37, 8, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(38, 8, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(39, 8, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(40, 8, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(41, 8, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(42, 8, 1, 9, 1, NULL, NULL, NULL, NULL, NULL),
(43, 9, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(44, 9, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(45, 9, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(46, 9, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(47, 9, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(48, 9, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(49, 10, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(50, 10, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(51, 10, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(52, 10, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(53, 12, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(54, 13, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(55, 13, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(56, 13, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(57, 13, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(58, 14, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(59, 15, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(60, 15, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(61, 15, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(62, 1, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(63, 1, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(64, 1, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(65, 1, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(66, 2, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(67, 2, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(68, 2, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(69, 2, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(70, 2, 2, 8, 1, NULL, NULL, NULL, NULL, NULL),
(71, 3, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(72, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(73, 3, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(74, 3, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(75, 4, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(76, 4, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(77, 4, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(78, 4, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(79, 5, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(80, 5, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(81, 5, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(82, 5, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(83, 6, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(84, 6, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(85, 6, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(86, 6, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(87, 6, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(88, 6, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(89, 7, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(90, 7, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(91, 7, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(92, 7, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(93, 7, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(94, 7, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(95, 7, 2, 8, 1, NULL, NULL, NULL, NULL, NULL),
(96, 8, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(97, 8, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(98, 8, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(99, 8, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(100, 8, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(101, 8, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(102, 8, 2, 8, 1, NULL, NULL, NULL, NULL, NULL),
(103, 8, 2, 9, 1, NULL, NULL, NULL, NULL, NULL),
(104, 9, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(105, 9, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(106, 9, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(107, 9, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(108, 9, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(109, 9, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(110, 10, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(111, 10, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(112, 10, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(113, 10, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(114, 12, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(115, 13, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(116, 13, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(117, 13, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(118, 13, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(119, 15, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(120, 15, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(121, 15, 2, 3, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_to_users`
--

CREATE TABLE `module_to_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_to_users`
--

INSERT INTO `module_to_users` (`id`, `module_id`, `user_id`, `activity_id`, `status`, `deleted_at`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(188, 1, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(189, 1, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(190, 1, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(191, 1, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(192, 2, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(193, 2, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(194, 2, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(195, 2, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(196, 2, 2, 8, 1, NULL, NULL, NULL, NULL, NULL),
(197, 3, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(198, 3, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(199, 3, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(200, 3, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(201, 4, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(202, 4, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(203, 4, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(204, 4, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(205, 5, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(206, 5, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(207, 5, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(208, 5, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(209, 6, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(210, 6, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(211, 6, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(212, 6, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(213, 6, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(214, 6, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(215, 7, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(216, 7, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(217, 7, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(218, 7, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(219, 7, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(220, 7, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(221, 7, 2, 8, 1, NULL, NULL, NULL, NULL, NULL),
(222, 8, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(223, 8, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(224, 8, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(225, 8, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(226, 8, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(227, 8, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(228, 8, 2, 8, 1, NULL, NULL, NULL, NULL, NULL),
(229, 8, 2, 9, 1, NULL, NULL, NULL, NULL, NULL),
(230, 9, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(231, 9, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(232, 9, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(233, 9, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(234, 9, 2, 5, 1, NULL, NULL, NULL, NULL, NULL),
(235, 9, 2, 7, 1, NULL, NULL, NULL, NULL, NULL),
(236, 10, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(237, 10, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(238, 10, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(239, 10, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(240, 12, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(241, 13, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(242, 13, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(243, 13, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(244, 13, 2, 4, 1, NULL, NULL, NULL, NULL, NULL),
(245, 15, 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(246, 15, 2, 2, 1, NULL, NULL, NULL, NULL, NULL),
(247, 15, 2, 3, 1, NULL, NULL, NULL, NULL, NULL),
(699, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(700, 1, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(701, 1, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(702, 1, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(703, 2, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(704, 2, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(705, 2, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(706, 2, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(707, 2, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(708, 3, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(709, 3, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(710, 3, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(711, 3, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(712, 4, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(713, 4, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(714, 4, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(715, 4, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(716, 5, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(717, 5, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(718, 5, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(719, 5, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(720, 6, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(721, 6, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(722, 6, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(723, 6, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(724, 6, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(725, 6, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(726, 7, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(727, 7, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(728, 7, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(729, 7, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(730, 7, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(731, 7, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(732, 7, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(733, 8, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(734, 8, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(735, 8, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(736, 8, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(737, 8, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(738, 8, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(739, 8, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(740, 8, 1, 9, 1, NULL, NULL, NULL, NULL, NULL),
(741, 9, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(742, 9, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(743, 9, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(744, 9, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(745, 9, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(746, 9, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(747, 10, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(748, 10, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(749, 10, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(750, 10, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(751, 11, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(752, 11, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(753, 11, 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(754, 11, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(755, 12, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(756, 13, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(757, 13, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(758, 13, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(759, 13, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(760, 14, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(761, 15, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(762, 15, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(763, 15, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(764, 16, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(765, 16, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(766, 16, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(767, 16, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(768, 16, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(769, 16, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(770, 17, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(771, 17, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(772, 17, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(773, 17, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(774, 17, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(775, 18, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(776, 18, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(777, 18, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(778, 18, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(779, 18, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(780, 18, 1, 8, 1, NULL, NULL, NULL, NULL, NULL),
(781, 19, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(782, 19, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(783, 19, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(784, 19, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(785, 19, 1, 5, 1, NULL, NULL, NULL, NULL, NULL),
(786, 20, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(787, 20, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(788, 20, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(789, 20, 1, 4, 1, NULL, NULL, NULL, NULL, NULL),
(790, 21, 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(791, 21, 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(792, 21, 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(793, 21, 1, 4, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `info`, `status`, `deleted_at`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', NULL, 1, NULL, 1, 1, '2020-07-07 00:13:22', '2020-07-07 04:30:45'),
(2, 'Admin', NULL, 1, NULL, 1, 1, '2020-07-07 00:32:03', '2020-07-28 16:50:18'),
(3, 'IT Manager', NULL, 1, NULL, 1, 1, NULL, NULL),
(4, 'Employee', NULL, 1, NULL, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_accommodation_category_list`
--

CREATE TABLE `tb_accommodation_category_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_accommodation_category_list`
--

INSERT INTO `tb_accommodation_category_list` (`id`, `category_name`, `remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Single Room', 'N/A', 1, 1, 1, '2020-08-08 10:08:18', '2020-08-08 10:08:18'),
(2, 'Male Ward 01', 'Male Ward', 1, 1, 1, '2020-08-08 10:08:43', '2020-08-08 10:09:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_accommodation_list`
--

CREATE TABLE `tb_accommodation_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `accommodation_name_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accommodation_category_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rent_amount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_accommodation_list`
--

INSERT INTO `tb_accommodation_list` (`id`, `accommodation_name_number`, `accommodation_category_id`, `rent_amount`, `remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'ROOM 101', '1', '750', NULL, 1, 1, 1, '2020-08-08 10:27:40', '2020-08-08 10:27:40'),
(2, 'WORD 201-A', '2', '250', NULL, 1, 1, 1, '2020-08-08 10:28:09', '2020-08-08 10:28:59'),
(3, 'WORD 201-B', '2', '250', NULL, 1, 1, 1, '2020-08-08 10:29:11', '2020-08-08 10:29:11');

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_attendance_history`
--

CREATE TABLE `tb_app_attendance_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attendance_ref_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `check_in` time DEFAULT NULL,
  `check_out` time DEFAULT NULL,
  `check_in_lat` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_long` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_lat` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_long` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_in_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out_ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance_history`
--

CREATE TABLE `tb_attendance_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `in_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_time` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_type` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_attendance_history_tmp`
--

CREATE TABLE `tb_attendance_history_tmp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `punch_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_blood_donor_list`
--

CREATE TABLE `tb_blood_donor_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donor_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_gender_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_age` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_blood_group` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `donor_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_donation_date` date DEFAULT NULL,
  `donor_remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_blood_donor_list`
--

INSERT INTO `tb_blood_donor_list` (`id`, `donor_name`, `donor_gender_id`, `donor_age`, `donor_phone`, `donor_blood_group`, `donor_address`, `last_donation_date`, `donor_remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'Bappy', '1', '27', '01515268864', 'B+', 'Gazipur', '2019-12-03', 'N/A', 1, 1, 1, '2020-08-05 03:26:53', '2020-08-05 03:50:48'),
(3, 'Bappy', '1', '27', '01515268864', 'O+', 'Gazipur', '2019-12-03', 'N/A', 1, 1, 1, '2020-08-05 03:26:53', '2020-08-05 03:50:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_company_account_currency`
--

CREATE TABLE `tb_company_account_currency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suffix` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decimal_point_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_company_account_currency`
--

INSERT INTO `tb_company_account_currency` (`id`, `company_id`, `currency_code`, `prefix`, `suffix`, `decimal_point_number`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Taka', '৳', NULL, '1', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_company_information`
--

CREATE TABLE `tb_company_information` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_tagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '1 for Active, 0 for Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_company_information`
--

INSERT INTO `tb_company_information` (`id`, `company_logo`, `company_name`, `company_tagline`, `company_phone`, `company_email`, `company_address1`, `company_address2`, `status`, `created_at`, `updated_at`) VALUES
(1, 'default.png', 'LIT Healthcare', NULL, '55087469', 'info@lithealthcare.com', 'Sonargaon Janapath, Uttara 11, Dhaka 1230', NULL, 1, '2020-07-28 15:46:30', '2020-07-28 15:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_department_list`
--

CREATE TABLE `tb_department_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_department_list`
--

INSERT INTO `tb_department_list` (`id`, `department_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Production', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30', NULL),
(2, 'Merchandising', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_designation_list`
--

CREATE TABLE `tb_designation_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_designation_list`
--

INSERT INTO `tb_designation_list` (`id`, `designation_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Manager', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(2, 'Asst. Manager', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(3, 'Quality Controller', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_doctor_list`
--

CREATE TABLE `tb_doctor_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doctor_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_bangla_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_department_id` int(11) DEFAULT NULL,
  `doctor_designation_id` int(11) DEFAULT NULL,
  `doctor_qualification` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_bangla_qualification` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_joindate` date DEFAULT NULL,
  `doctor_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_registration_no` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_room` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_app_fee` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_doctor_list`
--

INSERT INTO `tb_doctor_list` (`id`, `doctor_name`, `doctor_bangla_name`, `doctor_department_id`, `doctor_designation_id`, `doctor_qualification`, `doctor_bangla_qualification`, `doctor_email`, `doctor_joindate`, `doctor_phone`, `doctor_registration_no`, `doctor_room`, `doctor_app_fee`, `doctor_address`, `doctor_photo`, `remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Samsujjaman Bappy', 'Bappy', 2, 2, 'NA', 'NA', 'admin@site.com', '2020-08-04', '01824168996', '321232655', '12003', '600', 'Uttara', '8951596381049.png', NULL, 1, 1, 1, '2020-08-02 14:35:39', '2020-08-02 15:10:50'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-08-02 16:15:59', '2020-08-02 16:15:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_doctor_shift_list`
--

CREATE TABLE `tb_doctor_shift_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `shift_day` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_doctor_shift_list`
--

INSERT INTO `tb_doctor_shift_list` (`id`, `doctor_id`, `shift_day`, `start_time`, `end_time`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(8, 1, 'Saturday', '14:28', NULL, NULL, 1, 1, '2020-08-02 16:24:27', '2020-08-02 16:24:27'),
(9, 1, 'Tuesday', '12:26', NULL, NULL, 1, 1, '2020-08-02 16:24:27', '2020-08-02 16:24:27');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_education_info`
--

CREATE TABLE `tb_employee_education_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `emp_exam_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_institution_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_result` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_scale` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_passing_year` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave_application`
--

CREATE TABLE `tb_employee_leave_application` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `leave_type_id` bigint(20) DEFAULT NULL,
  `leave_starting_date` date DEFAULT NULL,
  `leave_ending_date` date DEFAULT NULL,
  `actual_days` int(11) DEFAULT NULL,
  `approved_by` tinyint(4) DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_leave_type_setting`
--

CREATE TABLE `tb_employee_leave_type_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `leave_type_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_leave_days` int(11) DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_list`
--

CREATE TABLE `tb_employee_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_first_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_last_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_department_id` int(11) DEFAULT NULL,
  `emp_designation_id` int(11) DEFAULT NULL,
  `emp_gender_id` int(11) DEFAULT NULL,
  `emp_shift_id` int(11) DEFAULT NULL,
  `emp_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_dob` date DEFAULT NULL,
  `emp_joining_date` date DEFAULT NULL,
  `emp_probation_period` int(11) DEFAULT NULL,
  `emp_religion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_marital_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bank_account` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bank_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_card_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_blood_group` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_discontinuation` date DEFAULT NULL,
  `reason_of_discontinuation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nid` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nationality` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_parmanent_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_current_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_father_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_mother_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_ipbx_extension` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_account_status` tinyint(4) DEFAULT NULL,
  `created_by` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_employee_list`
--

INSERT INTO `tb_employee_list` (`id`, `employee_id`, `company_id`, `emp_first_name`, `emp_last_name`, `emp_department_id`, `emp_designation_id`, `emp_gender_id`, `emp_shift_id`, `emp_email`, `emp_phone`, `emp_photo`, `emp_dob`, `emp_joining_date`, `emp_probation_period`, `emp_religion`, `emp_marital_status`, `emp_bank_account`, `emp_bank_info`, `emp_card_number`, `emp_blood_group`, `date_of_discontinuation`, `reason_of_discontinuation`, `emp_nid`, `emp_nationality`, `emp_parmanent_address`, `emp_current_address`, `emp_father_name`, `emp_mother_name`, `emp_ipbx_extension`, `emp_account_status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '1001', '1', 'Bappy', 'Miah', 2, 1, 1, NULL, 'bappy@feits.co', '01824168996', '1595955903.jpg', '2020-06-29', '2020-07-01', NULL, 'Islam', 'Single', NULL, NULL, NULL, 'O+', NULL, NULL, NULL, 'Bangladeshi', NULL, 'Uttara', 'ABC', 'XYZ', '777', 1, '1', '2020-07-28 17:02:44', '2020-07-28 17:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_nominee`
--

CREATE TABLE `tb_employee_nominee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `nominee_name` varchar(220) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_relation` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominee_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominee_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_others_info`
--

CREATE TABLE `tb_employee_others_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_shift_weekend`
--

CREATE TABLE `tb_employee_shift_weekend` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift_id` tinyint(4) DEFAULT NULL,
  `day_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '"1" is weekend or  "0" Office Day',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee_work_history`
--

CREATE TABLE `tb_employee_work_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emp_id` bigint(20) DEFAULT NULL COMMENT 'emp_id will come from ("tb_employee_list.id") table',
  `wh_company_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_designation` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_joining_date` date DEFAULT NULL,
  `wh_resign_date` date DEFAULT NULL,
  `wh_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wh_attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gender_list`
--

CREATE TABLE `tb_gender_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gender_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_gender_list`
--

INSERT INTO `tb_gender_list` (`id`, `gender_name`, `remarks`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Male', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(2, 'Female', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(3, 'Other', 'None', 1, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_holidays_observances_leave`
--

CREATE TABLE `tb_holidays_observances_leave` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `holiday_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1 COMMENT '"1" for active,  "0" for inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_inpatient_occupancies_list`
--

CREATE TABLE `tb_inpatient_occupancies_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admission_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accommodation_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_inpatient_occupancies_list`
--

INSERT INTO `tb_inpatient_occupancies_list` (`id`, `admission_id`, `patient_id`, `doctor_id`, `accommodation_id`, `admission_reason`, `remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '123456', '1', '1', '1', 'Fever', 'None', 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pathology_test_category_list`
--

CREATE TABLE `tb_pathology_test_category_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_pathology_test_category_list`
--

INSERT INTO `tb_pathology_test_category_list` (`id`, `category_name`, `remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'Urine', 'N/A', 1, 1, 1, '2020-08-05 04:59:09', '2020-08-05 05:03:00'),
(3, 'Blood', 'N/A', 1, 1, 1, NULL, '2020-08-05 05:02:35');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pathology_test_list`
--

CREATE TABLE `tb_pathology_test_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `test_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `test_category_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_amount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `test_location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_pathology_test_list`
--

INSERT INTO `tb_pathology_test_list` (`id`, `test_name`, `test_category_id`, `price_amount`, `test_location`, `remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'CBC', '3', '380', NULL, NULL, 1, 1, 1, '2020-08-05 08:29:37', '2020-08-05 08:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `tb_patient_list`
--

CREATE TABLE `tb_patient_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_father_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_mother_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_gender_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_age` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_blood_group` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_profession` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact_phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_contact_relation` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_remarks` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_patient_list`
--

INSERT INTO `tb_patient_list` (`id`, `patient_id`, `patient_name`, `patient_father_name`, `patient_mother_name`, `patient_gender_id`, `patient_age`, `patient_address`, `patient_blood_group`, `patient_phone`, `patient_email`, `patient_profession`, `patient_photo`, `emergency_contact_name`, `emergency_contact_phone`, `emergency_contact_relation`, `patient_remarks`, `company_id`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '202001', 'Samsujjaman Bappy', 'XYZ', 'TYH', '1', '24', 'Uttara 10', 'O+', '01824168996', 'bappy@feits.co', 'Job Holder', NULL, 'ABC', '01515268864', 'Brother', 'None', 1, 1, 1, '2020-08-08 04:32:34', '2020-08-08 04:32:34'),
(2, '202002', 'Samsujjaman', 'abc', 'xyz', '1', '24', 'Uttara', 'O+', '01824168996', 'abc@gmail.com', 'Job', '20202020021596815080.jpg', 'Sabbir', '01515268864', 'Brother', 'None', 1, 1, 1, '2020-08-07 15:44:40', '2020-08-07 15:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_weekend_holiday`
--

CREATE TABLE `tb_weekend_holiday` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `day_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `company_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_weekend_holiday`
--

INSERT INTO `tb_weekend_holiday` (`id`, `day_name`, `status`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'Saturday', 1, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(2, 'Sunday', 0, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(3, 'Monday', 0, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(4, 'Tuesday', 0, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(5, 'Wednesday', 0, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(6, 'Thursday', 0, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30'),
(7, 'Friday', 1, '1', '2020-07-28 15:46:30', '2020-07-28 15:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_work_shift_list`
--

CREATE TABLE `tb_work_shift_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exit_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buffer_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '"1" is enable or  "0" disable',
  `created_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_ip` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 for inactive user and 1 for active user',
  `last_login_at` datetime DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_id`, `user_type`, `company_id`, `name`, `email`, `password`, `login_ip`, `status`, `last_login_at`, `created_by`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, 'super admin', 'super@email.com', '$2y$10$EyqK4Hh0kmOYBRCoPKWWBOVmRH2yc/PXCAo3aQ0BZHrZ8QL1nK2A6', '::1', 1, '2020-08-10 21:09:20', 1, NULL, NULL, '2020-07-28 15:46:30', '2020-07-28 16:53:07'),
(2, NULL, 2, NULL, 'admin', 'admin@email.com', '$2y$10$bcRZ.kA3VIE7KVHkMOSM.e3kbkEJqZkiD1zV/O2xQcRdUifwzDczS', '::1', 1, '2020-07-28 22:13:16', 1, NULL, NULL, '2020-07-28 15:46:30', '2020-07-28 15:46:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_to_activities`
--
ALTER TABLE `module_to_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_to_activities_module_id_index` (`module_id`),
  ADD KEY `module_to_activities_activity_id_index` (`activity_id`);

--
-- Indexes for table `module_to_roles`
--
ALTER TABLE `module_to_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_to_roles_module_id_index` (`module_id`),
  ADD KEY `module_to_roles_role_id_index` (`role_id`),
  ADD KEY `module_to_roles_activity_id_index` (`activity_id`);

--
-- Indexes for table `module_to_users`
--
ALTER TABLE `module_to_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_to_users_module_id_index` (`module_id`),
  ADD KEY `module_to_users_user_id_index` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_accommodation_category_list`
--
ALTER TABLE `tb_accommodation_category_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_accommodation_list`
--
ALTER TABLE `tb_accommodation_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_accommodation_list_accommodation_category_id_index` (`accommodation_category_id`);

--
-- Indexes for table `tb_app_attendance_history`
--
ALTER TABLE `tb_app_attendance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_app_attendance_history_attendance_ref_id_index` (`attendance_ref_id`),
  ADD KEY `tb_app_attendance_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_attendance_history`
--
ALTER TABLE `tb_attendance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_attendance_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_attendance_history_tmp`
--
ALTER TABLE `tb_attendance_history_tmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_attendance_history_tmp_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_blood_donor_list`
--
ALTER TABLE `tb_blood_donor_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_company_account_currency`
--
ALTER TABLE `tb_company_account_currency`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_company_account_currency_company_id_index` (`company_id`);

--
-- Indexes for table `tb_company_information`
--
ALTER TABLE `tb_company_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_department_list`
--
ALTER TABLE `tb_department_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_designation_list`
--
ALTER TABLE `tb_designation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_doctor_list`
--
ALTER TABLE `tb_doctor_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_doctor_list_doctor_department_id_index` (`doctor_department_id`),
  ADD KEY `tb_doctor_list_doctor_designation_id_index` (`doctor_designation_id`),
  ADD KEY `tb_doctor_list_doctor_email_index` (`doctor_email`),
  ADD KEY `tb_doctor_list_doctor_phone_index` (`doctor_phone`);

--
-- Indexes for table `tb_doctor_shift_list`
--
ALTER TABLE `tb_doctor_shift_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_doctor_shift_list_doctor_id_index` (`doctor_id`);

--
-- Indexes for table `tb_employee_education_info`
--
ALTER TABLE `tb_employee_education_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_education_info_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_leave_application`
--
ALTER TABLE `tb_employee_leave_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_leave_application_employee_id_index` (`employee_id`),
  ADD KEY `tb_employee_leave_application_leave_type_id_index` (`leave_type_id`);

--
-- Indexes for table `tb_employee_leave_type_setting`
--
ALTER TABLE `tb_employee_leave_type_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_list`
--
ALTER TABLE `tb_employee_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_list_company_id_index` (`company_id`),
  ADD KEY `tb_employee_list_emp_department_id_index` (`emp_department_id`),
  ADD KEY `tb_employee_list_emp_designation_id_index` (`emp_designation_id`),
  ADD KEY `tb_employee_list_emp_shift_id_index` (`emp_shift_id`);

--
-- Indexes for table `tb_employee_nominee`
--
ALTER TABLE `tb_employee_nominee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_nominee_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_others_info`
--
ALTER TABLE `tb_employee_others_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_others_info_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_employee_shift_weekend`
--
ALTER TABLE `tb_employee_shift_weekend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_employee_work_history`
--
ALTER TABLE `tb_employee_work_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_employee_work_history_emp_id_index` (`emp_id`);

--
-- Indexes for table `tb_gender_list`
--
ALTER TABLE `tb_gender_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_gender_list_gender_name_unique` (`gender_name`);

--
-- Indexes for table `tb_holidays_observances_leave`
--
ALTER TABLE `tb_holidays_observances_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_inpatient_occupancies_list`
--
ALTER TABLE `tb_inpatient_occupancies_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pathology_test_category_list`
--
ALTER TABLE `tb_pathology_test_category_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pathology_test_list`
--
ALTER TABLE `tb_pathology_test_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_pathology_test_list_test_category_id_index` (`test_category_id`);

--
-- Indexes for table `tb_patient_list`
--
ALTER TABLE `tb_patient_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_patient_list_patient_id_unique` (`patient_id`);

--
-- Indexes for table `tb_weekend_holiday`
--
ALTER TABLE `tb_weekend_holiday`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_weekend_holiday_company_id_index` (`company_id`);

--
-- Indexes for table `tb_work_shift_list`
--
ALTER TABLE `tb_work_shift_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `module_to_activities`
--
ALTER TABLE `module_to_activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `module_to_roles`
--
ALTER TABLE `module_to_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `module_to_users`
--
ALTER TABLE `module_to_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=794;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_accommodation_category_list`
--
ALTER TABLE `tb_accommodation_category_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_accommodation_list`
--
ALTER TABLE `tb_accommodation_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_app_attendance_history`
--
ALTER TABLE `tb_app_attendance_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_attendance_history`
--
ALTER TABLE `tb_attendance_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_attendance_history_tmp`
--
ALTER TABLE `tb_attendance_history_tmp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_blood_donor_list`
--
ALTER TABLE `tb_blood_donor_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_company_account_currency`
--
ALTER TABLE `tb_company_account_currency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_company_information`
--
ALTER TABLE `tb_company_information`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_department_list`
--
ALTER TABLE `tb_department_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_designation_list`
--
ALTER TABLE `tb_designation_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_doctor_list`
--
ALTER TABLE `tb_doctor_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_doctor_shift_list`
--
ALTER TABLE `tb_doctor_shift_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_employee_education_info`
--
ALTER TABLE `tb_employee_education_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_leave_application`
--
ALTER TABLE `tb_employee_leave_application`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_leave_type_setting`
--
ALTER TABLE `tb_employee_leave_type_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_list`
--
ALTER TABLE `tb_employee_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_employee_nominee`
--
ALTER TABLE `tb_employee_nominee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_others_info`
--
ALTER TABLE `tb_employee_others_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_shift_weekend`
--
ALTER TABLE `tb_employee_shift_weekend`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_employee_work_history`
--
ALTER TABLE `tb_employee_work_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_gender_list`
--
ALTER TABLE `tb_gender_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_holidays_observances_leave`
--
ALTER TABLE `tb_holidays_observances_leave`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_inpatient_occupancies_list`
--
ALTER TABLE `tb_inpatient_occupancies_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_pathology_test_category_list`
--
ALTER TABLE `tb_pathology_test_category_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_pathology_test_list`
--
ALTER TABLE `tb_pathology_test_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_patient_list`
--
ALTER TABLE `tb_patient_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_weekend_holiday`
--
ALTER TABLE `tb_weekend_holiday`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_work_shift_list`
--
ALTER TABLE `tb_work_shift_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
